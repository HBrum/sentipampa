#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, os


"""
	Esse script foi criado por Henrico Brum para aplicação exclusiva em seu trabalho de TCC 2.
	Provavelmente esse script só funciona para esse caso. :(
	Pelo menos funciona MUITO BEM pra esse caso. :)
	
	Para usar esse script, utilize a seguinte sintaxe.
	
	python build_test_files.py <CORPUS_PATH> <FOLDER_PATH> <FOLDER_NAME>
	
	<CORPUS_PATH> - caminho para onde está o córpus que você está querendo criar os arquivos de teste
	<FOLDER_PATH> - caminho para onde salvar a pasta com os 10 casos de teste
	<FOLDER_NAME> - nome para a pasta onde serão salvos os 10 casos de teste
	
	Use com cuidado. Não queremos sobrescrever nada importante para você.
	
	Exemplo:
		- python build_test_files.py /home/user/Documents/corpus.tree /home/user/Documents/ TESTES_CORPUS
		
	Será criado /home/users/Documents/TESTES_CORPUS 
	
"""

def main():
	argv = sys.argv
	
	if len(argv) < 4 or "-help" in argv:
		callHelp()
		exit()
	
	path = argv[2] + "/" + argv[3] + "/"
	
	#Criando o diretório de testes
	os.mkdir( path, 0755)
	
	tree = []
	
	train = []
	dev = []
	test = []
	
	#Lendo arquivo de córpus
	inFile = open(argv[1], "r")
	
	for x in inFile:
		tree.append(str(x).strip())
	
	inFile.close()
	
	total = len(tree)
	
	print("Total de árvores capturadas: " + str( total ) )
	
	testSize = int( total / 10 )
	trainSize = int( (total - testSize) * .9)
	devSize = total - (trainSize + testSize)
	
	print("Árvores em TRAIN: " + str( trainSize ) )
	print("Árvores em DEV  : " + str( devSize ) )
	print("Árvores em TEST : " + str( testSize ) )
	
	c = 0
	
	while c < 10:
		for i in range(0,len(tree)):
			if i >= c*testSize and i < (c+1)*testSize:
				test.append(tree[i])
			else:
				if len(train) >= trainSize:
					dev.append(tree[i])
				else:
					train.append(tree[i])
		
		#Criando pasta para caso de teste
		os.mkdir( path+"/0" + str(c), 0755)
		trainFile = open(path+"/0" + str(c) + "/train.tree", "w")
		devFile = open(path+"/0" + str(c) + "/dev.tree", "w")
		testFile = open(path+"/0" + str(c) + "/test.tree", "w")
		
		os.mkdir( path+"/0" + str(c) + "/noDev", 0755)
		
		trainFile2 = open(path+"/0" + str(c) + "/noDev/train.tree", "w")
		testFile2 = open(path+"/0" + str(c) + "/noDev/test.tree", "w")
		
		for t in train:
			if len(t) > 0:
				trainFile.write(t+"\n")
				trainFile2.write(t+"\n")
		for t in dev:
			if len(t) > 0:
				devFile.write(t+"\n")
				trainFile2.write(t+"\n")
		for t in test:
			if len(t) > 0:
				testFile.write(t+"\n")
				testFile2.write(t+"\n")
		
		train = []
		dev = []
		test = []
		
		trainFile2.close()
		testFile2.close()
		
		trainFile.close()
		devFile.close()
		testFile.close()
		
		c += 1
	
	


def callHelp():
	print ("\nPara usar esse script, utilize a seguinte sintaxe.")
	
	print ("\npython build_test_files.py <CORPUS_PATH> <FOLDER_PATH> <FOLDER_NAME>")
	
	print ("\n\t<CORPUS_PATH> - caminho para onde está o córpus que você está querendo criar os arquivos de teste")
	print ("\t<FOLDER_PATH> - caminho para onde salvar a pasta com os 10 casos de teste")
	print ("\t<FOLDER_NAME> - nome para a pasta onde serão salvos os 10 casos de teste")
	
	print ("\nUse com cuidado. Não queremos sobrescrever nada importante para você.")
	
	print ("\nExemplo:")
	print ("	- python build_test_files.py /home/user/Documents/corpus.tree /home/user/Documents/ TESTES_CORPUS")
		
	print ("\nSerá criado /home/users/Documents/TESTES_CORPUS \n")

if __name__ == "__main__":
	main()
