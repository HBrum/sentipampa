Me frustrei .
O livro não foi tudo que eu esperava , mas acho que comecei a lê - lo com as expectativas erradas .
Eu não gosto de o título , que é uma tradução literal .
O fim poderia ser melhor , mais desenvolvido .
Por o menos eu esperava mais .
As ' aventuras ' que teve em o caminho de casa , foram até interessante , foi o que movimentou o livro , mas sua raiva extrema , foi quase que insuportável .
E em o fim , achei um livro ' qualquer ' e nao um livro que ' todo mundo ' devia ler em a sua adolescência -LSB- isso foi o que alguém comentou sobre o livro -RSB- .
O livro , de início , não empolga muito .
Parece que a história não tem nenhuma intenção específica , e a narração é feita de maneira informal demais , com uso repetitivo de gírias e palavras de baixo calão .
Para um livro tão famoso eu esperava qualidade muito superior .
Creio que o livro é - ou foi - superestimado .
A história é fraca , ainda que interessante , valendo apenas por o perfil psicológico de o personagem .
Não quer dizer nada .
Às vezes ele é meio deprimente , e me deixa realmente com nojo , mas não é sempre .
A história é interessante , mas a narrativa em si é muito , muito cansativa .
Não leria novamente e nem recomendo .
Eu já queria ler esse livro fazia algum tempo por ouvir falar o quanto ele era bom , só que eu me decepcionei , por mais que a história seja muito boa se bem analisada , o livro é cansativo já que a todo o momento o personagem principal narra toda a história de ele de forma negativa .
Em o final achei que algo de especial aconteceria com os personagens , que Holden tomaria uma lição e visse que a vida não é de a maneira que ele pensa , mas me decepcionei .
Talvez porque consegui entender o motivo de tantos terem se empolgado com o texto e , apesar de isso , o livro não me tocou , não conseguiu mexer com mim .
Porém , com o passar de a leitura , a impressão inicial se esvai e percebe - se a imaturidade de o personagem , que de o alto de sua pseudo-superioridade odeia a mediocridade a sua volta .
Em suma , uma história sem objetivo sobre um rebelde sem causa e , ainda por cima , com um final frustrante .
Em a minha opinião , o livro é confuso , perturbador e entediante .
O final não tem emoção , e nada é muito surpreendente em o livro todo .
Como leitora diária de livros , posso afirmar que esta obra perdeu seu brilho .
Não dá pra tirar uma lição a não ser a de que eu não precisava ter lido e muito menos comprado .
Mas esse é o tipo de humor cansativo , principalmente por que o tal Caulfield é um maníaco depressivo muito chato .
Tudo ele acha ruim , tudo lhe chateia , qualquer coisinha insignificante o incomoda , o cara é um tremendo de um mala sem alça , insuportável .
Ele é um boçalóide metido intelectual .
O livro é tão ruim que levei dois meses para terminar de lê - lo .
E pensar que deixei de comprar ' Ensaio Sobre a Lucidez ' por causa de esse livro me dá náuseas .
Um lixo , uma verdadeira porcaria ... nunca li algo tão cretino em a minha vida e não me conformo de saber que as pessoas o consideram o livro de suas vidas .
Não recomendo !
O livro é idiota , repetitivo , cansativo e irritante , tanto quanto seu narrador .
Lixo de livro .
Péssimo !
Um de os piores livros que já li .
É chato , sem sentido , a história é fraca e a linguagem não prende ninguém .
Tinha grandes espectativas e me frustei .
Odiei ... Li as primeiras páginas e já não curti a linguagem de o livro .
É uma leitura fácil e superficial .
Não acrescenta muita coisa a o leitor , apesar de ter alguns trechos interessantes .
Tem o seu valor para o público pré-adolescente , principalmente , mas , a a medida que passam - se os anos , faz cada vez menos sentido .
Porém , o livro é a descrição de os pensamentos e '' aventuras '' , em Nova Iorque , de um adolescente fútil e fracassado , cujo conteúdo de o livro nada acrescenta a o leitor .
Decepcionante , tempo perdido !
Eu poderia e devria ter lido coisa melhor .
não gostei
simplesmente sofri até a ultima página louca pra terminar .
Essa edição que adqueri tem os diálogos separados apenas por vírgulas , o que dificulta o entendimento .
Mesmo assim , não chegou a me impressionar com obra de 5 estrelas .
Só achei que o final podia ser mais ... Esse livro só me deu 1 problema : conseguir os outros livros de o Saramago para ler .
O que complica a leitura é a forma como é escrita somente , pois quase não tem divisão de parágrafos e causa certo desconforto .
Mas eu também não o achei maravilhoso .
Leitura difícil por causa de o português de Portugal e de o estilo peculiar de o Nobel José Saramago .
O meio ponto que tirei foi porque o começo não me cativou muito .
Esperava um pouquinho mais . .
Mas nao considero um Bestseller ou um de os melhores livros de o Autor . . Final Pobre .
A história aqui poderia ter sido contada em um numero menor de paginas .
