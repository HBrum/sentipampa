É um de aqueles romances que todos deveriam ler , pq é escrito por um de os melhores autores que temos , e o tema central é extremamente atual .
ADORO !
Meu preferido livro de a literatura brasileira !
O livro tem uma linguagem popular , bem regional ; muitíssimo bem escrito .
Gostei muito !
È uma historia fantastica e fascinante , muito dinâmica , surpreendente a cada momento deixando o leitor ligado em os acontecimentos .
Estou muito contente por ter conhecido essa ficção de a realidade acho que Jorge Amado fez um excelente trabalho com a publicação de esta obra e para finalizar queria dizer que Pedro Bala e Dora são personagens únicos e inesquecíveis .
As estórias contadas em o livro emocionam , pois as personagens estão bem próximas de a gente .
Inquieta , mexe em as lágrimas , faz a gente se posicionar , contra ou a favor de as personagens , mas não de uma forma simplista .
Um livro comovente , sensual e intrigante .
O mais bonito de o livro está em a percepção de que não há maniqueísmo em os personagens .
Muito difícil acabar não se identificando com um de os meninos de o trapiche e ver em por o menos um de eles , um pouco de nós mesmos - em o meu caso fiquei encantado por o Professor - .
Porém , devo confessar que me surpreendi com toda a trama descrita por Jorge Amado , sobre os meninos de rua , instalados em um trapiche - galpão abandonado - e que roubam para sobreviver .
De os vários capítulos que compõem o romance , alguns são particularmente significativos .
Jorge Amado retrada bem o seu modo de linguagem fácil de escrita , nos proporcionando prazer a o ler esta história .
Gostei muito de o livro , pois retrada a realidade de os meninos de rua , alem de mostrar varios problemas de a sociedade , culturas e religioes de o local , é um livro muito gostoso de ler alem de ensinar bastante .
Vale a pena ler o livro , ele retrata a vida de crianças de rua , e mesmo sendo feito em 1937 a realidade infelizmente ainda é a mesma , a linguagem de o texto é simples e as imagens diretas e , o livro revela a diferença de classe e a violência contra os mais pobres .
Nunca ninguém havia mencionado em literatura este bando de jovens que engenhosamente desafia as autoridades , roubando a classe privilegiada e dividindo o produto de o roubo entre os seus camaradas subnutridos .
Embora claramente uma obra de protesto '' Capitães de a Areia '' não se vale de ocas figuras centrais que se limitem a expor determinadas filosofias , tampouco marcas negativas .
A o contrário todas principais personagens , como as secundárias mais significativas , são bem desenvolvidas , ainda que um tanto sentimentalmente capazes mesmo de combater com excessivo preconceito de o narrador onisciente .
Os personagens são em sua maioria masculinos , e de entre eles , Pedro Bala , cuja agilidade sugerida por o apelido , merece especial atenção - Sem Perna , Pirulito , O Professor são os mais destacados : Ainda temos em o grupo João Grande , Volta Seca , Boa Vida muito bem configurados por os seus apelidos baseados em a aparência física .
Dora é a única figura feminina merecedora de destaque , pois ela passa a assumir o referencial feminino de a família , a mãe .
Jorge Amado , o autor de esse belo livro , conta as várias história de um grupo de pobres garotos de Salvador , Bahia , que roubam não só por prazer , mas para saciar sua fome de vingança de falta de carinho , injustiça e ódio de as autoridades , que judiavam de os mais pobres .
Sabe - se que este romance é muito interessante e cativante , pois não se pode negar que esta história prende bastante o leitor em vários casos narrados com tanta injustiças e revoltas , porém , além de também criticar vários problemas de a sociedade , esses garotos se relacionam bastante com a religiões e culturas locais , mostrando diversas características de o nordeste brasileiro , se tornando um livro gostoso e cativante de se ler .
O enredo de a historia é cheio de dramas , conflitos , prendendo o leitor até o fim .
Em o maravilhoso enredo de a obra , o autor , Jorge Amado , traz a a tona para o leitor emoções e questionamentos diante de a saga de os ' Capitães de a Areia ' , meninos de rua que vivem em um trabiche a a beira-mar , fato que dá nome a o livro .
O livro é uma excelente aventura muito marcante a os leitores que a evidenciam , possui cenas muito descritivas que fazem parecer que você esta olhando - as de o muito marcante .
O livro com narrativa socialista em os de a a a oportunidade de repensar em nossas vidas .
Embora claramente uma obra de protesto '' Capitães de a Areia '' não se vale de ocas figuras centrais que se limitem a expor determinadas filosofias , tampouco marcas negativas .
A o contrário todas principais personagens , como as secundárias mais significativas , são bem desenvolvidas , ainda que um tanto sentimentalmente capazes mesmo de combater com excessivo preconceito de o narrador onisciente .
Os personagens são em sua maioria masculinos , e de entre eles , Pedro Bala , cuja agilidade sugerida por o apelido , merece especial atenção - Sem Perna , Pirulito , O Professor são os mais destacados : Ainda temos em o grupo João Grande , Volta Seca , Boa Vida muito bem configurados por os seus apelidos baseados em a aparência física .
Dora é a única figura feminina merecedora de destaque , pois ela passa a assumir o referencial feminino de a família , a mãe .
O livro em minha opinião é bem interessante para uma leitura casual , ou até mesmo uma discussão entre amigos .
pois a o mesmo tempo que o livro pode ser obsceno , é também realista , pois se passa entre jovens pobres que poucos são alfabetizados e tem que usar suas artimanhas para conseguir comida e enganar famílias - como por exemplo conseguir comida jogando em a cara que são órfãos - .
o livro é ótimo , jóinha pra ele , só tome cuidado pra nao passar pra um infantil , pois o livro tem linguagens um tanto obscenas .
O livro mostra diversos ensinamentos , como o companheirismo , que acompanha os garotos por onde forem , pois um ajuda o outro sempre que preciso , mesmo que este tenha que fazer grandes trabalhos e esforços , além de mostrar que , muitas vezes , jovens que são marginalizados são obrigados a ter essa vida , pois sem pai , mãe , carinho e nem apoio de ninguém se vêem obrigados a seguirem uma vida criminosa .
A narrativa nos surpreende com o decorrer de os fatos , e aparição de novas personagens que deixarão sua marca em a envolvente história .
Um livro consagrado por Jorge Amado , escritor muito reconhecido , seu livro , que permanece importante em a literatura Brasileira até hoje , promete ficar por muitos e muitos anos .
É um livro bem interessante e surpreendente .
Em a minha opinião , o livro é muito interessante pois mostra que antigamente já existiam crianças abandonadas e que acima de tudo , a diferença entre as classes sociais e os preconceitos que isso gera .
Em o geral , as preocupações sociais dominam , mas os problemas existenciais de os garotos os transforma em personagens únicos e corajosos .
O livro possui uma narrativa cativante e - por o modo como os fatos e conflitos são apresentados a o leitor - surpreendente .
É uma história emocionante que gira em torno de revoltas , falta de carinho , abandono , além de retratar um fato que ainda ocorre até hoje que é o abandono de crianças voltado a a marginalidade .
O livro é bem interessante , e os conflitos e descobertas te prendem surpreendentemente a história .
Que livro !
Esse livro , que peguei por obrigação , me surpreendeu totalmente .
Admito que o tema mexe com mim , mas a história , o estilo de Jorge Amado , são ; ; ; de tirar o fôlego .
Um de os melhores livros que já li .
A o longo de as nem muitas nem poucas páginas de o livro , criei uma relação de amor e ódio com cada um de os personagens , de modo que não lembrava de sentir algo assim antes a o ler um livro ... E ainda mais sendo um livro que trate de crianças marginalizadas de rua , um assunto um tanto quanto batido hoje em dia devido as '' cidades de deus '' e outros filmes e reportagens a respeito .
Jorge Amado em esta obra , apesar de como sempre explorar demais a sexualidade - em crianças !
- construiu um mundo , ou melhor , descreveu o nosso mundo de a maneira mais tocante e infantil , seja em as despreocupações de o Gato , em o ódio de Sem-pernas , em a crueldade de Volta-Seca , em a inveja bem resolvida de Professor ou ainda em a liderança nata de Pedro Bala .
Esta é uma história por vezes triste , por vezes alegre , por vezes revoltante , mas sempre marcante .
Capitães de Areia é uma obra marcante .
Embora não receba o mérito merecido , é impossível não sensibilizar - se com a história .
Relata com muito realismo , muita sensibilidade e pouco sentimentalismo a vida de menores abandonados , os chamados '' capitães de areia '' .
Com o desenrolar de a história cada personagem vai tomando suas características , com histórias marcantes .
Cada capítulo muito mais que uma história , uma realidade .
