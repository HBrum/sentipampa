Ensaio Sobre a Cegueira é primoroso em os detalhes e em os perfis psicológicos de os personagens .
O enredo é muito bem amarrado , mas o mais fantástico de tudo em o livro é que ele escancara a essência humana : em situações extremas não passamos de animais .
Talvez o melhor livro de Saramago , em a minha opinião .
Um livro forte e sensivel a o mesmo tempo .
Está entre os melhores que já lí .
Tive que ler para a faculdade e adorei .
Acho que um de os melhores livros que já li .
Recomendo ambos !
Gostei bastante de o livro .
Demais !
Chocante , duro e temeroso .
É de embrulhar o estômago e desnuda a alma humana como nenhum outro que já li o fez .
Uma leitura realmente fascinante .
Fantástica história , a forma como os personagens se desenvolvem .
Prende de o início a o fim .
Em " O outro lado de a meia-noite " o brilhante autor constrói uma história totalmente sólida de amor , ódio e vingança .
Todos os personagens são diferentes e únicos .
O diferencial de esse livro é que nossos sentimentos por os personagens principais mudam a cada página lida .
" O Outro lado de a meia-noite " não é apenas um livro que lemos de o início a o fim apenas para nos entreter .
Ele é tão bem escrito que parece que estamos em um teatro assistindo a uma grande história .
Leitura MUITO RECOMENDADA !
Se você está querendo comprar algo para ler , pode comprar sem pensar duas vezes !
Gostei bastante , gente !
Foi o primeiro livro de o autor que eu li , achei surpreendente e muito bom - :
Mais a personagem central de essa história para mim é a Noelle , nossa anti-heroína , ela é uma vilã fascinante difícil de odiar , você sente certa fascinação e atração por ela , além de bela tem um magnetismo , que nós poderemos sentir através de personagens que passaram por sua vida em diferentes fases , já em as primeiras páginas temos um vislumbre de a sua personalidade sobre pontos de vista diferentes .
Este livro realmente me surpreendeu .
Logo em o inicio já temos uma idéia de o fim , mas mesmo assim , o fim conseguiu me surpreender .
Mas em um contexto todo , o livro conseguiu suprir minhas expectativas , principalmente em o fim .
Para aqueles que gostam de histórias que se desenrrolem lentamente , esse livro é ideal .
É cheio de supresas , segredos , enfim , é um ótimo livro .
O enredo é envolvente , assim como todos de o SS , mas a o mesmo tempo é previsível .
Um de os melhores livros que ja li sem duvida .
Quando comecei a ler não consegui parar mais .
Perfeito para quem gosta de mentes sombrias com um toque de romance , aventura e guerra .
Bom não tem como descrever tudo o que o livro aborda , mas com certeza qualquer um que leu ou vai ler ficará triste quando chegar a o fim .
Por falar em fim é um de aqueles que só Sidney sabe fazer de deixar de boca aberta .
UM SENHOR BEST SELLER !
Me apaixonei por o livro , por o autor e por todas as outras obras de ele , as quais fui em busca para ler uma por uma .
Foi o outro lado de a meia noite e Sidney Sheldon que me abriram as portas de o mundo de a leitura , de o qual não consegui mais sair , e nem quero .
O romance é perfeito em tudo , desde personagens , lugares , e claro seu maravilhoso enredo cheio de reviravoltas , surpresas e muito suspense .
Pra mim é um livro inesquecivel e perfeito .
Acho muito dificil alguém ler e não gostar , mas pode acontecer , como eu não sei !
Não gosto muito de estórias que tratam de paixões , principalmente se for um triângulo amoroso , mas Sidney Sheldon sabe muito bem como nos prender a a estória e acabei que gostei de o livro .
trama excelente
A trama de este livro é simplesmente excelente , não é um livro previsível e o final é surpeendente .
Ameeeei a personagem Catharine .
Mas em a verdade todos os personagens foram muito bem montados .
Li em dois dias e só descansei qdo descobri tudo .
Vale muito a pena .
Esse ainda está incluido entre os bons de o cara .
Marcou minha adolescência
Eu li O outro lado de a meia-noite quando tinha 13/14 anos , Amei a estória muito bem escrita , é , junto com o Reverso de a Medalha , um de os meus preferidos de Sheldon .
Adoro .
è um bom livro , prende sua atenção , vc começa e não quer parar mais .
Acho esse livro impressionante .
Acho impossível não manter uma relação de amor e ódio com Noelle por exemplo , que em a minha opinião é uma de as personagens mais fortes que Sheldon já criou .
Sidney me surpreende a cada livro .
Em este livro emocionante , Sidney consegue mostrar duas mulheres totalmente distintas , mas com uma coisa em comum : elas amam o mesmo homem .
Recomendo ; DD
Lembro que gostei bastante e que Noelle era uma verdadeira P ... Ela sabia usar o poder de a sedução com os homens que caiam como patinhos .
É interessante a trama de cada personagem .
Mesmo cortando as tramas , a gente se envolvia rapidamente em a outra e assim o livro seguia .
Muuuuuuuuuuito bom !
Um livro eletrizante ... duas mulheres que sofrem por causa de um mesmo homem ... isso não poderia acabar bem não é ?
O mais interessante é que o final foi imprevisível e surpeendente .
Muito bom !
Não me recordo de os pequenos detalhes , mas esse livro está em o top 5 de os livros de o Sidney que marcam o leitor .
Apesar da Noelle não ser uma pessoa com boa conduta e tudo mais , é a minha personagem preferida , não sei explicar o por quê , simplemente li e gostei mais de ela .
Gostei bastante de esse livro de SS ..... uma trama bem feita , mais aberta sem aquele suspense pra depois escancarar tudo de vez , o que as vezes fica meio chato com o decorrer de a estória .
O final não deixou nada a desejar .
Recomendado .
noossa , personagens incriveis !
historia incrivel .
Quando começa a ler nao consegue parar mais !
Recomendo !
Livro para ningupem botar defeito .
Uma história cheia de requintes de suspense e drama , Sidney sheldon mais de o que ninguém sabe entreter seu leitor de um jeito que ficamos ecstasyados rsrs .
Adorava a aldácia de Noelle personagem que eu mais gostei .
Mesmo eu gostando de Noelle achei o final fantástico , um livro pra sempre se recordar .
É uma história fascinante , com um final a o mesmo tempo satisfatório e triste .
Maravilhoso !
Você começa a ler e não consegue parar mais .. O livro é conta sobre o quanto o desejo de vingança pode custar caro .
Simplesmente muuito bom !
Recomendo !
Um livro bom .
História muito bem elaborada .
Verdade esse é talvez o livro que perdi as contas de quantas vezes eu reli .
Já que é um livro tão bom que sempre pegava emprestado para ler , pois é um livro que entrou em as lista de mais vendidos por dezenas de semanas .
Excelente livro !
Muito Bom !
Recomendo !
A leitura me surpreendeu de o início a o fim .
Sofri com a protaginista a cada nova página .
Foi um livro que me emocionou muito .
esse livro como todos os outros de o Sidney Sheldon é maravilhos , instigante , empolgante ... enfim muito bom e o mais incrivel é que quando eu assisti o filme eu pensei :'vai faltar muita coisa e não vai ser tão bom quanto o livro .
' errei o filme é muuito bom é claro que falta alguns pequenos detalhes né mais é bom mesmo .... Recomendo tanto o livro quanto o filme !
Já perdi a conta de quantas vezes reli esse livro ... e cada vez que releio , é como se fosse , de novo , a primeira vez !
Noeelle não é a minha heroína favorita , mas com certeza , está em o topo five !
Achei simplesmente incrível .
Os personagens criados em essa história são maravilhosos , e a forma como o autor os desenvolve a o longo de o livro é fascinante .
Quando os destinos de os três principais personagens se entrelaçam , parar de ler se torna impossível .
O julgamento em o último capítulo é emocionante , e o final , apesar de muito triste , é ótimo .
Recomendo a qualquer um que desejar um bom livro !
Com uma história empolgante e personagens fascinantes .
Um bom livro , mas sem dúvidas não é o melhor de o autor .
A o longo de o livro somos testemunhas de o poder de Kate e sempre ficamos impressionados como ela sempre estava a um passo a frente nos surpreendendo com a força vital de uma mulher que me deixou marcado por sua determinação .
Outros personagens marcantes foram as netas Eve e Alexandra com aparência idêntica , mas com personalidades diferentes .
Adorei o livro assim como todos que já li de o Sidney .
" O reverso de a medalha " não é um livro ruim .
A trama é movimentada , abrange um longo período de tempo e três gerações de a poderosa família Blackwell em uma trama de muitas reviravoltas , como é característico de o autor .
Gostei particularmente de a primeira parte de a história , que se passa em a África de o Sul e se foca em James Blackwell , um homem corajoso , perseverante e movido por o desejo de vingança .
Embora o foco de o livro de uma maneira geral seja Kate Blackwell , foi James quem achei mais interessante , cheio de qualidades maravilhosas e defeitos terríveis , como só os bons personagens podem ter .
Ora , se a trama é boa e em ela há por o menos 1 personagem memorável - também gostei muito de Margareth - , por que apenas 3 estrelas ?
Para quem gosta de histórias rápidas , com muita ação e sem perda de tempo com grandes descrições de ambientes ou sentimentos , Sidney Sheldon é o autor certo e " O reverso de a medalha " é uma de suas melhores histórias .
Mesmo para aqueles que , como eu , não apreciam muito esse tipo de escrita , " O reverso de a medalha " é um bom livro para se ler naqueles momentos em que se quer uma leitura leve , sem grandes compromissos .
Leiam esse livro .
não irão se arrepender , até hoje foi o melhor que já li !
Uma de as melhores obras que eu já li .
Que livro maravilhoso .
Adorei o livro .
É uma obra prima , cheia de tramas e assassinatos .
Adoroooooo esse livro !
marcou minha juventude .
A história de esta família marcada por assassinatos , escândalos , muito amor , ódio e riqueza em os guia através de cada capítulo em os mantendo " presos " a a narrativa .
Os personagens são carismáticos , humanos , enfim reais ... e com suas qualidades e defeitos , erros e acertos são o ponto forte de a trama .
Enfim , o mestre Sheldon conseguiu com o livro construir um thriller envolvente e instigante .
O melhor de Sidney Sheldon
Foi uma de as melhores escolhas que já fiz em a biblioteca .
Sem a menor dúvida , esse livro é a obra-prima .
Ainda que siga a mesma receita de os outros livros , o autor vai além e cria uma história longa , complexa e emocionante .
Não importa quantas vezes eu leia - até onde eu me lembro foram umas 5 leituras - , " O Reverso de a Medalha " sempre vai me prender e me deixar tensa até que eu chegue em o final .
Escrita A trama e o desenvolvimento de a história é riquíssima em detalhes , mas conhecendo o autor , ele detalha de forma magistral , sua escrita inconfundível , simples , direta é o seu maior trunfo em o livro , pois " persegue " o leitor , sempre querendo ler o próximo capítulo .
Para os fãs e os que querem conhecer o autor , não se esqueça de ler este livro !
Que história !
Gostei bastante .
O melhor livro lido em a minha vida !
Comecei esse livro com muita expectativa , afinal , sou fã de Sidney Sheldon , porém , logo em as primeiras páginas desanimei um pouco , parece que demorou uma eternidade para aquelas poucas páginas passarem , mas de novo , estamos falando de Sidney Sheldon , e quando Jamie apareceu , o mundo estava bom de novo .
Jamie , David e Tony foram as partes que me fizeram dizer que valeu a pena a leitura .
Pode - se dizer que o livro tem seus autos e baixos , mas em meu conceito , ficou apenas em o bom - e que de acordo com o Skoob , são três estrelas - .
Faz com que leitor fique preso a a narrativa .
É envolvente e instigante .
Efim é o melhor livro de o Sidney Sheldon que li até hoje .
O reverso de a medalha foi o primeiro livro de Sydney Sheldon que tive contato , foi uma boa primeira impressão , o livro consegue te deixar ansioso boa parte de o tempo , querendo saber qual vai ser o próximo golpe que os Blackwell vão planejar , seja em os negócios ou dentro de a sua caótica família .
O livro se divide em arcos mostrando os anos e em torno de quem a historia vai girar , devo dizer que meus favoritos foram os que envolveram Jamie McGregor , Kate - jovem - e Tony .
Tony foi um personagem que não dava nada , mas acabou se tornando meu favorito , ele foi o primeiro a lutar contra a maldição que é o peso de a corporação Blackwell , e talvez o que sofreu mais com isso .
Entretanto todos os arcos conseguem te prender a o livro e as paginas simplesmente voam !
Depois de O Reverso da Medalha eu definitivamente pretendo ler mais obras de o Sydney Sheldon , fica a recomendação pra quem se interessar também !
Esse livro foi contagiante e profundo .
Repleto de ação , suspense , aventura , sedução , intrigas , muitas reviravoltas e eletrizante de o princípio a o fim ! '
Em a minha singela opinião , esse romance de Sidney Sheldon é um clássico , onde o autor narra a história de várias gerações de a família McGregor , que fundou seu império com os diamantes de a África de o Sul , chegando até a protagonista Kate Blackwell , que é uma mulher sensual , bela e extremamente impetuosa .
Essa , sem a menor sombra de dúvida , foi a melhor parte de o livro , suspense de o princípio a o fim durante a travessia de os dois por o deserto cheio de armadilhas e perigos ... Muito emocionante !
Sem dúvida nenhuma , podemos tirar uma grande lição de como aprender de forma intuitiva e prazerosa a o passar por as páginas de esse romance fantástico que mistura realidade e ficção a ponto de deixar o leitor sempre atento para verificar o destino de uma senhora ambiciosa que é capaz de sacrificar o próprio filho para centralizar suas riquezas , uma neta também arteira e mal-caráter que entre tantos desmandes e armações conseguem a maioria de seus objetivos financeiros .
Ação e Mistério de tirar o fôlego , mas início arrastado
Mas o Reverso da Medalha me cativou com nenhum outro .
Mas a partir de o momento que a ação começa , é impossível fechar o livro novamente .
Recomendo a todos , ótimo livro e merece meu respeito .
Emocionante de o princípio a o fim : a chegada de Jamie a a Africa de o sul , o achado de o primeiro diamante em a praia de a Namíbia , as ruas boêmias de Paris , as mansões imponentes de a Inglaterra , as salas de reuniões de a alta sociedade , as alcovas de a América , até a família Macgregor se tornar a elite européia dona de um vasto império comercial que se expandia cada vez mais , de acordo com os planos de a ambiciosa Kate Blackwell , que a meu ver , é uma de as mulheres mais fortes de a literatura .
Maravilhoso .
Cada membro de essa família tem uma história melhor de o que a outra .
Empolgante !
O Reverso da Medalha é um de os meus preferidos !
A história é envolvente , não há como não sentir - se preso A as páginas de o livro , com uma grande vontade de conhecer seu desfecho !
Os personagens são marcantes , e trama criada por o autor é de fazer prender o fôlego !
Quanto mais nos aproximamos de o final , mais emocionante fica !
Eu não via a hora de terminar a leitura de as 459 páginas !
Eu li , reli e recomendo !
É o melhor livro que já li .
Vibramos por Jamie em o começo e por tudo que ele alcançou , mas o detestamos por o modo como trata Maggie .
Depois nos orgulhamos de Kate , para nos decepcionarmos com ela mais a frente .
E em o final quando Eve é castiga achamos que ela merecia muito mais .
Sou fã de o Sidney , pois ele sabia escrever com maestria , nos envolver em a história a ponto de não querermos largar , fazia muito tempo que não lia um livro de 459 páginas de um dia para o outro .
Valeu cada hora de sono perdido ... rsrsrs .
Super recomendo .... Nota 10 !
De todos os livros que já li de o Sidney Sheldon , esse é um de os melhores .
Me surpreendi bastante , tem uma história interessante e com um gostinho de quero mais .
Muito bom !
sem dúvida o melhor livro de o sidney sheldon !
muitoooooooo bom !
Eu nunca me surpreendi tão positivamente com um livro .
Uma emoção muito grande mesmo após a leitura .
A perfeição defeituosa de os personagens é o que compõe a obra .
A importância que Kate tem por a companhia che ga a ser apaixonate , a o mesmo tempo muito irritante , mas apaixonante novamente .
A maldade de alguns personagens chega a ser chocante , mas alguns personagens como Banda , Margaret , Tony , Jamie e Kate toranram - se meus favoritos .
enfim , apesar de td e tmb de as historias anteriores a de a Kate , o livro é muito boom !
Ameei !
Kate Blackwell essa sim é uma de as personagens mais fantásticas de o Sidney Sheldon , essa vontade de ela de querer controlar tudo e a todos , a sagacidade de ela , só posso dizer que é uma personagem fantástica .
Adorei o tempo em que se passa a história toda a árvore genealógica o inicio de a fortuna de a familia , os acontecimentos a o redor de o mundo com o passar de a história , a história de cada geração a mudança de cenário e a rapidez de a história sem fugir a os detalhes e fatos , adorei .
É viciante !
São muitas surpresas !
E quando você pensa que não há mais nada possível para acontecer , mais uma vez você é surpreendido !
Valeu a pena cada caractere de o livro e cada milésimo de o tempo gasto .
Em 459 páginas são contadas as histórias de cinco gerações e em nenhum momento essa história é contada de forma superficial .
Somos absorvidos por as intrigas e por os problemas de a família e de a empresa de Kate .
Em o começo eu a adorava , achava um mulher forte e maravilhosa por estar determinada a não ser igual a todas as outras mulheres .
É um história ótima , ótima mesmo .
Adorei sua trama , as vinganças , os planos bem elaborados , as reviravoltas , os dramas .
Sidney Sheldon em este livro constrói uma saga emocionante .
Não dá pra parar de ler .
A história é muito bem escrita , a saga de essa poderosa família é eletrizante e eu sofria cada vez que tinha que adiar a leitura .
Kate Blackwell , ou você a ama ou a odeia , mas não dá para não admirá - la .
Tenho que ser coerente e dar 5 estrelas pois me senti totalmente envolvida em a história .
maravilha de livro
Essa narrativa é simplesmente alucinante .....
Adorei a trama .
Mais uma história incrível de Sidney Sheldon .
A história de esta família marcada por assassinatos , escândalos , muito amor , ódio e riqueza guia o leitor a cada capítulo se manter " preso " a a narrativa .
Personagens bem carismáticos , humanos , com suas qualidades e defeitos , erros e acertos são um ponto forte de a trama .
Um thriller envolvente e instigante .
Leitura obrigatória
O Reverso da Medalha é de longe uma de as obras mais fortes e bem montadas de o gênio que Sidney Sheldon foi .
E com certeza , uma de as grandes obras de a literatura romancista .
O livro está marcado por personalidades fortíssimas , apaixonantes , eu deveria dizer .
É absolutamente um grande livro que consegue facilmente marcar o leitor de forma forte e altiva .
Leitura obrigatória para os amantes de a literatura .
Eu queria ser como Kate Blackwell , ela é um tipo diferente de heroína , não é a típica donzela em perigo , acho que gostei mais de ela que de o livro em si , a forma como ela joga com as pessoas pra conseguir tudo o que quer .
Amei o livro .
ÓTIMO !
liivroo ótiimo .... e surpreendente !
adoreii , realmente SIDNEY SHELDON soube dispertar emoções em esse livroo ! ...
Mais uma vez Sheldon arrasou , com um enredo fascinante e envolvente e personagens pra lá de complexos !
Espetacular !
Logo em as primeiras págimas me apaixonei por o livro , que considero o melhor de Sidney Sheldon .
Perfeito .
Não existem palavras que possam definir essa obra prima .
PS :Adorei o final de Eve , melhor impossível .
Um livro fascinate que soube me entreter de o começo a o fim , o livro conta a história de várias gerações de uma família de milionários que percorre todo o século XX .
Em esse livros encontramos vários personagens marcantes ente esses personagens temos Kate Blakwell uma mulher ambiciosa e sem escrúpulos faz de tudo para conseguir atingir seus objetivos , mesmo que isso lhe custe o carinho de as pessoas que a cerca .
Vale a pena ler essa obra magnifica que te prende de o começo a o fim .
Enredo eletrizante , muitas reviravoltas em o desenrolar de a trama , final previsível , mas que gera grande expectativa em o leitor .
Recomendo para quem dispensa textos densos .
Não sei como uma pessoa pode ter uma mente tão boa , é inacreditavel como tudo acontece tão perfeito em este livro ... estou apaixonada mais uma vez com os livros de ele .
Adoreiii Kate ela é simplismente inacreditavel e inabalavel , numca vi nada parecido .
indico a todos !
Definitivamente é uma leitura agradável , ainda que não seja a melhor de o autor !
Este livro é tão bom , que achei o filme de ele e assisti depois de ter lido o livro , e reli muitas vezes também .
Muito bom .
Incrível !
huahuahua É incrível ... são várias história .. .
Essa é apenas uma de as meneiras que podemos definir esse livro de Sidney Sheldon , a mais bem elaborada de suas histórias devido tanto a quantidade de tempo em que o enredo se passa - um século , passando inclusive por as grandes guerras mundiais - quanto a riqueza de detalhes e realismo de seus personagens .
A mais marcante é sem dúvida Kate Blackwell , uma mulher de garra que não poupa enforços em sua vida profissional , transformando a herança de o pai em uma de as mais poderosas holdings de o planeta .
narrativa fantástica ...
Apesar de muito suspense , emoção , drama ... este livro tem uma bela mensagem .
Choque .
Suspense .
Imprevisibilidade .
Drama .
Maniqueísmo .
: -
Sem duvida ... umas de as melhores obras escritas por um humano .
Sidney Sheldon se consagrou em essa história marcante e , para mim , perfeita .
Nossa esse livro com toda certeza é o meu preferido !
foi o 1º que eu li e foi amor imediato .
Ele conta a estoria de 3 gerações cheia de altas e baixa mas que depois de tanta luta e determinação tudo dá certo , uma leitura apaixonante que te segura de o começo a o fim .
Eu recomendo !
Já li umas 10x por o menos é a minha reliquia ..
Trama cativante e um livro muito gostoso de ser lido , realmente te prende de o início até o fim !
Tramas interessantes , cheias de detalhes , de qualidade , ricas , envolvendo coisas de o mundo e traçoes de personalidades , muito bem escrito , muito bom de se ler - inclusive em as questões relativas a amor , sexo , relacionamentos , acaba te prendendo e te interessando , devido a riqueza de informações de o autor e forma de escrever - , e as últimas páginas são eletrizantes , de um suspense incrível e que mexe com você - e o final é fantástico e hilário , mas muito verdadeiro - .
Pensei em ler somente este livro de o autor porque era o mais bem comentado e muito elogiado , mas ele me cativou , conquistou , por a riqueza de informações que dá em a obra , sobre o mundo e suas coisas , sobre as pessoas , e de uma forma muito bem escrita e cativante , envolvendo suspense e situações fortes mas envolvendo coisas de o mundo em suas diversas áreas , o que dá o tom de poderem sim ter o tom de verdadeiras - em o vasto mundo , ou então tirando - as pras coisas comuns - ; então me deu um querer de conhecer outras obras futuramente , os títulos de as outras obras são bastante sugestivos , como " Lembranças de a meia-noite , " O outro lado de a meia-noite " , " Manhã , tarde e noite " , " Nada dura para sempre " ... ; acho que pode ser uma leitura muito boa de se ter , além de ser uma leitura " bem gostosa " , realmente - pra um momento de " retomada " a os livros , então !
Gostei de o livro e adorei a principal ... é uma familia cheio de doidos , mas vale a pena ler e entender o que eu quero dizer .
PARECIDA apenas .
Kate Blackwell é a dona de o livro , mas pra mim quem teve mais brilho foi Jamie .
: - Um livro fascinante de suspense e armação que tem mais reviravoltas que novela mexicana quando começa a ficar sem assunto e tem mais 3 meses pra terminar .
Esse livro tem uma narrativa q te prende , isso é bem verdade .
Perfeito .
Cativante e muito inusitado .
Choque .
Choque .
Suspense .
Imprevisibilidade .
Drama .
Maniqueísmo .
: -
As outras histórias acabam até tendo alguns clichês , principalmente as clássicas gêmeas boa e má , mas ainda assim o livro é delicioso !
E o desenrolar de os fatos é sempre surpreeendente !
Com uma narrativa que te prende de o início a o fim , o Reverso da Medalha é um de os livros que mais gosto !
Já li 3 vezes e recomendo !
Nossa esse livro é 10 !
Eu comecei e não consegui mais parar .
