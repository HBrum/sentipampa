Vale a pena conferir .
Surpreendeu .
Lí esse livro meio que por obrigação , mas surpreendeu .
Tem horas que você odeia os personagens , tem horas que você se sente mal por eles .
Leitura fácil .
É um livro que você deve reler e reler e deixar as idéias , aquelas idéias que você não quer processar , entrem em sua mente .
E posso dizer que por toda minha vida a história de Pedro Bala , Professor , João Grande , Sem-Pernas , Pirulito , Dora e Gato estarão em minha mente .
Publicado em 1937 , a obra parece conter os ingredientes necessários para se manter contemporânea : a retratação de a desigualdade social , oriunda de uma vivência intensa de a parte de o autor em as ruas de Salvador e até em o Trapiche - lugar de paz e acalanto para os capitães de a areia , que em este refúgio guardam os objetos furtados e descansam .
Tão marcante quanto o traço social , o traço figurativo de como agem as autoridades é requintado e bem delineado por Jorge Amado , onde mostra que há uma distorcão de informações sobre os serviços governamentais para que a sociedade possa ser cúmplice contra os Capitães de a Areia .
O legado de a obra incide em a majestosa visão humana que nos é transferida .
Um livro batuta , este .
Capitães da Areia é uma opção de leitura agradável e dramática , se mantém um tom adulto , mas com toque moleque .
O livro é muito bom , só que o livro tem um ritmo , você não pode para de ler em qualquer parte , e estar concentrado em a leitura , mas é muito bom , recomendo !
É de se espantar a fidelidade de esses garotos para com o grupo .
São verdadeiros paradoxos que confundem o leitor , e o deixa sem saber qual lado tomar : o de a justiça , que condena os criminosos , ou o de os criminosos de o grupo Capitães de a Areia , que necessitam de essa vida de crimes para sobreviver ; é impossível não se comover com a situação de eles , nem de se ver " em cima de o muro " .
A escrita é fácil , fluente e recheada de gírias .
Com certeza , é o livro mais " fácil " requerido por o vestibular de a Fuvest/Unicamp .
Até essa data , meu livro favorecido , por a linguagem encantadora , poética .
Por os personagens vivos , reais , quase que posso vê - los andando por aí .
Por o cenário mágico , por a história que prende e comove , por me identificar com cada personagem , por reconhecer a minha pátria estampada em letras .
Um livro com alma brasileira , que envolve , enriquece , um livro que transforma que o lê .
O melhor
Olha se você não curti muito literatura , esse é um bom livro pois a historia não é tão chata como de costume e tem um enredo que deixa preso a historia
Retrato atemporal
Porque Jorge Amado criou um romance atemporal .
A obra exibe com perfeição o retrato social que provavelmente nunca se apagará de a sociedade capitalista - a desigualdade , o ódio e o desprezo entre as camadas sociais .
Mas , com o passar de a obra e com o passar de Dora por o trapiche , notamos o caráter humano de os meninos de rua necessitados de compreensão - destaque notável a a história de Sem-Pernas - .
Esse é um de os poucos livros que realmente me emociona , é recheado de sentimento a cada página , e Jorge Amado aborda muito bem as várias facetas de a complexidade humana .
Livro batuta ; -
Um livro muito bem escrito de uma historia muito emocionante , eu me sentia uma capitã de areia já , como a Dora ... sofri junto com o Pedro bala em o reformátorio , torci para que o sem pernas ficasse em a casa de Ester e o encanto de o livro é esse , é como ele te prende em a historia , como ele te envolve , eu adorei ... um livro que eu recomendo é realmente muito bom .
Capitães de areia é um romance carregado de o lirismo de Jorge Amado .
Meu pai me presenteou pouco antes de falecer e guardo um especial carinho por este livro .
Contando a história de um grupo de meninos de rua de a Bahia o autor mergulha em um mundo de abandono , revolta social e ainda conta o nascimento de o amor de Pedro Bala e Dora carregado de o desejo carnal e muita ternura .
é um livro muito bonito , onde cada um de o gruto tem sua historia .
muito bem escrito , com uma leitura simples e facil .
A história em si é ótima .
Embora fiquei cansativo de lê - lo em algumas partes , em geral o livro é muito bom de ser lido .
Surpresa boa
Comprei o livro por ser de a lista de livros de a escola , e como já de praxe esperava um livro maçante como a maioria de os livros que sou " obrigada " a ler em a escola , mas me surpreendi com uma leitura muito agradável , adorei o livro e pretendo ler outros de o Jorge Amado .
" Capitães de a areia " é batuta // hahaa
Apesar de ter achado um pouco chato em o começo , confesso que mudei completamente de opnião sobre este livro espetacular .
Indubitavelmente é um clássico de a literatura brasileira e um livro para toda a vida .
Diferente de as tantas outras vezes , a o terminar de lê - lo fui tomado por uma sensação estranha ; indescritível .
Uma mistura de tristeza , impotência e a o mesmo tempo muito pesar por haver terminado a leitura - grande seria minha alegria se o livro ostentasse a o menos duzentas páginas a mais .
Obviamente me senti transformado após apreciar uma obra tão grandiosa .
A narrativa é desenvolvida com maestria por Jorge Amado , fazendo com que a leitura flua de maneira bem agradável .
Somos arrebatados desde o início por a representação gramatical de o linguajar malandro de os meninos , por as diferenças de personalidade presentes em cada um e é claro , por a variação constante de cenas - sem dúvida uma de as características mais importantes de o enredo .
A obra não fica estagnada em o mesmo ponto e prima por o dinamismo trazido por as aventuras de os Capitães de a Areia .
Uma obra perene , sem dúvidas .
Em começo , o livro traz uma maneira de descrever os " Capitães de Areia " , grupo de crianças abandonadas que precisam furtar para sobreviver , de um jeito inédito em o meu ver ; apresentam as públicações de um Jornal sobre eles , assim como relatos de pessoas de a sociedade tanto sobre as marginalidades de o grupo como as questões que levam eles a fazerem isso .
Em suma , é um ótimo livro , faz - nos conhecer a realidade de algumas pessoas que ainda existem em o mundo , assim como a necessidade de elas , que é mais de carinho .
Recomendo bastante , prende o leitor , nao consegui parar de ler quando comecei ; D
um de os melhores livros !
O livro torna o leitor íntimo de as pequenas criaturas , cada uma de elas com suas carências e ambições - de o líder Pedro Bala a o religioso Pirulito , de o ressentido e cruel Sem-Pernas a o aprendiz de cafetão Gato , de o sensato Professor a o rústico sertanejo Volta Seca .
AMEI !
AMEI !
Me Surpreendeu !
