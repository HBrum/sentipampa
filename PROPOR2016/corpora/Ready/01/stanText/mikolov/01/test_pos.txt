Indubitavelmente , o melhor livro que já li .
Acho incrível a forma com a qual o livro é conduzido , apresentado e descrito .
Só posso dizer : quero ler de novo !
Interessante : a verdade nua e crua de os meninos de rua , um clássico que pode ser lido e contextualizado a a nossa realidade .
Me emocionei com todos os garotos e seus sonhos , todos mesmo .
Claro que me apaixonei por Dora também - garota que servia de tudo um pouco para eles , uma família que nunca tiveram .
Mas apesar de tudo , até que eu gostei d' este livro .
Li por obrigação , mas não foi uma leitura tão exaustiva como eu achei que seria .
Este livro não é ruim , como muitos pensam .
Parecia que eu estava ali , passando por tudo aquilo com os Capitães de a areia , como eu estive presente em várias outras cenas mais .
O livro é bastante interessante ... tem hora que parece baseado em fatos reais .
A narrativa , os dialógos e o final são muito legais .
Esse livro de Jorge Amadao é fundamental para compreendermos o comportamento de os jovens infratores .
Eu simplesmente amei a maneira carinhosa e detalhista que Jorge Amado descreve a rotina de essas crinças , seu personagem principal , Pedro Bala , é um menino sonhador , todos os personagens , em a minha opinião são extremamente carismáticos .
Esse livro me ajudou muito a compreender nossa sociedade , e principalmente o quanto devemos zelar e olhar por os menores .
Muito bom , recomendo !
Esse livro é tão bom que tenho medo de falar .
Fala serio esse livro e perfeito , muito bom , voce comeca a fazer parte de a historia sem querer e tomar as dores e as alegrias de essas crianacs sofridas e abandonadas ... Para pessoas que aceitam a vida como ela realmente e , e se conforma facil com certas situacoes ... Um livro maravilhoso ...
Recomendo !
Mas Capitães de areia é o único livro que eu recomendo .
É singelo e mostra a realidade de os meninos de rua de forma romântica , digamos assim .
Enfim , recomendo !
O primeiro livro que li de Jorge Amando , ainda em a minha adolescência , me deixou profundamente impressionado com duas coisas ; sua linguagem e a trama em si .
O livro já começa bem , de forma bem original com recortes de jornais - que deixam dúvidas sobre sua realidade - onde o leitor é apresentado a os capitães de areia e como a sociedade baiana se incomodava com sua existência , mas não fazia nada para resolver os seus problemas .
Outro destaque é os personagens , cada um mais bem construído que outro .
O Livro tocou profundamentemeu coração , nunca vou esquecer de Pedro Bala o lider de os meninos , o Querido João Grande tinha um coração de o tamanho de ele , e a Meiga Dora a '' mãe '' de todos . . eu sou Apaixonada por esse livro , E foi esse livro q me motivou a continuar a lendo ... E esse Livro sempre estara em a minha mente e coração !
Recomendadissiiimoooo para leitura !
A os 9 anos , não compreendia metade de as palavras usadas em o livro mas me apaixonei por Pedro Bala , um personagem que oscila entre o bem e o mal e por o Professor , por quem eu torci durante todo o livro para que tivesse um final feliz .
Chorei muito com as mazelas retratadas em o livro e , anos depois , relendo o texto com olhos adultos , descobri que Jorge Amado tinha um jeito só de ele de escrever e que eu gostava de isso .
De qualquer forma , mesmo com estes poréns , a leitura vale a pena .
Um ótimo livro para quem gosta de histórias voltadas a a realidade .
Livro excepcional e devaneante !
Em a verdade , só li pq era livro de vestibular - tenho pavor de livros indicados para o vestibular ... - , mas me supreendeu , tanto que me fez comover com as histórias de esses garotos de rua e até me emocionar com um de eles .
Um livro muito bom
Li quando estava em a oitava série , lembro - me de ter me impressionado muito com algumas cenas , mas gostei muito de a leitura , pois ela me envolveu de o início a o fim .
Continua sendo ate hoje um de os melhores livros que já li , a forma com que Jorge Amado retrata a vida em a Bahia , para mim é mais que perfeita - apesar de nunca ter sequer ido ate lá - , enfim , para alguém que nunca tinha pego um livro para ler , com uns 14 anos , ler esse em 1 dia e meio ... fez muita diferença em minha vida , e um dia vou reler essa ótima história .
5 estrelinhas merecidas !
Eu amei esse livro , não conseguia parar de ler * __ * Vc se envolve de uma maneira com os pias de o Capitães de a Areia , cada um tem sua história em especial que cativa mesmo o leitor .
Com certeza emocionante a história de Pedro Bala e seus amigos de o trapiche .
Muito bom !
O melhor de o nacional .
Esse é um livro muito bom , acho que de os nacionais é o meu preferido .
Ele conta história de os meninos de rua , que povoam e dominam Salvador de forma espetacular , realística e a o mesmo tempo com algo a mais em o ar .
Sou muito suspeita pra falar de ele , pois este foi um de os livros que mudaram a minha vida .
Gostei .
Uma história tocante que em os mostra o cotidiano de os meninos de rua .
O romance é escrito de uma forma bem tocante e com cenas muito fortes .
O tempo passa mais a história é sempre atual
Ótimo livro de Jorge Amado .
muito bom , Jorge Amado estava inspirado a o escrever a história de esse meninos de rua , crítico em a medida certa e bem interessante ...
Quando eu conheci axei q era rum por o fato de ter preconceito de livros indicados em a época de escola , mas a o 1º capitulo eu já estava viciado !
Realmente um ótimo livro .
Com a força envolvente de sua prosa , Jorge Amado nos aproxima de esses garotos e em os contagia com seu intenso desejo de liberdade .
Adorei o livro .
É o tipo de livro que te prende de o início a o fim .
Engoli o livro rs ... Se pudesse madrugava lendo .
Achei um livro muito bom , levando em conta que quando tenho de ler algum por causa de provas/vestibulares já fico meio com o pé atrás .
É uma leitura agradável , só o que me incomodou mais foram as palavras repetidas várias e várias vezes a o longo de o mesmo parágrafo , por vezes frases inteiras .
Em o geral , realmente posso dizer que fiquei feliz com a surpresa de ter gostado de o livro .
A história te envolve de o início até o fim !
Um livro que realmente vale a pena ser lido .
Ótimo livro , narrativa fácil para os jovens e crianças .
Infelizmente os tais capitães de a areia , como eram chamados há quase um século , se multiplicaram por as metrópoles urbanas e já não são mais tão românticos e divertidos quanto os personagens de este livro .
Li este livro em dois dias , pois não conseguia me separar de as personagens de o mesmo por muito tempo .
É um livro muito bom .
Muito intenso .
Recomendo .
Para mim esse é o melhor livro de Jorge Amado !
Muito bom . .
Um excelente livro que narra a história real de o abandono de nossas crianças menos favorecidas e o destino que lhes é imposto .
excelente !
É UM LIVRO QUE VALE APENA LER .
Livro muito bom , retrata a vida de meninos em as ruas de Salvador e o desenvolvimento de os mesmos com o passar de o tempo .
Já li várias vezes , e vira e mexe me pego com ele em as mãos novamente .
Livro imortal .
Não sou Brasileira , mas arrepiou imenso o que li .
Enterneceu e apaixonou .
Mas voltando a o livro foi uma experiência marcante que gostei de ter .
E Capitães de areia é o meu preferido .
Não há como não se apaixonar por a história e , principalmente , por os personagens .
Um livro lindo , mostrando a triste realidade de crianças de rua ... Vale a pena ler !
; D
Muito bom o livro ... eu li já tem algum tempo , foi o primeiro livro de o Jorge que eu li e me apaixonei por o jeito bem natural de ele de escrever ... depois de isso li vários outros livros todos seguindo a mesma linha !
Recomendo !
Mais um livro de denúncia social , muito bem escrito por sinal .
Você se apaixona por os personagens de o livro , por as suas imperfeições .
Adorei o livro !
Em a minha modesta opinião , um de os melhores livros de Jorge Amado .
Uma historia linda de garotos criados em a beira de o cais .
Uma lição de amor , amizade , paixão , companheirismo , em o meio de uma politica regional .
Impossivel perder uma leitura como esta .
Recomendo .
Crepúsculo poderia ser como qualquer outra história não fosse um elemento irresistível : o objeto de a paixão de a protagonista é um vampiro .
Assim , soma - se a a paixão um perigo sobrenatural temperado com muito suspense , e o resultado é uma leitura de tirar o fôlego - um romance repleto de as angústias e incertezas de a juventude - oarrebatamento , a atração , a ansiedade que antecede cada palavra , cada gesto , e todos os medos .
Eleé lindo , perfeito , misterioso e , a a primeira vista , hostil a a presença de Bella ?
o que provoca em ela umainquietação desconcertante .
Li por acaso e gostei
Em o início , achei meio bobo essa coisa de vampiros e tal , mas depois fui lendo o livro e me envolvendo em o romance de Bella , a humana desajeitada e Edward , o vampiro bonitão .
Acho muito bom e um de os melhores livros de Stephenie Meyer .
Crepúsculo sinceramente me animou muito , comprei o livro quando soube que teria um filme - algumas vezes compro livros só por esse detalhe , mas é em a minoria - .
Crepúsculo me encantou muito , porque os romances em a maioria de as vezes eram clichês , mas esse surpreendeu um pouco .
A maioria de os personagens que são apresentados para nós são perfeitinhos demais , mas ainda acho que Crepúsculo é um '' conto de fadas meio-gótico '' então deixo essa hipótese de personagens perfeitos como um lado positivo e não negativo de a escrita e história .
O personagem que mais me conectei foi com o Charlie , porque ele foi o mais normal possível ... Como um pai .
Um conto de fadas - sim , ainda tenho esta ideia - adorável , e absurdamente cheio de suspense com um final louco - em o bom sentido - e lindo .
Crepúsculo foi uma obra que marcou muito a minha vida - e não tenho vergonha de admitir , eu gosto sim de a saga - .
E sim o livro é bem melosinho , eu que não gostava de nada assim acabei me redimindo e me entregando a esse romance que contém aquela parte que ambos os sexos o nós todos bem sabemos '' A atração por aquilo que nos é negado '' .
Leia '' Crepúsculo '' e conheça mais um pouco sobre os mais profundos instintos humanos , aqueles que nos impulsionam e nos revolucionam e nós nem percebemos .
Se entregue a está belíssima história de amor .
Bonzinho ...
É um lindo romance , uma grande ficção , uma grande aventura , uma grande literatura juvenil , enfim ... É pra todos os gostos !
Stephanie Meyer reúne em sua obra cinco elementos que chamam á atenção de o público teen ultimamente : enredo intrigante , personagens bem construídos , cenários de o cotidiano , dramas adolescentes e uma narração misteriosa e bem amarrada .
Os diálogos são bem colocados e ás perguntas de Bella são comuns , assim como suas teorias .
Acho interessante Stephenie ter criado vampiros '' herbívoros '' , que se alimentam somente de animais .
O livro é tudo de bom .
Este livro encantou muita gente por qual motivo não iria me encantar ?
Ele realmente me cativou .
Eu gostei muito , mais uma vez a Stephenie Meyer é maravilhosa .
Adoreei o livro e recomendo .
o que eu achei de o livro e maravilhoso e totalmemte sentimental cada palavra e situaçoês quere lemos em o livro e um história que eu ler e pude sentir tudo que estava lendo o que eu aprende com ele e que não inporta o que você er o fais se você amar tem que luta por esse amor masmo que nao valer apena em o final e algum que eu achei lindo em essa historia foi entao pouco tempo a familia cullan ja contruior aquele amor cuidadoso e amoroso coma a mocinha de a historia essa historia mim chamo atençao por ser tao parecida com a minha vida -LSB- jessyka lwyza -RSB-
De todos os livros de a saga esse foi o que eu realmente fiquei presa em a magia de ele , é difícil alguém ler e não gostar .
É tao magico , todo o livro , os personagens , cada ponto cada vírgula é perfeito .
Gostei muito , quando eu li ainda não tinha o filme , e as histórias sobre vampiros não eram tão famosas , então eu realmente me apaixonei por o Edward Cullem .
Gostei porque é diferente de tudo que ja tinha visto sobre vampiros até aquele momento .
Recomendo ele principalmente pra quem só assistiu o filme , o livro é rico de detalhes , e você conhece melhor os personagens , a história é mais empolgante .
Se você for olhar só a parte romantica de o livro ele não é ruim - só o fato de a Bella ser meio desesperada por companhia etc e tal e não viver sem o Edward , o que é meloso demais pro meu gosto , até porque comecei a ler esse livro esperando sangue e vampiros de '' verdade '' - .
Mas é uma história bem diferente de tudo que já vimos e não deixa de ser incrível por o fato de ser um pouco melosa .
Para mim , uma ótima história com uma péssima adaptação cinematográfica .
Para aqueles que pensam como eu pensava esse livro merece seu tempo e sua atencao eh atemporal e nos ensina que nosso cerebro nao tem idade pra aprender sobre forca de vontade , doacao , superacao , generosidade , senso de familia , amizade e amor .
Me apaixonei por ele nao por sonhar com Edward , Jacob ou Bella mas por me dar de volta a ideia de que a vida eh o que fazemos de ela , voce soh tem que fazer o seu melhor e ela sera tao linda e completa como em os livros .
Meu Romance Favorito
Não existe romance melhor que este , sou muito apaixonada por esta coleção MARAVIILHOSA
Deixo claro que acho o livro interessante .
Gosto muito de alguns personagens - especialmente o Dr. Carlisle e o Charlie - .
Apesar de o casal , dá para se entreter com a estória .
Se curte um romance meloso vai gostar .
Crepúsculo é um livro bom , mas não excelente !
Em o entanto , uma história fascinante , personagens bem trabalhadas e uma dificuldade em largar o livro enquanto não o terminamos são alguns de os pontos de esse best-seller .
Em a verdade é muito complicado ler uma critica mesmo que ela não seja construtiva , e se referindo a saga Crepúsculo não vejo porque uma acusação tão forte de que seja ruim pois ele é escrito muito bem não foge de a ideia como diversos autores e tem uma linha de a raciocínio muito boa ; em relação a historia em si todos que pegamos o livros para ler já sabíamos que seria uma fantasia com romance e um romance lindo e ingenuo como o primeiro romance de todo o adolescente não concordo de que é ruim todos tínhamos uma ideia que seria uma historia com bastante utopia e baseado em o que já sabíamos esta fantástico pois a escritora soube muito bem conduzir o texto
A história é realmente interessante e cativante .
E a história é realmente muito boa .
Mas digo que vale apena experimentar , não é tão ridículo quanto parece , o filme REALMENTE FERROU com a história .
Sua narrativa é muito interessante , seu linguajar é ótimo e a estrutura de o livro é muito bem resolvida .
É uma mistura de o real com o sobrenatural , mas o que mais me impressionou é o fato de ser uma história de amadurecimento de a personagem e sua coragem para viver a sua história de amor .
Mas , para meu espanto , até que não foi tão ruim assim .
A autora cria uma trama epleta de segredos , paíxões e elementos fantásticos , gerando assim uma obra sensual e atraente .
Simplesmente Amei o livro muito bom !
Muito bom !
Admito que é bem pueril , mas é viciante !
Indico !
Li em uma semana , ele conseguiu me prender bem , pois queria saber quais partes não tinham em o filme , e isso fez com que eu acabasse ele rápido .
A história de Alice e Carlisle também me fascinaram .
É um livro impossível de ser largado .
Muito bem escrito .
Um livro-fenômeno .
Um romance de tirar o fôlego .
Eu achei uma aventura . . rs
Tudo mudou , começando por eu me apaixonar por o personagem que antes eu desprezava , Edward .
Edward é encantador , suas qualidades e a intensidade de seu amor faria qualquer garota escolhe - lo .
O livro é bom , pra quem gosta de fantasia e romance .
Melhor Livro
eu li em 2 dias !
Agradavel .
Perfeito !
Assim que comecei a ler já fiquei maravilhada .
Super recomendado !
O livro é bem escrito e de leitura fácil .
Porém , eu achei as idéias originais , interessantes e válidas .
Enfim , gosto de o livro e recomendo .
Nunca gostei de histórias de ficcção nem em livros nem em filmes , comecei a ler a Saga Crepúsculo por insistência de uma amiga , e fiquei totalmente viciada em a história !
Quando eu li por a primeira vez , eu me apaixonei por tudo , eu vivia os momentos , me prendia e não conseguia parar de ler .
Hoje eu sei reconhecer que o livro é mal estruturado , mas mesmo assim , acho a história excelente , os personagens são bem formulados , os detalhes .
Uma história de amor encantadora ...
A leitura de ele é simples , flui de maneira muito fácil , apesar de as vezes eu achar que algumas palavras muito grandes poderiam ser cortadas .
Mas se você é mente aberta e não tem problemas com isso , bom aí deixe - se levar até a chuvosa Forks , o que pode ser uma viagem muito legal .
Eu gosto de a Bella .
Em os últimos capítulos a ação em a história deixa tudo muito bom e mostra como a Bella pode sim ser muito inteligente , enganando o vamp Jasper .
De zero a dez , minha nota para o primeiro volume de essa série é 9 , fico por aqui com essa resenha e espero que você tenha curtido !
Gostei de a história em si , ela me prendia , mas toda essa descrição me cansava .
Sou fã !
É um livro que te prende desde o início , faz você se apaixonar por os personagens .
* - * Super Legal * - *
Eu achei ele um maximo ... Irado . .
Um livro mtoo dinâmico e qando vc percebe ele ja acabou .
Vampiros viciantes
Stephenie em os apresenta uma leitura envolvente e encantadora , que prende a atenção de os leitores de o começo a o fim e que deixa sempre um gostinho de quero mais .
O livro é bem escrito .
Ficar preso a uma só visão , sem aquela onisciência de o narrador em terceira pessoa , captura a atenção de o leitor com muita eficiência .
Stephenie Meyer soube fazer excelente uso de este recurso .
Gostei de a relação de ela com o pai , um misto de estranheza , delimitação de território e camaradagem silenciosa .
A autora soube despertar a curiosidade de os leitores , com cenas cheias de suspense , embora de certa previsibilidade .
Não é uma obra prima - e duvido que Stephenie Meyer tivesse esta pretensão - mas é um livro interessante para ler em um dia de folga .
O livro nos remete a uma boa leitura pra quem gostar de um mundo ' su-real ' com alguns questionamentos de a adolescência .
achei interessante . . mais prefiro o filme !
Apesar de todas as críticas , Crepúsculo não é um livro de todo , ruim , é apenas mediano , de aquele que não fica em a memória muito tempo após se ler , sabe , aquelas histórias que você esqueceria , se não fosse por o exageiro '' uma marco em a literatura infanto-juvenil . ''
Apesar de tudo isso , Crepúsculo se for visto como um romance - e não uma história de vampiros - não é tão repulsiva assim , e pode até ser lido até o fim sem problemas , falo de o primeiro livro , os outros já são outra história .
Por outro lado , a narrativa te impulsiona a ler o que se tem em as mãos , a história pode não ser a melhor já inventada , mas o modo com que ela é colocada é muito boa , se não fosse por o exageiro em as palavras complicadas , a narrativa seria exelente .
Esse livro eu amo , mostra muito mais que o filme além de ver como tudo começou e de mostrar coisas que em o filme não é possível notar .
Bem , um de os pontos fortes de crepúsculo são os detalhes , a autora descreve tudo minimamente desde o clima de a cidade e seu aspecto chuvoso A as roupas de os colegas de Bella e que por falar em ela mesmo aparentando ser '' sonsa '' é dona de uma personalidade forte e uma coragem incrível além de ser uma crítica de primeira mão , e é aí que estão os detalhes : Bella chega em um lugar e o descreve completamente as cores de as cortinas e o material de o carpete , e quem lê as entrelinhas percebe isso , uma Bella meio orgulhosa , acho que essa não é a palavra certa mas foi a única que me veio a cabeça , e egoísta que quer Edward mesmo sabendo que ele sofre por estar a o seu lado , sofre tendo que se controlar mas ela simplesmente quer ele e acredita que ele vai conseguir se controlar mas o problema está aí , e se ele não conseguir ?
Botando tudo em a balança as qualidades de a personagem superam os defeitos , muitos criticam o fato de Bella abandonar família , amigos e escola para viver um romance com seu vampiro mas quem nunca pensou em largar tudo e fugir com '' aquela '' pessoa que a seu ver era perfeita ?
Finalmente , o livro é ótimo , uma leitura fácil , uma linguagem interessante e os diálogos são cativantes te fazendo querer saber mais sobre a vida de o personagem menos importante .
Indico para qualquer um que queira fantasiar com um amor impossível ou até meio possível para alguns que acreditam e vou lhes confessar que eu não me surpreenderia se aparecesse em a minha frente um vampiro em um volvo prata me chamando pra dar um volta por as campinas de Forks .
Bem , quando o li o livro de a saga eu pensei : PERFEITO .
A o todo o livro é ótimo , é bem explicativo e acho que a Stephanie Meyer soube ser bem objetiva em a hora de exemplificar algumas partes de o livro o que facilitou o entendimento .
Enfim , o que salva são os acontecimentos durante o desenvolver de o livro .
Mas , se quer minha opinião eu gostei bastante de o livro .
Bastante mesmo apesar de os defeitos .
Agora se recomendação é o que estavam esperando , eis aqui sua resposta , recomendo !
Mas foi ele que me impulsionou a ler com frequência , e ter gosto por a coisa .
Crepúsculo poderia ser como qualquer outra história não fosse um elemento irresistível : o objeto de a paixão de a protagonista é um vampiro .
Assim , soma - se a a paixão um perigo sobrenatural temperado com muito suspense , e o resultado é uma leitura de tirar o fôlego - um romance repleto de as angústias e incertezas de a juventude - o arrebatamento , a atração , a ansiedade que antecede cada palavra , cada gesto , e todos os medos .
Nunca pensei que gostaria de Crepúsculo , porque sinceramente , não apostava que um livro que agradou os adolescentes pudesse me fazer cócegas .
Só que me surpreendi e passei a ver alguns motivos muito sólidos para continuar a leitura .
Os capitulos são de uma beleza incomum .
O livro certo para quem gosta de sonhar com um amor impossivel !
foi com CREPÚSCULO que descobri que gostava de ler ... depois de escrever ... E ler mais ainda .
Sinceramente , se a saga se resumisse apenas á esse livro , seria excelente , porque esse foi o único livro de a saga que eu gostei , tem um ar sombrio e um mistério que envolve todo o enredo de a história , é uma boa história que prende o leitor de o começo a o fim .
Este livro vária pessoas já fizeram resenha , é uma típica historia de a menina e o vampiro mas é muito boa !
É uma historia eletrizante , vista por o ponto de vista de Bella Swan .
Crepúsculo mistura ficção , mito , romance proibido e atração incomum que nos faz pirar antes mesmo de virar a última página .
Mas sim , porque realmente é interessante a forma como a autora transforma a forma de o mito em uma versão possível - que permanece com você até mesmo em seus sonhos .
Eu aconselho todos a lerem , pois eu o fiz muitas vezes e não me arrependo .
É fácil de entender , envolvente e arrebatador .
A capa é linda , sugestiva , e a sinopse prometia .
Eu simplesmente me apaixonei por cada página , por cada fala , por cada personagem .
Eu gosto de romances , gosto de suspense , gosto de coisas não tão reais , e esse livro me proporcionou tudo isso .
Independente de qualquer coisa , em a minha opinião , Crepúsculo é um livro apaixonante .
NÃO TEM COMO NÃO AMAR o Edward , menino a a moda antiga que valoriza o amor verdadeiro , que se entrega de corpo e alma a o relacionamento , que coloca a sua namorada em primeiro lugar .
RECOMENDO O LIVRO PARA PESSOAS QUE AINDA ACREDITAM EM O AMOR , mesmo que ele seja de o tipo '' melequento e clichê '' .
Tremendamente sedutor , Crepúsculo mantém seus leitores ligados até a última página .
Eu gostei de o livro .
É meio água com açucar , mas eu recomendo .
Mas não um vampiro de o tipo Lestat ou Conde Drácula e sim um vampiro bonzinho , fofo , romântico , estonteantemente lindo - não que o Lestat não fosse , né ?
Bom muita gente já assistiu o filme Crepusculo e as vezes nem tem interesse de ler o livro , por o fato de já conhecer a história , mas sinceramente vale muito a pena tu ler o livro , além de ser totalmente diferente de o filme , o livro é intenso e muito interessante .
Vale muito a pena ler ok ?
Endosso o fato de que deve haver primeiramente a valorização de a autora , pois se trata de uma narrativa demasiadamente criativa - isso é fato - , poucos teriam a capacidade de fazê - lo de forma competente .
Crepusculo é um livro para ser preciado , para mergulhar e aceitar toda irrealidade que o cerca .
Por fim , devo dizer que recomendo a leitura .
A historia e muito boa e contada de uma maneira muito interessante .
Um belo romace escrito por Stephenie Meyer , recomendo a todos que amam romances com um pouco de ficção .
Enfim não vou dizer que Crepúsculo é uma bosta , porque eu gosto de Crepúsculo mais também tenho senso critico e sei que não é um livro muito bom .
Mais recomendo por causa de o amor envolta de a história que é realmente muito lindo .
A história em si é um enorme clichê como disse em o início de a crítica , o vocabulário não é algo avançado como em Nárnia , Alice ou O Morro dos Ventos Uivantes , sendo fácil de ler o livro te cativa , te chama para lê - lo .
Bem legal , foi o primeiro livro que eu li de a minha estante , foi graças a ele que me despertou o sublime desejo por a ' ' literatura estrangeira ' ' Enfim , um livro bom .
Fantástico . .
Um livro ótimo , totalmente fantástico assim como todos os outros , Meyer nos transporta para um novo mundo , ela criou vampiros diferentes , com a capacidade de amar , e nos fazer amar o sobrenatural em si .
Recomendo !
Nunca tinha me interessado por vampiros antes , e achei o enredo de crepúsculo fantástico , diferente , uma coisa que realmente me prendeu .
O primeiro livro foi muito bom , pecou apenas em algumas partes extremamente melosas e com a personalidade super protetora de o Edward , que muitas vezes é muito chato .
Eu gostei de o livro .
Bom !
Em a minha opinião o livro é muito bom pra quem é amante de livros românticos com um toque de suspense .
Um livro maravilhoso que trouxe uma nova forma de se olhar para o mundo de os vampiros , através de Edward Cullen .
Ameii o primeiro livro ... Bella Swan muda - se de a ensolarada Phoenixpara a chuvosa cidade de Forks , Washington , para viver com seu pai , Charlie , o chefe de a polícia local .
É um bom livro , bem melhor de o que o filme !
Suspense , romance e ação permeiam este livro , para quem gosta de este tipo de enredo que mistura sobrenatural moderno com realidade é muito bom .
O início de o livro é bem interessante , pois conta um pouco de a história pessoal de Bella , Edward e de o próprio Jacob para podermos entender melhor o desenrolar de as ações .
Como a história é leve , te prende de o início a o fim e você fica querendo ler o próximo - já que é uma série - para querer saber o desenrolar de o triângulo amoroso que se inicia quase em o final de o livro .
Eu li , e confesso que gostei , mas hoje vejo que a historia é fraca e sem sal , não tem conteúdo ... sei lá !
Adorei o livro mesmo que digam que não é bom .
Ele é lindo , emocionante , envolvente , jamais nenhuma mulher ira ver os homens de a mesma forma ; por que dizer te amo , se você pode dizer : '' você é minha vida agora '' .
Um livro mágico e que atingiu o coração de milhares de pessoas sendo elas adolescentes , jovens e adultos .
Simplesmente magnifico .
Mas é um livro muito bem escrito , gostei bastante .
Mas foi uma ótima aquisição porque fiquei completamente apaixonada por ele e sempre indicava pra todas as pessoas que eu conhecia .
Me apaixonei não só por a história de amor entre Bella e Edward , mas também por o carisma de os outros personagens como Alice e Carlisle , irmã e pai de Edward .
Literatura Fantástica viciante .
Então por mais que seja uma leitura simples , boba ... eu me apaixonei por essa história e já li mais de 5 vezes esse livro .
Gosto de o diálogo de os personagens principais , gosto de sentir o que eles sentem , me colocar em o lugar de eles ... fabuloso !
É um livro rápido , de leitura fácil e agradável , faz você lembrar como é bom está apaixonado .
Uma boa pedida quando se tem tempo livre . .
Crepúsculo - diferente de o que alguns dizem - é muito bom - para os meninos preconceituosos , não me levem a mal , o livro é realmente bom - , mas também muito meloso .
Perfeito !
O melhor de a série , adorei e me apaixonei ... fui quase que obrigada a le - lo já que todas as minhas amigas já tinham lido e faziam a maior pressão rs , ainda bem : - que fui tão pressionada , me empolguei tanto que em a época li suas continuações em tempo recorde . . .
Gostei .
Tem 355 páginas de puro romantismo , o mais doce de eles .
Ler Crepúsculo é embarcar em uma narrativa longa e bem detalhada .
Particularmente gosto de o fato de a personagem seja um pouco de cada garota , mas o insegura , comum , desastrada , interessante , madura , independente - apesar de se tornar dependente de Edward - .
É um bom livro ...
