#!/bin/bash

for i in `ls $2`
do
	for j in `ls $2/$i`
	do
		for k in `ls $2/$i/$j`
		do
			SUBSTRING=$(echo $k| cut -d'.' -f 1)

			if [ -d "$2/$i/$j/$SUBSTRING" ]
			then
				rm -rf $2/$i/$j/$SUBSTRING	
			else
				echo "$2/$i/$j/$SUBSTRING"
				python $1 $2/$i/$j/$k $2/$i/$j $SUBSTRING
			fi

		done
	done
done

