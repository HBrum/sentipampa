#!/bin/bash
#The order matters!!!!!!
#FOLDER POS NEG NEU... or FOLDER POS NEG


if [ ! -d "$1" ]
then
	mkdir $1	
fi

if [ -d "$1/mikolov" ]
then
	rm -rf "$1/mikolov"	
fi

mkdir $1/mikolov/

for i in `seq 0 9`
do
	mkdir $1/mikolov/0$i

	#Positive Test
	cat $2/0$i/test.tree >> $1/mikolov/0$i/test_pos.txt
	
	#Negative Test
	cat $3/0$i/test.tree >> $1/mikolov/0$i/test_neg.txt
	
	#Neutral Test
	if [ "$#" -eq 4 ]; then
		cat $4/0$i/test.tree >> $1/mikolov/0$i/test_neu.txt
	fi
	
	#Positive Train	
	cat $2/0$i/train.tree >> $1/mikolov/0$i/train_pos.txt
	
	#Negative Train
	cat $3/0$i/train.tree >> $1/mikolov/0$i/train_neg.txt
	
	#Neutral Train
	if [ "$#" -eq 4 ]; then
		cat $4/0$i/train.tree >> $1/mikolov/0$i/train_neu.txt
	fi

done


