#!/bin/bash
#The order matters!!!!!!
#FOLDER POS NEG NEU... or FOLDER POS NEG

if [ ! -d "$1" ]
then
	mkdir $1
fi

if [ -d "$1/naive" ]
then
	rm -rf "$1/naive"	
fi

mkdir $1/naive/


for i in `seq 0 9`
do
	mkdir $1/naive/0$i

	#Positive Test
	cat $2/0$i/test.tree >> $1/naive/0$i/test.tree
	N=`wc -l $2/0$i/test.tree|cut -d' ' -f 1`
	for j in `seq 1 $N`
	do
		echo + >> $1/naive/0$i/testPol.tree
	done

	#Negative Test
	cat $3/0$i/test.tree >> $1/naive/0$i/test.tree
	N=`wc -l $3/0$i/test.tree|cut -d' ' -f 1`
	for j in `seq 1 $N`
	do
		echo - >> $1/naive/0$i/testPol.tree
	done

	#Neutral Test
	if [ "$#" -eq 4 ]; then
		cat $4/0$i/test.tree >> $1/naive/0$i/test.tree
		N=`wc -l $4/0$i/test.tree|cut -d' ' -f 1`
		for j in `seq 1 $N`
		do
			echo O >> $1/naive/0$i/testPol.tree
		done
	fi
	
	#Positive Train	
	cat $2/0$i/train.tree >> $1/naive/0$i/train.tree
	N=`wc -l $2/0$i/train.tree|cut -d' ' -f 1`
	for j in `seq 1 $N`
	do
		echo + >> $1/naive/0$i/trainPol.tree
	done
	
	#Negative Train
	cat $3/0$i/train.tree >> $1/naive/0$i/train.tree
	N=`wc -l $3/0$i/train.tree|cut -d' ' -f 1`
	for j in `seq 1 $N`
	do
		echo - >> $1/naive/0$i/trainPol.tree
	done
	
	#Neutral Train
	if [ "$#" -eq 4 ]; then
		cat $4/0$i/train.tree >> $1/naive/0$i/train.tree
		N=`wc -l $4/0$i/train.tree|cut -d' ' -f 1`
		for j in `seq 1 $N`
		do
			echo O >> $1/naive/0$i/trainPol.tree
		done	
	fi


done


