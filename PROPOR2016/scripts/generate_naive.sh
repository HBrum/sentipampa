#!/bin/bash
#Usa ./generate_naive.sh <PYTHON PATH> <FOLDER>

for i in `ls $2`
do
	for type in `ls $2/$i`
	do
		echo 'mkdir '$2'/'$i'/'$type'/naive/model'
		for tc in `ls $2/$i/$type/naive/`
		do
			echo 'python '$1' -train '$2'/'$i'/'$type'/naive/'$tc'/train.tree '$2'/'$i'/'$type'/naive/'$tc'/trainPol.tree '$2'/'$i'/'$type'/naive/model/naive_'$tc'.model &> '$3'/naive_'$i'_'$type'_'$tc'.log'
		done
	done
done

for i in `ls $2`
do
	for type in `ls $2/$i`
	do
		echo 'mkdir '$2'/'$i'/'$type'/results'
		for tc in `ls $2/$i/$type/naive/model/`
		do
			TEST=`echo "$tc"|cut -d'_' -f 2|cut -d'.' -f 1`
			echo 'python '$1' -run '$2'/'$i'/'$type'/naive/model/'$tc' -inputFile '$2'/'$i'/'$type'/naive/'$TEST'/test.tree -toFile '$2'/'$i'/'$type'/results/'$i'_'$type'_'$tc'.res'
		done 
	done
done
