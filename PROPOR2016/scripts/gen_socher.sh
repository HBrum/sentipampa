#!/bin/bash

if [ ! -d "$1" ]
then
	mkdir $1
fi

if [ -d "$1/socher" ]
then
	rm -rf "$1/socher"	
fi

mkdir $1/socher/

for i in `seq 0 9`
do
	mkdir $1/0$i
	cat $2/0$i/test.tree >> $1/socher/0$i/test.tree
	cat $3/0$i/test.tree >> $1/socher/0$i/test.tree
	if [ "$#" -eq 4 ]; then
		cat $4/0$i/test.tree >> $1/socher/0$i/test.tree
	fi
	
	cat $2/0$i/train.tree >> $1/socher/0$i/train.tree
	cat $3/0$i/train.tree >> $1/socher/0$i/train.tree
	if [ "$#" -eq 4 ]; then
		cat $4/0$i/train.tree >> $1/socher/0$i/train.tree
	fi
done

