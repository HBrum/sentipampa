#!/bin/bash


for i in `ls $2`
do
	for type in `ls $2/$i`
	do
		for tc in `ls $2/$i/$type/socher/`
		do
			echo 'mkdir '$2'/'$i'/'$type'/socher/model'
			echo 'java -cp "'$1'/*" -mx8g edu.stanford.nlp.sentiment.SentimentTraining -maxTrainTimeSeconds 0 -trainPath '$2'/'$i'/'$type'/socher/'$tc'/train.tree -train -model '$2'/'$i'/'$type'/socher/model/model_'$tc'.ser.gz &> '$3'/socher_'$i'_'$tc'.log &'
		done
	done
done

