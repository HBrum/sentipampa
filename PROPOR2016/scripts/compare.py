#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

"""
	python compare.py <GOLD> <GUESS> <NAME_OF_FILE> <OUTPUT:statistics>
"""



def main():
	argv = sys.argv
	
	goldF  = open(argv[1],"r")
	guessF = open(argv[2],"r")
	name = argv[3]
	outputF = open(argv[4],"w")
	
	vecPol = []
	c = {}
	for l in goldF:
		pol = l.strip()
		if pol not in vecPol:
			vecPol.append(pol)
	goldF.close()
	
	for pol in vecPol:
		c[pol] = []
		for i in vecPol:
			c[pol].append(0)
	
	acc = 0
	err = 0
	goldF = open(argv[1],"r")
	for line in guessF:
		gu = line.strip()
		go = goldF.readline().strip()

		c[gu][getVecIndex(vecPol,go)] += 1
		if gu == go:
			acc += 1
		else:
			err += 1
	
	outputF.write("File: " + name+"\n\n")
	
	
	outputF.write("Hit : " + str(acc) + "\n")
	outputF.write("Miss: " + str(err) + "\n")
	acuracy = 0.0
	acuracy = float(acc)/(float(acc+err))
	outputF.write("Acc : " + str(acuracy) + "\n")
	outputF.write("\n    Confusion Matrix\n")
	outputF.write("\t")
	for cl in c:
		outputF.write(cl + "\t")
	outputF.write("\n")
	outputF.write("---------------------------------\n")
	for cl in c:
		line = str(cl) + "\t"
		for p in range(0,len(c[cl])):
			line += str( c[cl][p] ) + "\t"
		outputF.write(line + "\n")
		
	outputF.write("---------------------------------\n")
	
	printline = ""
	printline = name + ";" + str(acc) + ";" + str(err) + ";" + str(acuracy) + ";"
	for cl in c:
		for p in range(0,len(c[cl])):
			printline += str( c[cl][p] ) + ";"
	
	for cl in c:
		printline += str( cl ) + ";"
	
	print printline
def getVecIndex(vec, element):
	count = 0
	for i in vec:
		if element == i:
			return count
		count += 1

if __name__ == "__main__":
	if len(sys.argv) < 5:
		print "Not enought parameters"
	else:
		main()
