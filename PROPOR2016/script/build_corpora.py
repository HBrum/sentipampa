#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

def main():
	fin = open(sys.argv[1],"r")
	fout = open(sys.argv[2],"w")
	
	for l in fin:
		tb = Treebank(l.strip())
		fout.write(tb.writeSentence().strip()+"\n")
	
	fin.close()
	fout.close()

if __name__ == "__main__":
	execfile("ptbMng.py")
	main()
