#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys


def polaritiesChange(node, wList, pList, score):
	""" Write only the bottom of the tree that is the sentence withou tags (it's basically a stack implementation) 
		Actually now it writes polarities in every node non-leaf of the tree. :P
	""" 
	
	polar = 2
	if pList[0] == "+":
		polar = 3
	if pList[0] == "-":
		polar = 1
	
	i = 0
	
	stack = []
	stack.append( node.getNode() )
	while len(stack) is not 0:
		n = stack.pop()
		if n.isBottom():
#			n.getParent().setValue(pList[i])
			n.getParent().setValue( str(polar) )
			
		else:
			#n.setValue(pList[0])
			n.setValue( str(polar) )
			for c in n.getChildren():
				stack.append(c)
	
	return node




def main():
	
	"""Lê os argumentos do algoritmo"""
	if len(sys.argv) < 2:
		print "Too few parameters."
		exit()
	
	
	""" """
	inPol  = open(sys.argv[1]+"pol.txt", "r")
	inSco  = open(sys.argv[1]+"sco.txt", "r")
	inText = open(sys.argv[1]+"text.txt", "r")
	inTree = open(sys.argv[1]+"tree.txt", "r")
	
	outTree = open(sys.argv[1]+"output.txt", "w")
	#Remove it
	c = 0
	for line in inTree:
		
		if line.strip() == "(())":
			polarities = inPol.readline().split(" ")
			texts = inText.readline().split(" ")
			scores = float( inSco.readline().strip() )
			print "Error at " + str(c)
			continue
		
		tb = Treebank( line )
		#tb.draw()
		
		#Binarizing the Trees
		print "Changing tree: " + str(c)
		arv = toTree(tb.top)
		Tree.chomsky_normal_form( arv )
		arv = toTB(arv)
		tb.setTop(arv)
		
		
		
		polarities = inPol.readline().split(" ")
		texts = inText.readline().split(" ")
		scores = float( inSco.readline().strip() )
		
		tb.getNode().setValue(polarities[0])
		
		#tb.draw()
		newTree = polaritiesChange(tb, texts, polarities, scores)

		#newTree.draw()
		#print c,
		#print newTree.printTb()
		outTree.write(newTree.printTb()+"\n")
		#print "\n"
				
		#if c > 5:
		#	break
		
		#remove it
		c+= 1

	inPol.close()
	inSco.close()
	inText.close()
	inTree.close()
	
	outTree.close()

if __name__ == "__main__":
	""" O script ptbMng.py possui as chamadas para a biblioteca nltk."""
	execfile('ptbMng.py')
	
	main()
