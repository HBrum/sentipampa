#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

argv = sys.argv

def main():
	
	verbose = False
	
	#Ativando verbose mode
	if "-v" in argv:
		verbose = True
		argv.remove("-v")
		if len(argv) < 3:
			print "Erro de parametros."
			exit()
	
	#Leitura do corpus e escrita do arquivo de sentenças em texto puro
	dictionary, maxCounter = readReli(argv[1], argv[2]+"_text.txt")
	if verbose:
		print("Arquivo " + argv[2] + "_text.txt criado com sucesso.")


	outputPol = open(argv[2]+"_pol.txt","w")
	outputSco = open(argv[2]+"_sco.txt","w")
	
	#Contadores para polaridades de sentenças
	pos = 0
	neg = 0
	neu = 0
	
	#Escrita das polaridades e notas em arquivos
	for i in range(0,maxCounter):
		
		#Incremento da contagem de polaridades
		if dictionary[i][1][0] == "+":
			pos += 1
		else:
			if dictionary[i][1][0] == "-":
				neg += 1
			else:
				neu += 1
		
		outputPol.write( formSentence(dictionary[i][1])  + "\n")
		outputSco.write( str(dictionary[i][2]) + "\n")
	
	outputPol.close()
	outputSco.close()

	if verbose:
		print("Arquivo " + argv[2] + "_pol.txt criado com sucesso.")
		print("Arquivo " + argv[2] + "_sco.txt criado com sucesso.")
		print("\nTotal de sentenças: " + str(maxCounter))
		print("\nSentencas Positivas :   " + str(pos) + " \t/ " + str( 100 * pos/maxCounter) + "%")
		print  ("Sentencas Negativas :   " + str(neg) + " \t/ " + str( 100 * neg/maxCounter) + "%")
		print  ("Sentencas Neutras   :   " + str(neu) + " \t/ " + str( 100 * neu/maxCounter) + "%")


def formSentence(listOfWords):
	"""
		Função formSentence
		
		Recebe uma lista de palavras e retorna uma string com a sentença separada por espaços em branco.
		Exemplo: ["Me", "chamo", "Carlos", "."] retornará "Me chamo Carlos ."
		
		parametros 	: lista com palavras
		retorno		: string com sentença
	"""
	sentence = ""
	
	for w in listOfWords:
		sentence += str(w) + " "
	return sentence[:-1]




def readReli(inputName, outputName):
	"""
		Função readReli
		
		Função que faz a leitura do arquivo de entrada, salva as informações de marcação e escreve o arquivo de sentenças em texto puro.
		
		parametros	: o nome do arquivo de entrada e do arquivo de sentenças
		retorno		: um dicionário com polaridades e notas para cada sentença do arquivo e o contador com o número de sentenças.
	
	"""
	
	inputFile 	= open(inputName, "r")
	outputFile 	= open(outputName, "w") 
	
	
	dic = {}
	wrtLine = []
	polarity = []
	
	score = 0
	
	counter = 0
	
	for line in inputFile:
		
		if "#Nota" in line:
			score = float(line[6:])
			continue
		if line[0] == "#":
			continue
		
		if len(line.strip()) < 1:
			if len(formSentence(wrtLine)) > 0:
				outputFile.write(formSentence(wrtLine) + "\n")
				dic[counter] = [wrtLine,polarity, score]
				
				polarity = []
				wrtLine = []
				counter += 1
			continue
		
		cutLine = line.split('\t')
		wrtLine.append(cutLine[0])
		polarity.append(cutLine[4])
		
			
	outputFile.close()
	inputFile.close()
	
	return dic, counter


""" Função de ajuda."""
def callHelp():
	print "Esse algoritmo busca a leitura do córpus ReLi e a geração de outros 3 arquivos - um com a sentenças em texto puro, outro com as polaridades associadas e outro com as notas de cada resenha."
	print "\n\n Por gentileza, retire a primeira sentença "
	print "\n	Algumas opções:"
	print "\n	     -v (verbose)	- Vai mostrar informações sobre o córpus."
	print "\n	     -help (ajuda)	- Mostra esse texto de novo. :| "
	print "\nComo usar:"
	print "\npython reading_reli.py reli.txt <-v> PREFIXO"
	print "\nExemplo:"
	print "\npython reading_reli.py reli.txt -v reli_Amado"
	print "\nO algoritmo irá ler o corpus reli.txt e irá gerar os arquivos reli_Amado_text.txt, reli_Amado_pol.txt e reli_Amado_sco.txt enquanto printa na tela informações sobre o córpus."



if __name__ == "__main__":
	
	if "-help" in argv or len(argv) < 3:
		callHelp()
		exit()
	main()
