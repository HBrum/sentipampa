#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, os

argv = sys.argv

dPath = argv[1]
fBerk = argv[2]
fStan = argv[3]




def main():
	
	""" CRIAÇÃO DE DIRETÓRIOS """
	dFull 			= criaDiretorio(dPath, "full")
	#All e posneg
	dAllFull 		= criaDiretorio(dFull, "all")
	dPosnegFull		= criaDiretorio(dFull, "posneg")
	#All Full
	dBerkAllFull	= criaDiretorio(dAllFull, "berk")
	dStanAllFull	= criaDiretorio(dAllFull, "stan")
	#All Full berk
	dSBerkAllFull	= criaDiretorio(dBerkAllFull, "socher")
	dMBerkAllFull	= criaDiretorio(dBerkAllFull, "mikolov")
	dNBerkAllFull	= criaDiretorio(dBerkAllFull, "naive")
	#All Full stan
	dSStanAllFull	= criaDiretorio(dStanAllFull, "socher")
	dMStanAllFull	= criaDiretorio(dStanAllFull, "mikolov")
	dNStanAllFull	= criaDiretorio(dStanAllFull, "naive")
	#Pos/Neg Full
	dBerkPosnegFull	= criaDiretorio(dPosnegFull, "berk")
	dStanPosnegFull	= criaDiretorio(dPosnegFull, "stan")
	#All Pos/Neg berk
	dSBerkPosnegFull	= criaDiretorio(dBerkPosnegFull, "socher")
	dMBerkPosnegFull	= criaDiretorio(dBerkPosnegFull, "mikolov")
	dNBerkPosnegFull	= criaDiretorio(dBerkPosnegFull, "naive")
	#All Pos/Neg stan
	dSStanPosnegFull	= criaDiretorio(dStanPosnegFull, "socher")
	dMStanPosnegFull	= criaDiretorio(dStanPosnegFull, "mikolov")
	dNStanPosnegFull	= criaDiretorio(dStanPosnegFull, "naive")

	dOver 			= criaDiretorio(dPath, "over")
	#All e posneg
	dAllOver 		= criaDiretorio(dOver, "all")
	dPosnegOver		= criaDiretorio(dOver, "posneg")
	#All Over
	dBerkAllOver	= criaDiretorio(dAllOver, "berk")
	dStanAllOver	= criaDiretorio(dAllOver, "stan")
	#All Over berk
	dSBerkAllOver	= criaDiretorio(dBerkAllOver, "socher")
	dMBerkAllOver	= criaDiretorio(dBerkAllOver, "mikolov")
	dNBerkAllOver	= criaDiretorio(dBerkAllOver, "naive")
	#All Over stan
	dSStanAllOver	= criaDiretorio(dStanAllOver, "socher")
	dMStanAllOver	= criaDiretorio(dStanAllOver, "mikolov")
	dNStanAllOver	= criaDiretorio(dStanAllOver, "naive")
	#Pos/Neg Over
	dBerkPosnegOver	= criaDiretorio(dPosnegOver, "berk")
	dStanPosnegOver	= criaDiretorio(dPosnegOver, "stan")
	#All Pos/Neg berk
	dSBerkPosnegOver	= criaDiretorio(dBerkPosnegOver, "socher")
	dMBerkPosnegOver	= criaDiretorio(dBerkPosnegOver, "mikolov")
	dNBerkPosnegOver	= criaDiretorio(dBerkPosnegOver, "naive")
	#All Pos/Neg stan
	dSStanPosnegOver	= criaDiretorio(dStanPosnegOver, "socher")
	dMStanPosnegOver	= criaDiretorio(dStanPosnegOver, "mikolov")
	dNStanPosnegOver	= criaDiretorio(dStanPosnegOver, "naive")

	dUnder			= criaDiretorio(dPath, "under")
	#all e posneg
	dAllUnder 		= criaDiretorio(dUnder, "all")
	dPosnegUnder		= criaDiretorio(dUnder, "posneg")
	#All Under
	dBerkAllUnder	= criaDiretorio(dAllUnder, "berk")
	dStanAllUnder	= criaDiretorio(dAllUnder, "stan")
	#All Under berk
	dSBerkAllUnder	= criaDiretorio(dBerkAllUnder, "socher")
	dMBerkAllUnder	= criaDiretorio(dBerkAllUnder, "mikolov")
	dNBerkAllUnder	= criaDiretorio(dBerkAllUnder, "naive")
	#All Under stan
	dSStanAllUnder	= criaDiretorio(dStanAllUnder, "socher")
	dMStanAllUnder	= criaDiretorio(dStanAllUnder, "mikolov")
	dNStanAllUnder	= criaDiretorio(dStanAllUnder, "naive")
	#Pos/Neg Under
	dBerkPosnegUnder	= criaDiretorio(dPosnegUnder, "berk")
	dStanPosnegUnder	= criaDiretorio(dPosnegUnder, "stan")
	#All Pos/Neg berk
	dSBerkPosnegUnder	= criaDiretorio(dBerkPosnegUnder, "socher")
	dMBerkPosnegUnder	= criaDiretorio(dBerkPosnegUnder, "mikolov")
	dNBerkPosnegUnder	= criaDiretorio(dBerkPosnegUnder, "naive")
	#All Pos/Neg stan
	dSStanPosnegUnder	= criaDiretorio(dStanPosnegUnder, "socher")
	dMStanPosnegUnder	= criaDiretorio(dStanPosnegUnder, "mikolov")
	dNStanPosnegUnder	= criaDiretorio(dStanPosnegUnder, "naive")

	"""	FIM DA CRIAÇÃO DE DIRETÓRIOS"""

	""" Listas de armazenamento de sentenças """

	listBerkTree = [[],[],[]]
	listBerkSent = [[],[],[]]
	listStanTree = [[],[],[]]
	listStanSent = [[],[],[]]
	
	""" Adicionando sentenças nas estruturas de armazenamento"""
	with open(fBerk) as fileSource:
		for line in fileSource:
			if line[1] == '1':
				listBerkTree[0].append(line.strip())
				listBerkSent[0].append(Treebank(line.strip()).writeSentence())
			if line[1] == '2':
				listBerkTree[1].append(line.strip())
				listBerkSent[1].append(Treebank(line.strip()).writeSentence())
			if line[1] == '3':
				listBerkTree[2].append(line.strip())
				listBerkSent[2].append(Treebank(line.strip()).writeSentence())
	
	with open(fStan) as fileSource:
		for line in fileSource:
			if line[1] == '1':
				listStanTree[0].append(line.strip())
				listStanSent[0].append(Treebank(line.strip()).writeSentence())
			if line[1] == '2':
				listStanTree[1].append(line.strip())
				listStanSent[1].append(Treebank(line.strip()).writeSentence())
			if line[1] == '3':
				listStanTree[2].append(line.strip())
				listStanSent[2].append(Treebank(line.strip()).writeSentence())
		
	#    ____   _   _   _     _
	#	|  __| | | | | | |   | |
	#	| |_   | | | | | |   | |
	#	|  _|  | | | | | |   | |
	#	| |    | |_| | | |_  | |_
	#	|_|    |_____| |___| |___| 
	#
	"""
		ALL
	"""
	
	## SOCHER
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dSBerkAllFull,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		for j in range(0,3):
			nTrees = len(listBerkTree[j])
			for k in range(0,nTrees):
				if k >= (i/10.0) * nTrees and k < ((i+1)/10.0) * nTrees:
					fTest.write(listBerkTree[j][k] + "\n")
				else:
					fTrain.write(listBerkTree[j][k] + "\n")
			
		fTrain.close()
		fTest.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dSStanAllFull,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		for j in range(0,3):
			nTrees = len(listStanTree[j])
			for k in range(0,nTrees):
				if k >= (i/10.0) * nTrees and k < ((i+1)/10.0) * nTrees:
					fTest.write(listStanTree[j][k] + "\n")
				else:
					fTrain.write(listStanTree[j][k] + "\n")
			
		fTrain.close()
		fTest.close()
	
	## NAIVE BAYES
	for i in range(0,10):
		dTenFold = criaDiretorio(dNBerkAllFull,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTrainPol = open(dTenFold + "/trainPol.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		fTestPol  = open(dTenFold + "/testPol.txt", 'w')
		for j in range(0,3):
			nSent = len(listBerkSent[j])
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					fTest.write(listBerkSent[j][k] + "\n")
					if j == 0:
						fTestPol.write("-\n")
					if j == 1:
						fTestPol.write("O\n")
					if j == 2:
						fTestPol.write("+\n")
				else:
					fTrain.write(listBerkSent[j][k] + "\n")
					if j == 0:
						fTrainPol.write("-\n")
					if j == 1:
						fTrainPol.write("O\n")
					if j == 2:
						fTrainPol.write("+\n")
			
		fTrain.close()
		fTrainPol.close()
		fTest.close()
		fTestPol.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dNStanAllFull,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTrainPol = open(dTenFold + "/trainPol.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		fTestPol  = open(dTenFold + "/testPol.txt", 'w')
		for j in range(0,3):
			nSent = len(listStanSent[j])
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					fTest.write(listStanSent[j][k] + "\n")
					if j == 0:
						fTestPol.write("-\n")
					if j == 1:
						fTestPol.write("O\n")
					if j == 2:
						fTestPol.write("+\n")
				else:
					fTrain.write(listStanSent[j][k] + "\n")
					if j == 0:
						fTrainPol.write("-\n")
					if j == 1:
						fTrainPol.write("O\n")
					if j == 2:
						fTrainPol.write("+\n")
			
		fTrain.close()
		fTrainPol.close()
		fTest.close()
		fTestPol.close()
	
	
	## MIKOLOV
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dMBerkAllFull,str(i))
		fTrainNeg = open(dTenFold + "/trainNeg.txt", 'w')
		fTrainNeu = open(dTenFold + "/trainNeu.txt", 'w')
		fTrainPos = open(dTenFold + "/trainPos.txt", 'w')
		
		fTestNeg  = open(dTenFold + "/testNeg.txt", 'w')
		fTestNeu  = open(dTenFold + "/testNeu.txt", 'w')
		fTestPos  = open(dTenFold + "/testPos.txt", 'w')
		for j in range(0,3):
			nSent = len(listBerkSent[j])
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					if j == 0:
						fTestNeg.write(listBerkSent[j][k] + "\n")
					if j == 1:
						fTestNeu.write(listBerkSent[j][k] + "\n")
					if j == 2:
						fTestPos.write(listBerkSent[j][k] + "\n")
				else:
					if j == 0:
						fTrainNeg.write(listBerkSent[j][k] + "\n")
					if j == 1:
						fTrainNeu.write(listBerkSent[j][k] + "\n")
					if j == 2:
						fTrainPos.write(listBerkSent[j][k] + "\n")
			
		fTrainNeg.close()
		fTrainNeu.close()
		fTrainPos.close()
		fTestNeg.close()
		fTestNeu.close()
		fTestPos.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dMStanAllFull,str(i))
		fTrainNeg = open(dTenFold + "/trainNeg.txt", 'w')
		fTrainNeu = open(dTenFold + "/trainNeu.txt", 'w')
		fTrainPos = open(dTenFold + "/trainPos.txt", 'w')
		
		fTestNeg  = open(dTenFold + "/testNeg.txt", 'w')
		fTestNeu  = open(dTenFold + "/testNeu.txt", 'w')
		fTestPos  = open(dTenFold + "/testPos.txt", 'w')
		for j in range(0,3):
			nSent = len(listStanSent[j])
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					if j == 0:
						fTestNeg.write(listStanSent[j][k] + "\n")
					if j == 1:
						fTestNeu.write(listStanSent[j][k] + "\n")
					if j == 2:
						fTestPos.write(listStanSent[j][k] + "\n")
				else:
					if j == 0:
						fTrainNeg.write(listStanSent[j][k] + "\n")
					if j == 1:
						fTrainNeu.write(listStanSent[j][k] + "\n")
					if j == 2:
						fTrainPos.write(listStanSent[j][k] + "\n")
			
		fTrainNeg.close()
		fTrainNeu.close()
		fTrainPos.close()
		fTestNeg.close()
		fTestNeu.close()
		fTestPos.close()
	
	"""
		POS/NEG
	"""
	
	## SOCHER
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dSBerkPosnegFull,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		for j in [0,2]:
			nTrees = len(listBerkTree[j])
			for k in range(0,nTrees):
				if k >= (i/10.0) * nTrees and k < ((i+1)/10.0) * nTrees:
					fTest.write(listBerkTree[j][k] + "\n")
				else:
					fTrain.write(listBerkTree[j][k] + "\n")
			
		fTrain.close()
		fTest.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dSStanPosnegFull,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		for j in [0,2]:
			nTrees = len(listStanTree[j])
			for k in range(0,nTrees):
				if k >= (i/10.0) * nTrees and k < ((i+1)/10.0) * nTrees:
					fTest.write(listStanTree[j][k] + "\n")
				else:
					fTrain.write(listStanTree[j][k] + "\n")
			
		fTrain.close()
		fTest.close()
	
	## NAIVE BAYES
	for i in range(0,10):
		dTenFold = criaDiretorio(dNBerkPosnegFull,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTrainPol = open(dTenFold + "/trainPol.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		fTestPol  = open(dTenFold + "/testPol.txt", 'w')
		for j in [0,2]:
			nSent = len(listBerkSent[j])
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					fTest.write(listBerkSent[j][k] + "\n")
					if j == 0:
						fTestPol.write("-\n")
					if j == 2:
						fTestPol.write("+\n")
				else:
					fTrain.write(listBerkSent[j][k] + "\n")
					if j == 0:
						fTrainPol.write("-\n")
					if j == 2:
						fTrainPol.write("+\n")
			
		fTrain.close()
		fTrainPol.close()
		fTest.close()
		fTestPol.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dNStanPosnegFull,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTrainPol = open(dTenFold + "/trainPol.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		fTestPol  = open(dTenFold + "/testPol.txt", 'w')
		for j in [0,2]:
			nSent = len(listStanSent[j])
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					fTest.write(listStanSent[j][k] + "\n")
					if j == 0:
						fTestPol.write("-\n")
					if j == 2:
						fTestPol.write("+\n")
				else:
					fTrain.write(listStanSent[j][k] + "\n")
					if j == 0:
						fTrainPol.write("-\n")
					if j == 2:
						fTrainPol.write("+\n")
			
		fTrain.close()
		fTrainPol.close()
		fTest.close()
		fTestPol.close()
	
	
	## MIKOLOV
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dMBerkPosnegFull,str(i))
		fTrainNeg = open(dTenFold + "/trainNeg.txt", 'w')
		fTrainPos = open(dTenFold + "/trainPos.txt", 'w')
		
		fTestNeg  = open(dTenFold + "/testNeg.txt", 'w')
		fTestPos  = open(dTenFold + "/testPos.txt", 'w')
		for j in [0,2]:
			nSent = len(listBerkSent[j])
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					if j == 0:
						fTestNeg.write(listBerkSent[j][k] + "\n")
					if j == 2:
						fTestPos.write(listBerkSent[j][k] + "\n")
				else:
					if j == 0:
						fTrainNeg.write(listBerkSent[j][k] + "\n")
					if j == 2:
						fTrainPos.write(listBerkSent[j][k] + "\n")
			
		fTrainNeg.close()
		fTrainPos.close()
		fTestNeg.close()
		fTestPos.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dMStanPosnegFull,str(i))
		fTrainNeg = open(dTenFold + "/trainNeg.txt", 'w')
		fTrainPos = open(dTenFold + "/trainPos.txt", 'w')
		
		fTestNeg  = open(dTenFold + "/testNeg.txt", 'w')
		fTestPos  = open(dTenFold + "/testPos.txt", 'w')
		for j in [0,2]:
			nSent = len(listStanSent[j])
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					if j == 0:
						fTestNeg.write(listStanSent[j][k] + "\n")
					if j == 2:
						fTestPos.write(listStanSent[j][k] + "\n")
				else:
					if j == 0:
						fTrainNeg.write(listStanSent[j][k] + "\n")
					if j == 2:
						fTrainPos.write(listStanSent[j][k] + "\n")
			
		fTrainNeg.close()
		fTrainPos.close()
		fTestNeg.close()
		fTestPos.close()
	
	#    _   _   _     _   ______    ____   _____
	#	| | | | | \   | | |  __  \  |  __| |  _  |
	#	| | | | |  \  | | | |  \  | | |_   | |_| |
	#	| | | | | | \ | | | |   | | |  _|  |    /
	#	| |_| | | |\ \| | | |__/  | | |__  | |\ \
	#	|_____| |_| \___| |______/  |____| |_| \_\
	#
	"""
		ALL
	"""	
	## SOCHER
	for i in range(0,10):
		dTenFold = criaDiretorio(dSBerkAllUnder,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		
		nTrees = min([len(listBerkTree[0]), len(listBerkTree[1]), listBerkTree[2]])
		
		for j in range(0,3):
			for k in range(0,nTrees):
				if k >= (i/10.0) * nTrees and k < ((i+1)/10.0) * nTrees:
					fTest.write(listBerkTree[j][k] + "\n")
				else:
					fTrain.write(listBerkTree[j][k] + "\n")
			
		fTrain.close()
		fTest.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dSStanAllUnder,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		
		nTrees = min([len(listStanTree[0]), len(listStanTree[1]), listStanTree[2]])
		
		for j in range(0,3):
			for k in range(0,nTrees):
				if k >= (i/10.0) * nTrees and k < ((i+1)/10.0) * nTrees:
					fTest.write(listStanTree[j][k] + "\n")
				else:
					fTrain.write(listStanTree[j][k] + "\n")
			
		fTrain.close()
		fTest.close()

	
	## NAIVE BAYES
	for i in range(0,10):
		dTenFold = criaDiretorio(dNBerkAllUnder,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTrainPol = open(dTenFold + "/trainPol.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		fTestPol  = open(dTenFold + "/testPol.txt", 'w')
	
		nSent = min([len(listBerkSent[0]), len(listBerkSent[1]), listBerkSent[2]])
		
		for j in range(0,3):
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					fTest.write(listBerkSent[j][k] + "\n")
					if j == 0:
						fTestPol.write("-\n")
					if j == 1:
						fTestPol.write("O\n")
					if j == 2:
						fTestPol.write("+\n")
				else:
					fTrain.write(listBerkSent[j][k] + "\n")
					if j == 0:
						fTrainPol.write("-\n")
					if j == 1:
						fTrainPol.write("O\n")
					if j == 2:
						fTrainPol.write("+\n")
			
		fTrain.close()
		fTrainPol.close()
		fTest.close()
		fTestPol.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dNStanAllUnder,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTrainPol = open(dTenFold + "/trainPol.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		fTestPol  = open(dTenFold + "/testPol.txt", 'w')
	
		nSent = min([len(listStanSent[0]), len(listStanSent[1]), listStanSent[2]])
		
		for j in range(0,3):
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					fTest.write(listStanSent[j][k] + "\n")
					if j == 0:
						fTestPol.write("-\n")
					if j == 1:
						fTestPol.write("O\n")
					if j == 2:
						fTestPol.write("+\n")
				else:
					fTrain.write(listStanSent[j][k] + "\n")
					if j == 0:
						fTrainPol.write("-\n")
					if j == 1:
						fTrainPol.write("O\n")
					if j == 2:
						fTrainPol.write("+\n")
			
		fTrain.close()
		fTrainPol.close()
		fTest.close()
		fTestPol.close()
	
	
	## MIKOLOV
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dMBerkAllUnder,str(i))
		fTrainNeg = open(dTenFold + "/trainNeg.txt", 'w')
		fTrainNeu = open(dTenFold + "/trainNeu.txt", 'w')
		fTrainPos = open(dTenFold + "/trainPos.txt", 'w')
		
		fTestNeg  = open(dTenFold + "/testNeg.txt", 'w')
		fTestNeu  = open(dTenFold + "/testNeu.txt", 'w')
		fTestPos  = open(dTenFold + "/testPos.txt", 'w')
		
		nSent = min([len(listBerkSent[0]), len(listBerkSent[1]), listBerkSent[2]])
		
		for j in range(0,3):
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					if j == 0:
						fTestNeg.write(listBerkSent[j][k] + "\n")
					if j == 1:
						fTestNeu.write(listBerkSent[j][k] + "\n")
					if j == 2:
						fTestPos.write(listBerkSent[j][k] + "\n")
				else:
					if j == 0:
						fTrainNeg.write(listBerkSent[j][k] + "\n")
					if j == 1:
						fTrainNeu.write(listBerkSent[j][k] + "\n")
					if j == 2:
						fTrainPos.write(listBerkSent[j][k] + "\n")
			
		fTrainNeg.close()
		fTrainNeu.close()
		fTrainPos.close()
		fTestNeg.close()
		fTestNeu.close()
		fTestPos.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dMStanAllUnder,str(i))
		fTrainNeg = open(dTenFold + "/trainNeg.txt", 'w')
		fTrainNeu = open(dTenFold + "/trainNeu.txt", 'w')
		fTrainPos = open(dTenFold + "/trainPos.txt", 'w')
		
		fTestNeg  = open(dTenFold + "/testNeg.txt", 'w')
		fTestNeu  = open(dTenFold + "/testNeu.txt", 'w')
		fTestPos  = open(dTenFold + "/testPos.txt", 'w')
		
		nSent = min([len(listStanSent[0]), len(listStanSent[1]), listStanSent[2]])
		
		for j in range(0,3):
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					if j == 0:
						fTestNeg.write(listStanSent[j][k] + "\n")
					if j == 1:
						fTestNeu.write(listStanSent[j][k] + "\n")
					if j == 2:
						fTestPos.write(listStanSent[j][k] + "\n")
				else:
					if j == 0:
						fTrainNeg.write(listStanSent[j][k] + "\n")
					if j == 1:
						fTrainNeu.write(listStanSent[j][k] + "\n")
					if j == 2:
						fTrainPos.write(listStanSent[j][k] + "\n")
			
		fTrainNeg.close()
		fTrainNeu.close()
		fTrainPos.close()
		fTestNeg.close()
		fTestNeu.close()
		fTestPos.close()
	
	"""
		POS/NEG
	"""	
	## SOCHER
	for i in range(0,10):
		dTenFold = criaDiretorio(dSBerkPosnegUnder,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		
		nTrees = min([len(listBerkTree[0]), listBerkTree[2]])
		
		for j in [0,2]:
			for k in range(0,nTrees):
				if k >= (i/10.0) * nTrees and k < ((i+1)/10.0) * nTrees:
					fTest.write(listBerkTree[j][k] + "\n")
				else:
					fTrain.write(listBerkTree[j][k] + "\n")
			
		fTrain.close()
		fTest.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dSStanPosnegUnder,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		
		nTrees = min([len(listStanTree[0]), listStanTree[2]])
		
		for j in [0,2]:
			for k in range(0,nTrees):
				if k >= (i/10.0) * nTrees and k < ((i+1)/10.0) * nTrees:
					fTest.write(listStanTree[j][k] + "\n")
				else:
					fTrain.write(listStanTree[j][k] + "\n")
			
		fTrain.close()
		fTest.close()

	
	## NAIVE BAYES
	for i in range(0,10):
		dTenFold = criaDiretorio(dNBerkPosnegUnder,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTrainPol = open(dTenFold + "/trainPol.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		fTestPol  = open(dTenFold + "/testPol.txt", 'w')
	
		nSent = min([len(listBerkSent[0]), listBerkSent[2]])
		
		for j in [0,2]:
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					fTest.write(listBerkSent[j][k] + "\n")
					if j == 0:
						fTestPol.write("-\n")
					if j == 2:
						fTestPol.write("+\n")
				else:
					fTrain.write(listBerkSent[j][k] + "\n")
					if j == 0:
						fTrainPol.write("-\n")
					if j == 2:
						fTrainPol.write("+\n")
			
		fTrain.close()
		fTrainPol.close()
		fTest.close()
		fTestPol.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dNStanPosnegUnder,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTrainPol = open(dTenFold + "/trainPol.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		fTestPol  = open(dTenFold + "/testPol.txt", 'w')
	
		nSent = min([len(listStanSent[0]), listStanSent[2]])
		
		for j in [0,2]:
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					fTest.write(listStanSent[j][k] + "\n")
					if j == 0:
						fTestPol.write("-\n")
					if j == 2:
						fTestPol.write("+\n")
				else:
					fTrain.write(listStanSent[j][k] + "\n")
					if j == 0:
						fTrainPol.write("-\n")
					if j == 2:
						fTrainPol.write("+\n")
			
		fTrain.close()
		fTrainPol.close()
		fTest.close()
		fTestPol.close()
	
	
	## MIKOLOV
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dMBerkPosnegUnder,str(i))
		fTrainNeg = open(dTenFold + "/trainNeg.txt", 'w')
		fTrainPos = open(dTenFold + "/trainPos.txt", 'w')
		
		fTestNeg  = open(dTenFold + "/testNeg.txt", 'w')
		fTestPos  = open(dTenFold + "/testPos.txt", 'w')
		
		nSent = min([len(listBerkSent[0]), listBerkSent[2]])
		
		for j in [0,2]:
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					if j == 0:
						fTestNeg.write(listBerkSent[j][k] + "\n")
					if j == 2:
						fTestPos.write(listBerkSent[j][k] + "\n")
				else:
					if j == 0:
						fTrainNeg.write(listBerkSent[j][k] + "\n")
					if j == 2:
						fTrainPos.write(listBerkSent[j][k] + "\n")
			
		fTrainNeg.close()
		fTrainPos.close()
		fTestNeg.close()
		fTestPos.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dMStanPosnegUnder,str(i))
		fTrainNeg = open(dTenFold + "/trainNeg.txt", 'w')
		fTrainPos = open(dTenFold + "/trainPos.txt", 'w')
		
		fTestNeg  = open(dTenFold + "/testNeg.txt", 'w')
		fTestPos  = open(dTenFold + "/testPos.txt", 'w')
		
		nSent = min([len(listStanSent[0]), listStanSent[2]])
		
		for j in [0,2]:
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					if j == 0:
						fTestNeg.write(listStanSent[j][k] + "\n")
					if j == 2:
						fTestPos.write(listStanSent[j][k] + "\n")
				else:
					if j == 0:
						fTrainNeg.write(listStanSent[j][k] + "\n")
					if j == 2:
						fTrainPos.write(listStanSent[j][k] + "\n")
			
		fTrainNeg.close()
		fTrainPos.close()
		fTestNeg.close()
		fTestPos.close()
	
	#    _____  ___        ___  ____   _____
	#	|  _  | \  \      /  / |  __| |  _  |
	#	| | | |  \  \    /  /  | |_   | |_| |
	#	| | | |   \  \  /  /   |  _|  |    /
	#	| |_| |    \  \/  /    | |__  | |\ \
	#	|_____|     \____/     |____| |_| \_\
	#
	"""
		ALL
	"""	
	## SOCHER
	for i in range(0,10):
		dTenFold = criaDiretorio(dSBerkAllOver,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		
		vMaxTrain = max([len(listBerkTree[0]), len(listBerkTree[1]), len(listBerkTree[2])]) * 0.9
		
		for j in range(0,3):
			
			nTrees = len(listBerkTree[j])
			
			trainingSentences = []
			
			for k in range(0,nTrees):
				if k >= (i/10.0) * nTrees and k < ((i+1)/10.0) * nTrees:
					fTest.write(listBerkTree[j][k] + "\n")
				else:
					trainingSentences.append(listBerkTree[j][k])
			
			vTrain = len(trainingSentences)
			for k in range(0,int(vMaxTrain)):
				fTrain.write(trainingSentences[k%vTrain] + "\n")
			
		fTrain.close()
		fTest.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dSStanAllOver,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		
		vMaxTrain = max([len(listStanTree[0]), len(listStanTree[1]), len(listStanTree[2])]) * 0.9
		
		for j in range(0,3):
			
			nTrees = len(listStanTree[j])
			
			trainingSentences = []
			
			for k in range(0,nTrees):
				if k >= (i/10.0) * nTrees and k < ((i+1)/10.0) * nTrees:
					fTest.write(listStanTree[j][k] + "\n")
				else:
					trainingSentences.append(listStanTree[j][k])
			
			vTrain = len(trainingSentences)
			for k in range(0,int(vMaxTrain)):
				fTrain.write(trainingSentences[k%vTrain] + "\n")
			
		fTrain.close()
		fTest.close()
	
	##NAIVE BAYES	
	for i in range(0,10):
		dTenFold = criaDiretorio(dNBerkAllOver,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTrainPol = open(dTenFold + "/trainPol.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		fTestPol  = open(dTenFold + "/testPol.txt", 'w')
		
		vMaxTrain = max([len(listBerkSent[0]), len(listBerkSent[1]), len(listBerkSent[2])]) * 0.9
		
		for j in range(0,3):
			
			nSent = len(listBerkSent[j])
			trainingSentences = []
			
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					fTest.write(listBerkSent[j][k] + "\n")
					if j == 0:
						fTestPol.write("-\n")
					if j == 1:
						fTestPol.write("O\n")
					if j == 2:
						fTestPol.write("+\n")
				else:
					trainingSentences.append(listBerkSent[j][k])
					
			vTrain = len(trainingSentences)
			for k in range(0,int(vMaxTrain)):
				fTrain.write(trainingSentences[k%vTrain] + "\n")
				if j == 0:
					fTrainPol.write("-\n")
				if j == 1:
					fTrainPol.write("O\n")
				if j == 2:
					fTrainPol.write("+\n")
			
		fTrain.close()
		fTrainPol.close()
		fTest.close()
		fTestPol.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dNStanAllOver,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTrainPol = open(dTenFold + "/trainPol.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		fTestPol  = open(dTenFold + "/testPol.txt", 'w')
		
		vMaxTrain = max([len(listStanSent[0]), len(listStanSent[1]), len(listStanSent[2])]) * 0.9
		
		for j in range(0,3):
			
			nSent = len(listStanSent[j])
			trainingSentences = []
			
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					fTest.write(listStanSent[j][k] + "\n")
					if j == 0:
						fTestPol.write("-\n")
					if j == 1:
						fTestPol.write("O\n")
					if j == 2:
						fTestPol.write("+\n")
				else:
					trainingSentences.append(listStanSent[j][k])
					
			vTrain = len(trainingSentences)
			for k in range(0,int(vMaxTrain)):
				fTrain.write(trainingSentences[k%vTrain] + "\n")
				if j == 0:
					fTrainPol.write("-\n")
				if j == 1:
					fTrainPol.write("O\n")
				if j == 2:
					fTrainPol.write("+\n")
			
		fTrain.close()
		fTrainPol.close()
		fTest.close()
		fTestPol.close()
		
	## MIKOLOV
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dMBerkAllOver,str(i))
		fTrainNeg = open(dTenFold + "/trainNeg.txt", 'w')
		fTrainNeu = open(dTenFold + "/trainNeu.txt", 'w')
		fTrainPos = open(dTenFold + "/trainPos.txt", 'w')
		
		fTestNeg  = open(dTenFold + "/testNeg.txt", 'w')
		fTestNeu  = open(dTenFold + "/testNeu.txt", 'w')
		fTestPos  = open(dTenFold + "/testPos.txt", 'w')
		
		vMaxTrain = max([len(listBerkSent[0]), len(listBerkSent[1]), len(listBerkSent[2])]) * 0.9
		
		for j in range(0,3):
			
			nSent = len(listBerkSent[j])
			trainingSentences = []
			
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					if j == 0:
						fTestNeg.write(listBerkSent[j][k] + "\n")
					if j == 1:
						fTestNeu.write(listBerkSent[j][k] + "\n")
					if j == 2:
						fTestPos.write(listBerkSent[j][k] + "\n")
				else:
					trainingSentences.append(listBerkSent[j][k])
			
			vTrain = len(trainingSentences)
			for k in range(0,int(vMaxTrain)):
				if j == 0:
					fTrainNeg.write(trainingSentences[k%vTrain] + "\n")
				if j == 1:
					fTrainNeu.write(trainingSentences[k%vTrain] + "\n")
				if j == 2:
					fTrainPos.write(trainingSentences[k%vTrain] + "\n")
		
		fTrainNeg.close()
		fTrainNeu.close()
		fTrainPos.close()
		fTestNeg.close()
		fTestNeu.close()
		fTestPos.close()
		
	for i in range(0,10):
		dTenFold = criaDiretorio(dMStanAllOver,str(i))
		fTrainNeg = open(dTenFold + "/trainNeg.txt", 'w')
		fTrainNeu = open(dTenFold + "/trainNeu.txt", 'w')
		fTrainPos = open(dTenFold + "/trainPos.txt", 'w')
		
		fTestNeg  = open(dTenFold + "/testNeg.txt", 'w')
		fTestNeu  = open(dTenFold + "/testNeu.txt", 'w')
		fTestPos  = open(dTenFold + "/testPos.txt", 'w')
		
		vMaxTrain = max([len(listStanSent[0]), len(listStanSent[1]), len(listStanSent[2])]) * 0.9
		
		for j in range(0,3):
			
			nSent = len(listStanSent[j])
			trainingSentences = []
			
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					if j == 0:
						fTestNeg.write(listStanSent[j][k] + "\n")
					if j == 1:
						fTestNeu.write(listStanSent[j][k] + "\n")
					if j == 2:
						fTestPos.write(listStanSent[j][k] + "\n")
				else:
					trainingSentences.append(listStanSent[j][k])
			
			vTrain = len(trainingSentences)
			for k in range(0,int(vMaxTrain)):
				if j == 0:
					fTrainNeg.write(trainingSentences[k%vTrain] + "\n")
				if j == 1:
					fTrainNeu.write(trainingSentences[k%vTrain] + "\n")
				if j == 2:
					fTrainPos.write(trainingSentences[k%vTrain] + "\n")
		
		fTrainNeg.close()
		fTrainNeu.close()
		fTrainPos.close()
		fTestNeg.close()
		fTestNeu.close()
		fTestPos.close()
		
		
	"""
		POS/NEG
	"""	
	## SOCHER
	for i in range(0,10):
		dTenFold = criaDiretorio(dSBerkPosnegOver,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		
		vMaxTrain = max([len(listBerkTree[0]), len(listBerkTree[2])]) * 0.9
		
		for j in [0,2]:
			
			nTrees = len(listBerkTree[j])
			
			trainingSentences = []
			
			for k in range(0,nTrees):
				if k >= (i/10.0) * nTrees and k < ((i+1)/10.0) * nTrees:
					fTest.write(listBerkTree[j][k] + "\n")
				else:
					trainingSentences.append(listBerkTree[j][k])
			
			vTrain = len(trainingSentences)
			for k in range(0,int(vMaxTrain)):
				fTrain.write(trainingSentences[k%vTrain] + "\n")
			
		fTrain.close()
		fTest.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dSStanPosnegOver,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		
		vMaxTrain = max([len(listStanTree[0]), len(listStanTree[2])]) * 0.9
		
		for j in [0,2]:
			
			nTrees = len(listStanTree[j])
			
			trainingSentences = []
			
			for k in range(0,nTrees):
				if k >= (i/10.0) * nTrees and k < ((i+1)/10.0) * nTrees:
					fTest.write(listStanTree[j][k] + "\n")
				else:
					trainingSentences.append(listStanTree[j][k])
			
			vTrain = len(trainingSentences)
			for k in range(0,int(vMaxTrain)):
				fTrain.write(trainingSentences[k%vTrain] + "\n")
			
		fTrain.close()
		fTest.close()
	
	##NAIVE BAYES	
	for i in range(0,10):
		dTenFold = criaDiretorio(dNBerkPosnegOver,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTrainPol = open(dTenFold + "/trainPol.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		fTestPol  = open(dTenFold + "/testPol.txt", 'w')
		
		vMaxTrain = max([len(listBerkSent[0]), len(listBerkSent[2])]) * 0.9
		
		for j in [0,2]:
			
			nSent = len(listBerkSent[j])
			trainingSentences = []
			
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					fTest.write(listBerkSent[j][k] + "\n")
					if j == 0:
						fTestPol.write("-\n")
					if j == 2:
						fTestPol.write("+\n")
				else:
					trainingSentences.append(listBerkSent[j][k])
					
			vTrain = len(trainingSentences)
			for k in range(0,int(vMaxTrain)):
				fTrain.write(trainingSentences[k%vTrain] + "\n")
				if j == 0:
					fTrainPol.write("-\n")
				if j == 2:
					fTrainPol.write("+\n")
			
		fTrain.close()
		fTrainPol.close()
		fTest.close()
		fTestPol.close()
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dNStanPosnegOver,str(i))
		fTrain = open(dTenFold + "/train.txt", 'w')
		fTrainPol = open(dTenFold + "/trainPol.txt", 'w')
		fTest  = open(dTenFold + "/test.txt", 'w')
		fTestPol  = open(dTenFold + "/testPol.txt", 'w')
		
		vMaxTrain = max([len(listStanSent[0]), len(listStanSent[2])]) * 0.9
		
		for j in [0,2]:
			
			nSent = len(listStanSent[j])
			trainingSentences = []
			
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					fTest.write(listStanSent[j][k] + "\n")
					if j == 0:
						fTestPol.write("-\n")
					if j == 2:
						fTestPol.write("+\n")
				else:
					trainingSentences.append(listStanSent[j][k])
					
			vTrain = len(trainingSentences)
			for k in range(0,int(vMaxTrain)):
				fTrain.write(trainingSentences[k%vTrain] + "\n")
				if j == 0:
					fTrainPol.write("-\n")
				if j == 2:
					fTrainPol.write("+\n")
			
		fTrain.close()
		fTrainPol.close()
		fTest.close()
		fTestPol.close()
		
	## MIKOLOV
	
	for i in range(0,10):
		dTenFold = criaDiretorio(dMBerkPosnegOver,str(i))
		fTrainNeg = open(dTenFold + "/trainNeg.txt", 'w')
		fTrainPos = open(dTenFold + "/trainPos.txt", 'w')
		
		fTestNeg  = open(dTenFold + "/testNeg.txt", 'w')
		fTestPos  = open(dTenFold + "/testPos.txt", 'w')
		
		vMaxTrain = max([len(listBerkSent[0]), len(listBerkSent[2])]) * 0.9
		
		for j in [0,2]:
			
			nSent = len(listBerkSent[j])
			trainingSentences = []
			
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					if j == 0:
						fTestNeg.write(listBerkSent[j][k] + "\n")
					if j == 2:
						fTestPos.write(listBerkSent[j][k] + "\n")
				else:
					trainingSentences.append(listBerkSent[j][k])
			
			vTrain = len(trainingSentences)
			for k in range(0,int(vMaxTrain)):
				if j == 0:
					fTrainNeg.write(trainingSentences[k%vTrain] + "\n")
				if j == 2:
					fTrainPos.write(trainingSentences[k%vTrain] + "\n")
		
		fTrainNeg.close()
		fTrainPos.close()
		fTestNeg.close()
		fTestPos.close()
		
	for i in range(0,10):
		dTenFold = criaDiretorio(dMStanPosnegOver,str(i))
		fTrainNeg = open(dTenFold + "/trainNeg.txt", 'w')
		fTrainPos = open(dTenFold + "/trainPos.txt", 'w')
		
		fTestNeg  = open(dTenFold + "/testNeg.txt", 'w')
		fTestPos  = open(dTenFold + "/testPos.txt", 'w')
		
		vMaxTrain = max([len(listStanSent[0]), len(listStanSent[2])]) * 0.9
		
		for j in [0,2]:
			
			nSent = len(listStanSent[j])
			trainingSentences = []
			
			for k in range(0,nSent):
				if k >= (i/10.0) * nSent and k < ((i+1)/10.0) * nSent:
					if j == 0:
						fTestNeg.write(listStanSent[j][k] + "\n")
					if j == 2:
						fTestPos.write(listStanSent[j][k] + "\n")
				else:
					trainingSentences.append(listStanSent[j][k])
			
			vTrain = len(trainingSentences)
			for k in range(0,int(vMaxTrain)):
				if j == 0:
					fTrainNeg.write(trainingSentences[k%vTrain] + "\n")
				if j == 2:
					fTrainPos.write(trainingSentences[k%vTrain] + "\n")
		
		fTrainNeg.close()
		fTrainPos.close()
		fTestNeg.close()
		fTestPos.close()
		
"""	FUNÇÕES	"""

#Função pra não ficar dando erro quando já existir o diretório
def criaDiretorio(path, folder):
	if path[-1] == '/':
		path += folder
	else:
		path += "/" + folder

	if not os.path.isdir(path):
		os.mkdir(path, 0755)	
	
	return path





if __name__ == "__main__":
	execfile("ptbMng.py")
	
	"""
		'path' é o caminho da pasta onde será salva a hierarquia de diretórios
		'berk' é o caminho do treebank de berkeley
		'stan' é o caminho do treebank de stanford
		'
	"""
	
	main()









