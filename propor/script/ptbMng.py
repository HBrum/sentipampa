#!/usr/bin/python
# -*- coding: utf-8 -*-
from nltk.tree import *
from nltk.draw import tree
import sys

"""Version 2.4"""

#BEGINNING OF NODE CLASS -----------------------------------------------#
class Node:
	
	"""Node to be used on the Penn-treebank object

    Attributes:
        children	Array of child nodes of this node
        value		The value of the node
        parent		The parent of the node
    """
	
	
	#Constructor
	def __init__(self, value):
		"""Creates the node.
		:param value: The value for the node created.
		"""
		self.parent = None
		self.value = value
		self.children = []
	
	def setValue(self, value):
		"""Sets a different value for the current node.
		:param value: the value to be set.
		"""
		self.value = value
	
	#Working with parents
	
	def setParent(self, parent):
		"""Sets a parent for the node.
		:param parent: A node to be used as a parent.
		"""
		self.parent = parent

	def getParent(self):
		"""Returns the parent of the node unless it's has no parent, then it returns None."""
		return self.parent



	#Working with children

	def addChild(self, value):
		"""Adds a child to the children array.
		:param value: A value for the child node.
		"""
		child = Node(value)
		child.setParent(self)
		self.children.append(child)
		return

	def addChildNode(self, child):
		"""Adds a child node to the children array.
		:param child: A node to be add to the children array.
		"""
		child.setParent(self)
		self.children.append(child)
	
	def getChildren( self ):
		"""Returns the children array."""
		return self.children
		
	def hasChildren( self ):
		"""Returns True if the node contains children"""
		if len( self.getChildren() ) > 0:
			return True
		return False
		
	
	
	#Working with the node! :D
	
	def get( self ):
		"""Returns the value of the node."""
		return self.value
	
	def isTop( self ):
		"""Returns a boolean representing if the node has a parent."""
		if self.parent == None:
			return True
		return False
		
	def isBottom( self ):
		"""Returns a boolean representing if the node has children."""
		if len(self.children) == 0:
			return True
		return False
	
	def printNode(self, node):
		"""
			Recursive function that prints prints a node and its children in PTB format
			Please, don't use it...
		"""
		st = ""
		if node.hasChildren():
			st += str("(" + node.get().strip() + " ")
			for c in node.getChildren():
				st += self.printNode(c)
			st += str(")")
		else:
			return str(node.get())
		return st
	
	def printTree(self):
		"""
			Function to activate the printing function. Use this instead.
		"""
		return self.printNode(self)
	
	def formString(self):
		"""write only the bottom of the tree that is the sentence withou tags (it's basically a stack implementation) """ 
		sentence = ""
		stack = []
		stack.append(self)
		while len(stack) is not 0:
			n = stack.pop()
			if n.isBottom():
				sentence = n.get() + " " + sentence
			else:
				for c in n.getChildren():
					stack.append(c)

		return sentence
	
#END OF NODE CLASS -----------------------------------------------------#





#BEGINNING OF TREE CLASS -----------------------------------------------#

class Treebank:
	
	"""Penn Treebank Class

    Attributes:
        sentence	String with the ptb sentence
        top			The top node of the treebank
    """
	
	#Constructor
	def __init__(self, sentence):
		"""Creates the treebank.
		:param sentence: A sentence string to be converted in nodes. The top will be stored.
		"""
		self.sentence = sentence
		self.top = self.tokenize( self.sentence )

	#Changing the Top
	def setTop(self, node):
		"""ALERT: this function may change the whole tree
		:param node: A node to be replace for the top of the treebank.
		"""
		self.top = node
		self.sentence = node.formString()

	#Get
	def getNode(self):
		"""Returns the top of the tree"""
		return self.top
	
	
	
	#Utils
	
	def tokenize( self, sentence ):
		"""Receives a sentence and returns the node of the top of a tree with the sentence.
		:param sentence: 
		"""
		sentence = sentence.strip()
		
		if not self.checkParenthesis(sentence):
			print "The sentence has a syntax problem on parenthesis"
			return None
		
		#If we have several empty 
		while sentence[0] == '(':
			sentence = sentence[1:-1].strip()
			
		return self.getNodes(sentence)
		
	
	
	def getNodes(self, sentence):
		"""Recursive function to create a tree based on a sentence.
		It basicaly reads a sentence and tokenize it in chunkies (1~infinite). Gets the first as a top node
		and add the rest as children of the first. The children will be applied to the same function untill
		the end of the sentence.
		
		:param sentence: string with the sen
		"""
		
		#Sub nodes of the sentence
		chunks = []
		
		chunk = ""
		
		#In order to tokenize the chunkies in parenthesis
		cont = 0
		
		for letter in sentence:
			
			#If letter is *blank, we should check for a simple value
			if letter == ' ':
				if cont == 0 and len(chunk.strip()) > 0:
					chunks.append(chunk.strip())
					chunk = ""
					continue
				
			#If it's the beginning of a parenthesis, we incremente the counter and
			#check for previous value
			if letter == '(':
				if cont == 0 and len(chunk.strip()) > 0:
					chunks.append(chunk.strip())
					chunk = ""
				cont += 1
				
			#We increment the letter on the chunk
			chunk += letter
				
			#If we close a parenthesis, we should check if it closes a chunk
			if letter == ')':
				cont -= 1
				if cont == 0 and len(chunk.strip()) > 0:
					chunks.append(chunk.strip())
					chunk = ""
		
		#At the end, we should check our buffer
		if len(chunk.strip()) > 0:
			chunks.append(chunk.strip())
		
		#We create the node for the first chunk, since it's the top of the new tree
		if len(chunks) < 1:
			cNode = Node (" ")
		else:
			cNode = Node(chunks[0])
		
		#If it's the only chunk, we simply create the node and return for the layer above
		if len(chunks) == 1:
			return cNode
			
			#We get the rest of the chunk and recur it to the function
		for c in chunks[1:]:
			
			#If it's a new tree, we remove the external parenthesis and get the node of it 
			if c[0] == '(':
				child = self.getNodes(c[1:-1].strip())
				cNode.addChildNode( child )
				
			#If it isn't, we simply get the node of it
			else:
				child = self.getNodes( c.strip() )
				cNode.addChildNode( child )
		
		return cNode
	
	
	def checkParenthesis(self, sentence):
		"""Checks if there are not parenthesis opened in the sentence.
		:param sentence: A sentence in string format
		:return boolean representing if all the parenthesis are closed or not
		"""
		
		cont = 0
		
		for l in sentence:
			if l == '(':
				cont += 1
			if l == ')':
				cont -= 1
				
		if cont == 0:
			return True
			
		return False
		
		
		
	#Printing Trees
	
	def printTb(self):
		"""prints the treebank in PTB format"""
		return self.top.printTree()
		
	
	#Drawing Trees	
		
	def draw(self):
		"""Prints using nltk draw function"""
		treeObj = toTree(self.top)
		treeObj.draw()
	
	#Writing sentences
	
	def writeSentence(self):
		""" Calls the FormString Node Function
			write only the bottom of the tree that is the sentence withou tags (it's basically a stack implementation)
		""" 
		return self.top.formString()
		
		
			
		
		
		
#ENF OF TREE CLASS -----------------------------------------------------#






#UTILS------------------------------------------------------------------#


#Converting
def toTB( tree):
	if len(tree) == 0:
		return Node(tree.label())
	else:
		current = Node( tree.label() )
		for child in tree:
			current.addChildNode( toTB(child) ) 
		return current
	
def toTree(node):
	"""returns a nltk.tree object"""
	if node.hasChildren():
		child = []
		for c in node.getChildren():
			child.append( toTree(c) )
		return Tree(node.get(), child)
	else:
		return Tree(node.get(), [])
		
def sampleTree():
	"""A sample Tree that returns a treebank with the same structure as in the "SampleText()" function"""
	tree = Node("4")
	tree.addChild("2")
	tree.addChild("3")
	vt = tree.getChildren()
	vt[0].addChild("2")
	vt[0].getChildren()[0].addChild("The")
	vt[0].addChild("2")
	vt[0].getChildren()[1].addChild("actors")
	vt[1].addChild("4")
	vt[1].getChildren()[0].addChild("2")
	vt[1].getChildren()[0].addChild("3")
	vt[1].getChildren()[0].getChildren()[0].addChild("are")
	vt[1].getChildren()[0].getChildren()[1].addChild("fantastic")
	vt[1].addChild("2")
	vt[1].getChildren()[1].addChild(".")

	return tree

def sampleText():
	"""A sample text that returns the same as the structure of the "SampleTree()" function"""
	#return '( (IP (NP (D Um) (N livro) (ADJP (Q muito) (ADJ legal)) (, ,) (CP (WNP (WPRO que)) (IP (NEG não) (VB tenho) (CP (C como) (IP (IP (IP (NEG não) (VB falar) (PP (P de) (NP (D a) (N abordagem) (ADJ brilhante) (PP (P de) (NP (D a) (N autora)))))) (, ,) (ADVP (ADV assim) (CP (C como) (IP (VB entrelaçamento) (PP (P de) (NP (NP (D os) (N personagens)) (CONJP (CONJ e) (NP (D a) (N precisão) (PP (P de) (NP (PRO ela))))))) (PP (P a) (NP (D o) (N relatar))) (NP (D os) (N fatos) (PP (P de) (NP (D o) (N ponto) (PP (P de) (NP (NPR Bella)))))) (, ,) (PP (P para) (NP (PRO mim))) (, ,) (IP (NP (D os) (N diretores) (PP (P de) (NP (N filmes) (CP (WNP (WPRO que)) (IP (VB vieram) (PP (P de) (NP (N livros)))))))) (NEG não) (VB (VB explorem) (CONJ e) (VB deixe)) (CP (C como) (IP (CP (C se) (IP (SR fosse) (NP (D um) (Q nada) (CP (WNP (WPRO que)) (IP (NP (CL se)) (VB constrói) (ADJP (ADJP (ADJ sozinho)) (, ,) (CONJP (CONJ mas) (ADJP (VB voltando) (PP (P a) (NP (D o) (N livro))))))))))) (, ,) (NP (PRO ela)) (VB deu) (NP (D o) (N mistério) (ADJ envolvente) (CP (WNP (WPRO que)) (IP (FP só) (NP (D um) (N livro) (PP (P de) (NP (D esse) (N tipo)))) (VB merece)))))))))) (, ,) (FP só) (TR tenho) (NP (D uma) (N reclamação))) (, ,) (CONJP (CONJ mas) (IP (IP (NEG não) (SR é) (ADJP (Q muito) (ADJ importante))) (, ,) (IP (NP (PRO eu)) (VB notei) (ADVP (NP (D uma) (N auto-estima)) (ADV abaixo) (PP (P de) (NP (NPR zero) (PP (P de) (NP (NPR Bella))))))) (, ,) (IP (CONJ e) (NP (DEM isso)) (FP só) (VB piora) (IP (CP (C quando) (IP (NP (PRO ela)) (VB encontra) (NP (NPR Edward) (NPR Cullen)))) (, ,) (QT ") (NP (PRO ele)) (SR é) (NP (Q tudo)) (, ,) (IP (NP (PRO eu)) (SR sou) (NP (Q nada)))) (QT "))))))))) (PP (NEG não) (P com) (NP (D essas) (N palavras))) (, ,) (SR era) (NP (DEM isso)) (CP (C que) (IP (NP (PRO ela)) (VB queria) (IP (VB falar) (PP (P em) (NP (D o) (N livro) (, ,) (CP (CONJ mas) (, ,) (IP (IP (PP (P fora) (NP (DEM isso))) (, ,) (NP (PRO eu)) (NEG não) (TR tenho) (NP (Q nada)) (NP (D a) (N reclamar) (PP (P de) (NP (NPR Crepúsculo))))) (CONJP (CONJ e) (IP (NP (D os) (OUTRO outros) (N livros) (PP (P de) (NP (D a) (N saga)))) (SR serão) (ADJP (ADV ainda) (ADJ melhores))))))))))) (. !)) )'
	#return '( (IP (ADVP (ADV Crepúculo)) (VB conta) (NP (D a) (N história) (PP (P de) (NP (D a) (ADJ jovem) (NPR Bella))) (CP (WNP (WPRO que)) (IP (VB vai) (VB morar) (PP (P com) (NP (PRO$ seu) (N pai))) (PP (P em) (NP (D a) (ADJP (ADJ pequena) (, ,) (ADJ cinza) (CONJ e) (ADJ úmida)) (N cidade) (PP (P de) (NP (NPR Forks)))))))) (. .)) )'
	return "    ( 4(2 (2 The)(2 actors))(3(4(2 are)(3 fantastic))(2 . ) ))"
	#return "(IP (CONJ Achei) (ADJP (ADV tão) (ADJ poético) (CONJP (CONJ e) (ADJX (ADJ bonito))) (, ,) (CP (C que) (IP (NP (FP mesmo) (D a) (N realidade) (CP (WNP (WPRO que)) (IP (SR é) (ADJP (ADJ mostrada)) (IP (SR ser) (ADJP (ADV bem) (ADJ crua) (CONJP (CONJ e) (ADJX (ADJ explicita))))))))))) (, ,) (NEG não) (VB tira) (NP (NP (D o) (ADJ lírico)) (CONJP (CONJ e) (NP (D a) (N alma) (ADJP (ADV tão) (ADJ verdadeira)) (CP (WNP (WPRO que)) (IP (TR tem) (PP (P em) (NP (D esse) (N livro)))))))) (. .))"

#execfile('ptbMng.py'); tb = Treebank(sampleText())
