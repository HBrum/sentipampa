#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

argv = sys.argv
fPaths = argv[1]
dModels = argv[2]
dResult = argv[3]
dLogs = argv[4]
fStanford = argv[5]

if dModels[-1] is not '/':
	dModels += "/"
if dResult[-1] is not '/':
	dResult += "/"


def getBatch(line):
	splt = line.split("/")
	if splt[-3] == 'naive' and splt[-1] == 'test.txt':
		folder = line[0:-8]
		modelName = "model_" + splt[-6] + "_" + splt[-5] + "_" + splt[-4] + "_" + splt[-2] + ".md"
		resultName = "result_" + splt[-6] + "_" + splt[-5] + "_" + splt[-4] + "_" + splt[-2] + ".res"
		print "python naivebayes.py -train " + folder +"train.txt " + folder + "trainPol.txt " + dModels + "naive/" + modelName
		print "python naivebayes.py -run " + dModels + "naive/" + modelName + " -inputFile " + folder + "test.txt -toFile " + dResult + resultName
	
	if splt[-3] == 'socher' and splt[-1] == 'test.txt':
		folder = line[0:-8]
		modelName = "model_" + splt[-6] + "_" + splt[-5] + "_" + splt[-4] + "_" + splt[-2] + ".ser.gz"
		logName = "log_" + splt[-6] + "_" + splt[-5] + "_" + splt[-4] + "_" + splt[-2] + ".log"
		print 'java -cp "'+fStanford+'*" -mx8g edu.stanford.nlp.sentiment.SentimentTraining -maxTrainTimeSeconds 0 -trainPath ' + folder + 'train.txt -train -model ' + dModels + 'socher/' +modelName + ' &> ' + dLogs +logName + ' &'
		

def main():
	
	with open(fPaths) as source:
		for line in source:
			getBatch(line.strip())

#Função pra não ficar dando erro quando já existir o diretório
def criaDiretorio(path, folder):
	if path[-1] == '/':
		path += folder
	else:
		path += "/" + folder

	if not os.path.isdir(path):
		os.mkdir(path, 0755)	
	
	return path		
	
if __name__ == "__main__":
		
	main()
