#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

"""
	TRAIN PHASE   ----------
	
	Parameters:
		- Sentences File
		- Polarities File
		- Path to model
	
	Eg. python naive_classf.py -train sentences.txt polarities.txt ./output/model.data
	
	Ps: DO NOT use a filename or path becoming with '-'. Please. ;)
"""
def train(sentencesFile, polaritiesFile, outputPath):
	
	
	w = {}
	c = {}
	vecPol = []
	
	#gambi
	polinF = open(polaritiesFile, "r")
	for polin in polinF:
		polr = polin[0]
	
		if polr not in vecPol:
			#Class[ polarity ] = [ Number of sentences in this polarity, number of words in this polarity]
			vecPol.append(polr)
	polinF.close()
	
	senF = open(sentencesFile, "r")
	polF = open(polaritiesFile, "r")
	outF = open(outputPath, "w")
	
	
	for polr in polF:
		polr = polr[0]
		sent = senF.readline().strip()
		
		#polarity
		if len(c) == 0 or polr not in c:
			#Class[ polarity ] = [ Number of sentences in this polarity, number of words in this polarity]
			c[polr] = [1,0]
		else:
			c[polr] = [ c[polr][0] + 1 , c[polr][1] ]
	
		polrI = getVecIndex(vecPol, polr)
		
		
		
		#word
		sent = sent.split(" ")
		for word in sent:
			word = word.lower()
			#Adding a word
			if len(w) == 0 or word not in w:
				w[word] = []
				for element in vecPol:
					w[word].append(0)
				w[word][polrI] = 1
				
			#Increasing an occurence
			else:
				c[polr][1] += 1
				w[word][polrI] += 1
	
	#Calculating sentences in document
	totalDoc = 0.0
	for polClass in c:
		totalDoc += c[polClass][0]
	
	
	
	for polClass in vecPol:
		probC = float(c[polClass][0]/totalDoc)
		line = polClass + '\t' + str(c[polClass][0]) + '\t' + str(c[polClass][1]) + '\t'  + str(probC) + '\n'
		outF.write(line)
	
	outF.write('\n')
	
	for wordText in w:
		line = wordText + '\t'
		
		for polNum in range(0,len(vecPol)):
			line += str(w[wordText][polNum]) + '\t'
		line = line.strip() + '\n'
		outF.write(line)
	

	
	outF.write('\n')

	senF.close()
	polF.close()
	outF.close()

def getVecIndex(vec, element):
	count = 0
	for i in vec:
		if element == i:
			return count
		count += 1

"""
	LOADING MODEL ----------
	
	Parameters:
			- Path to Model
			
	Return:
			- 
"""
def load(path):
	print "Loading model"
	
	fileIn = open(path, "r")
	
	w = {}
	c = {}
	vecPol = []
	
	line = fileIn.readline()
	while line != '\n':
		line = line.strip().split('\t')
		vecPol.append(line[0])
		c[line[0]] = [line[1],line[2], line[3]]
		line = fileIn.readline()
	
	
	line = fileIn.readline()
	while line != '\n':
		line = line.strip().split('\t')
		w[line[0]] = []
		for aux_c in range(1, len(c)+1):
			w[line[0]].append(line[aux_c])
		line = fileIn.readline()
	
	print "Model loaded"
	
	fileIn.close()
	
	return w, c, vecPol

"""
	NAIVE BAYES
	
	Parameters:
			- Sentence to be analyzed
			- Dictionary of words learned
			- Dictionary of classes learned
			- Vector of classes
			- Claculated class probabilities
	Return:
		- Class tag
"""
def classify(doc, w, c, vecPol):
	doc = doc.split(" ")
	maxOf = []
	
	for currentPol in vecPol:
		#currentIndex
		currentIndex = getVecIndex(vecPol,currentPol)
		
		#Class Probability
		maxOf.append(float(c[currentPol][2]))
		for word in doc:
			if word in w:
				n = float(w[word][currentIndex])
				n = n + 1 / float(c[currentPol][1])
				maxOf[-1] *= n
			else:
				maxOf[-1] *= 1
	
	return vecPol[argMax(maxOf)]
	
	
def argMax(vec):
	m = 0
	maxN = -1
	count = 0
	
	for n in vec:
		if n > m:
			m = n
			maxN = count
		count += 1
	return maxN
"""
	RUN PHASE     ----------
	
	Parameters:
			- Trained Model
			- Input Mode (left as None in case of Keyboard input)
			- PathOut (left as None in case of screen ouput)
"""
def run(model, inputMode, pathOut):
	w, c, vecPol = load(model)
	fileOut = None
	fileIn = None
	
	if pathOut != None:
		fileOut = open(pathOut,"w")
	
	
	#Keyboard Input
	if inputMode == None:
		print "Type 'exit' to finish. (press ENTER after typing... ._.)"
		sentence = str(raw_input()).strip().lower()
		while sentence != str('exit'):
			tagClass = classify(sentence,w,c,vecPol)
			if pathOut == None:
				print tagClass
			else:
				fileOut.write(tagClass + '\n')
			sentence = str(raw_input()).strip().lower()
			
	#File Input
	else:
		fileIn = open(inputMode, "r")
		for l in fileIn:
			tagClass = classify(l.strip().lower(),w,c,vecPol)
			if pathOut == None:
				print tagClass
			else:
				fileOut.write(tagClass + '\n')
		
	if fileIn != None:
		fileIn.close()
	
	if fileOut != None:
		fileOut.close()
	

def main(args):
	
	if '-train' in args:
		if len(args) < 5:
			print "Not enough parameters for training."
		train(args[2], args[3], args[4])
		print "Done"
		return
	
	if '-run' in args:
		detail = False
		
		pathIn = None
		pathOut = None
		
		i = 1
		while i < len(args):
			if args[i] == "-inputFile":
				pathIn = args[i+1]
				i = i+2
			if args[i] == "-toFile":
				pathOut = args[i+1]
				i = i+2
			i += 1
			
		run(args[2], pathIn, pathOut)
		
		return()
		
		print "Done"
	
	print "Wrong format. Consult documentation."
	
	
	
if __name__ == "__main__":
	if len(sys.argv) < 3:
		print "Not enought parameters"
	else:
		main(sys.argv)
