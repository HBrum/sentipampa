Parece que a história não tem nenhuma intenção específica , e a narração é feita de maneira informal demais , com uso repetitivo de gírias e palavras de baixo calão . 
Para um livro tão famoso eu esperava qualidade muito superior . 
Creio que o livro é - ou foi - superestimado . 
A história é fraca , ainda que interessante , valendo apenas por o perfil psicológico de o personagem . 
Não quer dizer nada . 
Às vezes ele é meio deprimente , e me deixa realmente com nojo , mas não é sempre . 
A história é interessante , mas a narrativa em si é muito , muito cansativa . 
Não leria novamente e nem recomendo . 
Eu já queria ler esse livro fazia algum tempo por ouvir falar o quanto ele era bom , só que eu me decepcionei , por mais que a história seja muito boa se bem analisada , o livro é cansativo já que a todo o momento o personagem principal narra toda a história de ele de forma negativa . 
Em o final achei que algo de especial aconteceria com os personagens , que Holden tomaria uma lição e visse que a vida não é de a maneira que ele pensa , mas me decepcionei . 
Talvez porque consegui entender o motivo de tantos terem se empolgado com o texto e , apesar de isso , o livro não me tocou , não conseguiu mexer com mim . 
Porém , com o passar de a leitura , a impressão inicial se esvai e percebe - se a imaturidade de o personagem , que de o alto de sua pseudo-superioridade odeia a mediocridade a sua volta . 
Em suma , uma história sem objetivo sobre um rebelde sem causa e , ainda por cima , com um final frustrante . 
Em a minha opinião , o livro é confuso , perturbador e entediante . 
O final não tem emoção , e nada é muito surpreendente em o livro todo . 
Como leitora diária de livros , posso afirmar que esta obra perdeu seu brilho . 
Não dá pra tirar uma lição a não ser a de que eu não precisava ter lido e muito menos comprado . 
Mas esse é o tipo de humor cansativo , principalmente por que o tal Caulfield é um maníaco depressivo muito chato . 
Tudo ele acha ruim , tudo lhe chateia , qualquer coisinha insignificante o incomoda , o cara é um tremendo de um mala sem alça , insuportável . 
Ele é um boçalóide metido intelectual . 
O livro é tão ruim que levei dois meses para terminar de lê - lo . 
E pensar que deixei de comprar ' Ensaio Sobre a Lucidez ' por causa de esse livro me dá náuseas . 
Um lixo , uma verdadeira porcaria ... nunca li algo tão cretino em a minha vida e não me conformo de saber que as pessoas o consideram o livro de suas vidas . 
Não recomendo ! 
O livro é idiota , repetitivo , cansativo e irritante , tanto quanto seu narrador . 
Lixo de livro . 
Péssimo ! 
Um de os piores livros que já li . 
É chato , sem sentido , a história é fraca e a linguagem não prende ninguém . 
Tinha grandes espectativas e me frustei . 
Odiei ... Li as primeiras páginas e já não curti a linguagem de o livro . 
É uma leitura fácil e superficial . 
Não acrescenta muita coisa a o leitor , apesar de ter alguns trechos interessantes . 
Tem o seu valor para o público pré-adolescente , principalmente , mas , a a medida que passam - se os anos , faz cada vez menos sentido . 
Porém , o livro é a descrição de os pensamentos e '' aventuras '' , em Nova Iorque , de um adolescente fútil e fracassado , cujo conteúdo de o livro nada acrescenta a o leitor . 
Decepcionante , tempo perdido ! 
Eu poderia e devria ter lido coisa melhor . 
não gostei 
simplesmente sofri até a ultima página louca pra terminar . 
Essa edição que adqueri tem os diálogos separados apenas por vírgulas , o que dificulta o entendimento . 
Mesmo assim , não chegou a me impressionar com obra de 5 estrelas . 
Só achei que o final podia ser mais ... Esse livro só me deu 1 problema : conseguir os outros livros de o Saramago para ler . 
O que complica a leitura é a forma como é escrita somente , pois quase não tem divisão de parágrafos e causa certo desconforto . 
Mas eu também não o achei maravilhoso . 
Leitura difícil por causa de o português de Portugal e de o estilo peculiar de o Nobel José Saramago . 
O meio ponto que tirei foi porque o começo não me cativou muito . 
Esperava um pouquinho mais . . 
Mas nao considero um Bestseller ou um de os melhores livros de o Autor . . Final Pobre . 
A história aqui poderia ter sido contada em um numero menor de paginas . 
De a forma como foi escrito o livro acaba tornando - se um pouco enfadonho e repetitivo , e o entusiasmo fica menor com a seqüência de o desenrolar de a história . 
O final é ridículo ! 
Detestei o livro por causa de isso . 
Fora que a história não tem clímax . 
Não recomendo . 
Para você curtir o livro , você tem que superar algumas '' barreiras '' , a primeira é que o livro esta em português de Portugal , então tem algumas palavras que tem que ser deduzidas por o contesto . 
A segunda barreira é que Saramago escreve com poucos parágrafos , o que A as vezes torna a leitura um pouco cansativa , os diálogos são indiretos , mas não atrapalha em nada a historia , de a pra entender perfeitamente as discussões , mesmo sem travessão , você percebe quem ta falando o que . 
A única coisa que eu não gosto em ele é a escrita um tanto diferente por o autor ser português . 
Só achei cansativa a leitura pois esta em português de Portugal . 
Ensaio Sobre a Cegueira é primoroso em os detalhes e em os perfis psicológicos de os personagens . 
O enredo é muito bem amarrado , mas o mais fantástico de tudo em o livro é que ele escancara a essência humana : em situações extremas não passamos de animais . 
Talvez o melhor livro de Saramago , em a minha opinião . 
Um livro forte e sensivel a o mesmo tempo . 
Está entre os melhores que já lí . 
Muito , muito , muito , muito bom . 
Tive que ler para a faculdade e adorei . 
Acho que um de os melhores livros que já li . 
Recomendo ambos ! 
Gostei bastante de o livro . 
Demais ! 
Chocante , duro e temeroso . 
É de embrulhar o estômago e desnuda a alma humana como nenhum outro que já li o fez . 
Uma leitura realmente fascinante . 
Fantástica história , a forma como os personagens se desenvolvem . 
Prende de o início a o fim . 
Em '' O outro lado de a meia-noite '' o brilhante autor constrói uma história totalmente sólida de amor , ódio e vingança . 
Todos os personagens são diferentes e únicos . 
O diferencial de esse livro é que nossos sentimentos por os personagens principais mudam a cada página lida . 
'' O Outro lado de a meia-noite '' não é apenas um livro que lemos de o início a o fim apenas para nos entreter . 
Ele é tão bem escrito que parece que estamos em um teatro assistindo a uma grande história . 
Leitura MUITO RECOMENDADA ! 
Se você está querendo comprar algo para ler , pode comprar sem pensar duas vezes ! 
Gostei bastante , gente ! 
Foi o primeiro livro de o autor que eu li , achei surpreendente e muito bom - : 
Mais a personagem central de essa história para mim é a Noelle , nossa anti-heroína , ela é uma vilã fascinante difícil de odiar , você sente certa fascinação e atração por ela , além de bela tem um magnetismo , que nós poderemos sentir através de personagens que passaram por sua vida em diferentes fases , já em as primeiras páginas temos um vislumbre de a sua personalidade sobre pontos de vista diferentes . 
Este livro realmente me surpreendeu . 
Logo em o inicio já temos uma idéia de o fim , mas mesmo assim , o fim conseguiu me surpreender . 
Mas em um contexto todo , o livro conseguiu suprir minhas expectativas , principalmente em o fim . 
Para aqueles que gostam de histórias que se desenrrolem lentamente , esse livro é ideal . 
É cheio de supresas , segredos , enfim , é um ótimo livro . 
O enredo é envolvente , assim como todos de o SS , mas a o mesmo tempo é previsível . 
Um de os melhores livros que ja li sem duvida . 
Quando comecei a ler não consegui parar mais . 
Perfeito para quem gosta de mentes sombrias com um toque de romance , aventura e guerra . 
Bom não tem como descrever tudo o que o livro aborda , mas com certeza qualquer um que leu ou vai ler ficará triste quando chegar a o fim . 
Por falar em fim é um de aqueles que só Sidney sabe fazer de deixar de boca aberta . 
UM SENHOR BEST SELLER ! 
Me apaixonei por o livro , por o autor e por todas as outras obras de ele , as quais fui em busca para ler uma por uma . 
Foi o outro lado de a meia noite e Sidney Sheldon que me abriram as portas de o mundo de a leitura , de o qual não consegui mais sair , e nem quero . 
O romance é perfeito em tudo , desde personagens , lugares , e claro seu maravilhoso enredo cheio de reviravoltas , surpresas e muito suspense . 
Pra mim é um livro inesquecivel e perfeito . 
Acho muito dificil alguém ler e não gostar , mas pode acontecer , como eu não sei ! 
Não gosto muito de estórias que tratam de paixões , principalmente se for um triângulo amoroso , mas Sidney Sheldon sabe muito bem como nos prender a a estória e acabei que gostei de o livro . 
trama excelente 
A trama de este livro é simplesmente excelente , não é um livro previsível e o final é surpeendente . 
Ameeeei a personagem Catharine . 
Mas em a verdade todos os personagens foram muito bem montados . 
Li em dois dias e só descansei qdo descobri tudo . 
Vale muito a pena . 
Esse ainda está incluido entre os bons de o cara . 
Marcou minha adolescência 
Eu li O outro lado de a meia-noite quando tinha 13/14 anos , Amei a estória muito bem escrita , é , junto com o Reverso de a Medalha , um de os meus preferidos de Sheldon . 
Adoro . 
è um bom livro , prende sua atenção , vc começa e não quer parar mais . 
Acho esse livro impressionante . 
Acho impossível não manter uma relação de amor e ódio com Noelle por exemplo , que em a minha opinião é uma de as personagens mais fortes que Sheldon já criou . 
Sidney me surpreende a cada livro . 
Em este livro emocionante , Sidney consegue mostrar duas mulheres totalmente distintas , mas com uma coisa em comum : elas amam o mesmo homem . 
Recomendo ; DD 
Lembro que gostei bastante e que Noelle era uma verdadeira P ... Ela sabia usar o poder de a sedução com os homens que caiam como patinhos . 
É interessante a trama de cada personagem . 
Mesmo cortando as tramas , a gente se envolvia rapidamente em a outra e assim o livro seguia . 
Muuuuuuuuuuito bom ! 
Um livro eletrizante ... duas mulheres que sofrem por causa de um mesmo homem ... isso não poderia acabar bem não é ? 
O mais interessante é que o final foi imprevisível e surpeendente . 
Muito bom ! 
Não me recordo de os pequenos detalhes , mas esse livro está em o top 5 de os livros de o Sidney que marcam o leitor . 
Apesar da Noelle não ser uma pessoa com boa conduta e tudo mais , é a minha personagem preferida , não sei explicar o por quê , simplemente li e gostei mais de ela . 
Gostei bastante de esse livro de SS ... uma trama bem feita , mais aberta sem aquele suspense pra depois escancarar tudo de vez , o que as vezes fica meio chato com o decorrer de a estória . 
O final não deixou nada a desejar . 
Recomendado . 
noossa , personagens incriveis ! 
historia incrivel . 
Quando começa a ler nao consegue parar mais ! 
Recomendo ! 
Livro para ningupem botar defeito . 
Uma história cheia de requintes de suspense e drama , Sidney sheldon mais de o que ninguém sabe entreter seu leitor de um jeito que ficamos ecstasyados rsrs . 
Adorava a aldácia de Noelle personagem que eu mais gostei . 
Mesmo eu gostando de Noelle achei o final fantástico , um livro pra sempre se recordar . 
É uma história fascinante , com um final a o mesmo tempo satisfatório e triste . 
Maravilhoso ! 
Você começa a ler e não consegue parar mais . . O livro é conta sobre o quanto o desejo de vingança pode custar caro . 
Simplesmente muuito bom ! 
Recomendo ! 
Um livro bom . 
História muito bem elaborada . 
Verdade esse é talvez o livro que perdi as contas de quantas vezes eu reli . 
Já que é um livro tão bom que sempre pegava emprestado para ler , pois é um livro que entrou em as lista de mais vendidos por dezenas de semanas . 
Excelente livro ! 
Muito Bom ! 
Recomendo ! 
A leitura me surpreendeu de o início a o fim . 
Sofri com a protaginista a cada nova página . 
Foi um livro que me emocionou muito . 
esse livro como todos os outros de o Sidney Sheldon é maravilhos , instigante , empolgante ... enfim muito bom e o mais incrivel é que quando eu assisti o filme eu pensei : ` vai faltar muita coisa e não vai ser tão bom quanto o livro . 
' errei o filme é muuito bom é claro que falta alguns pequenos detalhes né mais é bom mesmo ... Recomendo tanto o livro quanto o filme ! 
Já perdi a conta de quantas vezes reli esse livro ... e cada vez que releio , é como se fosse , de novo , a primeira vez ! 
Noeelle não é a minha heroína favorita , mas com certeza , está em o topo five ! 
Achei simplesmente incrível . 
Os personagens criados em essa história são maravilhosos , e a forma como o autor os desenvolve a o longo de o livro é fascinante . 
Quando os destinos de os três principais personagens se entrelaçam , parar de ler se torna impossível . 
O julgamento em o último capítulo é emocionante , e o final , apesar de muito triste , é ótimo . 
Recomendo a qualquer um que desejar um bom livro ! 
Com uma história empolgante e personagens fascinantes . 
Um bom livro , mas sem dúvidas não é o melhor de o autor . 
A o longo de o livro somos testemunhas de o poder de Kate e sempre ficamos impressionados como ela sempre estava a um passo a frente nos surpreendendo com a força vital de uma mulher que me deixou marcado por sua determinação . 
Outros personagens marcantes foram as netas Eve e Alexandra com aparência idêntica , mas com personalidades diferentes . 
Adorei o livro assim como todos que já li de o Sidney . 
'' O reverso de a medalha '' não é um livro ruim . 
A trama é movimentada , abrange um longo período de tempo e três gerações de a poderosa família Blackwell em uma trama de muitas reviravoltas , como é característico de o autor . 
Gostei particularmente de a primeira parte de a história , que se passa em a África de o Sul e se foca em James Blackwell , um homem corajoso , perseverante e movido por o desejo de vingança . 
Embora o foco de o livro de uma maneira geral seja Kate Blackwell , foi James quem achei mais interessante , cheio de qualidades maravilhosas e defeitos terríveis , como só os bons personagens podem ter . 
Ora , se a trama é boa e em ela há por o menos 1 personagem memorável - também gostei muito de Margareth - , por que apenas 3 estrelas ? 
Para quem gosta de histórias rápidas , com muita ação e sem perda de tempo com grandes descrições de ambientes ou sentimentos , Sidney Sheldon é o autor certo e '' O reverso de a medalha '' é uma de suas melhores histórias . 
Mesmo para aqueles que , como eu , não apreciam muito esse tipo de escrita , '' O reverso de a medalha '' é um bom livro para se ler naqueles momentos em que se quer uma leitura leve , sem grandes compromissos . 
Leiam esse livro . 
não irão se arrepender , até hoje foi o melhor que já li ! 
Uma de as melhores obras que eu já li . 
Que livro maravilhoso . 
Adorei o livro . 
É uma obra prima , cheia de tramas e assassinatos . 
Adoroooooo esse livro ! 
marcou minha juventude . 
A história de esta família marcada por assassinatos , escândalos , muito amor , ódio e riqueza em os guia através de cada capítulo em os mantendo '' presos '' a a narrativa . 
Os personagens são carismáticos , humanos , enfim reais ... e com suas qualidades e defeitos , erros e acertos são o ponto forte de a trama . 
Enfim , o mestre Sheldon conseguiu com o livro construir um thriller envolvente e instigante . 
O melhor de Sidney Sheldon 
Foi uma de as melhores escolhas que já fiz em a biblioteca . 
Sem a menor dúvida , esse livro é a obra-prima . 
Ainda que siga a mesma receita de os outros livros , o autor vai além e cria uma história longa , complexa e emocionante . 
Não importa quantas vezes eu leia - até onde eu me lembro foram umas 5 leituras - , '' O Reverso de a Medalha '' sempre vai me prender e me deixar tensa até que eu chegue em o final . 
Escrita A trama e o desenvolvimento de a história é riquíssima em detalhes , mas conhecendo o autor , ele detalha de forma magistral , sua escrita inconfundível , simples , direta é o seu maior trunfo em o livro , pois '' persegue '' o leitor , sempre querendo ler o próximo capítulo . 
Para os fãs e os que querem conhecer o autor , não se esqueça de ler este livro ! 
Que história ! 
Gostei bastante . 
O melhor livro lido em a minha vida ! 
Comecei esse livro com muita expectativa , afinal , sou fã de Sidney Sheldon , porém , logo em as primeiras páginas desanimei um pouco , parece que demorou uma eternidade para aquelas poucas páginas passarem , mas de novo , estamos falando de Sidney Sheldon , e quando Jamie apareceu , o mundo estava bom de novo . 
Jamie , David e Tony foram as partes que me fizeram dizer que valeu a pena a leitura . 
Pode - se dizer que o livro tem seus autos e baixos , mas em meu conceito , ficou apenas em o bom - e que de acordo com o Skoob , são três estrelas - . 
Faz com que leitor fique preso a a narrativa . 
É envolvente e instigante . 
Efim é o melhor livro de o Sidney Sheldon que li até hoje . 
O reverso de a medalha foi o primeiro livro de Sydney Sheldon que tive contato , foi uma boa primeira impressão , o livro consegue te deixar ansioso boa parte de o tempo , querendo saber qual vai ser o próximo golpe que os Blackwell vão planejar , seja em os negócios ou dentro de a sua caótica família . 
O livro se divide em arcos mostrando os anos e em torno de quem a historia vai girar , devo dizer que meus favoritos foram os que envolveram Jamie McGregor , Kate - jovem - e Tony . 
Tony foi um personagem que não dava nada , mas acabou se tornando meu favorito , ele foi o primeiro a lutar contra a maldição que é o peso de a corporação Blackwell , e talvez o que sofreu mais com isso . 
Entretanto todos os arcos conseguem te prender a o livro e as paginas simplesmente voam ! 
Depois de O Reverso da Medalha eu definitivamente pretendo ler mais obras de o Sydney Sheldon , fica a recomendação pra quem se interessar também ! 
Esse livro foi contagiante e profundo . 
Repleto de ação , suspense , aventura , sedução , intrigas , muitas reviravoltas e eletrizante de o princípio a o fim ! ' 
Em a minha singela opinião , esse romance de Sidney Sheldon é um clássico , onde o autor narra a história de várias gerações de a família McGregor , que fundou seu império com os diamantes de a África de o Sul , chegando até a protagonista Kate Blackwell , que é uma mulher sensual , bela e extremamente impetuosa . 
Essa , sem a menor sombra de dúvida , foi a melhor parte de o livro , suspense de o princípio a o fim durante a travessia de os dois por o deserto cheio de armadilhas e perigos ... Muito emocionante ! 
Sem dúvida nenhuma , podemos tirar uma grande lição de como aprender de forma intuitiva e prazerosa a o passar por as páginas de esse romance fantástico que mistura realidade e ficção a ponto de deixar o leitor sempre atento para verificar o destino de uma senhora ambiciosa que é capaz de sacrificar o próprio filho para centralizar suas riquezas , uma neta também arteira e mal-caráter que entre tantos desmandes e armações conseguem a maioria de seus objetivos financeiros . 
Ação e Mistério de tirar o fôlego , mas início arrastado 
Mas o Reverso da Medalha me cativou com nenhum outro . 
Mas a partir de o momento que a ação começa , é impossível fechar o livro novamente . 
Recomendo a todos , ótimo livro e merece meu respeito . 
Emocionante de o princípio a o fim : a chegada de Jamie a a Africa de o sul , o achado de o primeiro diamante em a praia de a Namíbia , as ruas boêmias de Paris , as mansões imponentes de a Inglaterra , as salas de reuniões de a alta sociedade , as alcovas de a América , até a família Macgregor se tornar a elite européia dona de um vasto império comercial que se expandia cada vez mais , de acordo com os planos de a ambiciosa Kate Blackwell , que a meu ver , é uma de as mulheres mais fortes de a literatura . 
Maravilhoso . 
Cada membro de essa família tem uma história melhor de o que a outra . 
Empolgante ! 
O Reverso da Medalha é um de os meus preferidos ! 
A história é envolvente , não há como não sentir - se preso A as páginas de o livro , com uma grande vontade de conhecer seu desfecho ! 
Os personagens são marcantes , e trama criada por o autor é de fazer prender o fôlego ! 
Quanto mais nos aproximamos de o final , mais emocionante fica ! 
Eu não via a hora de terminar a leitura de as 459 páginas ! 
Eu li , reli e recomendo ! 
É o melhor livro que já li . 
Depois nos orgulhamos de Kate , para nos decepcionarmos com ela mais a frente . 
Eve é a perversa . 
Sou fã de o Sidney , pois ele sabia escrever com maestria , nos envolver em a história a ponto de não querermos largar , fazia muito tempo que não lia um livro de 459 páginas de um dia para o outro . 
Valeu cada hora de sono perdido ... rsrsrs . 
Super recomendo ... Nota 10 ! 
Beijokas elis ! 
Me surpreendi bastante , tem uma história interessante e com um gostinho de quero mais . 
E ele não se prende em apenas um personagem . 
sem dúvida o melhor livro de o sidney sheldon ! 
muitoooooooo bom ! 
Ruim , uma estória muito previsível , como em todos os livros que ele escreve . 
Uma emoção muito grande mesmo após a leitura . 
Ficou muito feliz como acaso de ter achado esse livro e o escolhido entre as opções que eu tinha em a biblioteca . 
A importância que Kate tem por a companhia che ga a ser apaixonate , a o mesmo tempo muito irritante , mas apaixonante novamente . 
A maldade de alguns personagens chega a ser chocante , mas alguns personagens como Banda , Margaret , Tony , Jamie e Kate toranram - se meus favoritos . 
Eu que normalmente me interesso por histórias sobrenaturais , me interessei mil vezes mais por a escrita brilhante de Sidney Sheldon . 
Ameei ! 
Kate Blackwell essa sim é uma de as personagens mais fantásticas de o Sidney Sheldon , essa vontade de ela de querer controlar tudo e a todos , a sagacidade de ela , só posso dizer que é uma personagem fantástica . 
Adorei o tempo em que se passa a história toda a árvore genealógica o inicio de a fortuna de a familia , os acontecimentos a o redor de o mundo com o passar de a história , a história de cada geração a mudança de cenário e a rapidez de a história sem fugir a os detalhes e fatos , adorei . 
O mestre ! 
De o ínicio a o final ! 
E quando você pensa que não há mais nada possível para acontecer , mais uma vez você é surpreendido ! 
Valeu a pena cada caractere de o livro e cada milésimo de o tempo gasto . 
Esto ansioso para ler a sequencia , que não foi escrita por Sheldon , uma vez que já é falecido . 
Somos absorvidos por as intrigas e por os problemas de a família e de a empresa de Kate . 
Em o começo eu a adorava , achava um mulher forte e maravilhosa por estar determinada a não ser igual a todas as outras mulheres . 
Ela se apaixonou por um homem 22 anos mais velho e jurou , ainda criança , que um dia se casaria com ele . 
Adorei sua trama , as vinganças , os planos bem elaborados , as reviravoltas , os dramas . 
Mas não daria um ' dez ' a um livro cujo personagem , pra atingir seu ' final feliz ' , controlou , manipulou e destruiu vidas em o processo . 
Não dá pra parar de ler . 
Apesar de ser considerado baixa literatura ou de entretenimento não dá para negar o poder de envolver o leitor em cada página . 
Kate Blackwell , ou você a ama ou a odeia , mas não dá para não admirá - la . 
Tenho que ser coerente e dar 5 estrelas pois me senti totalmente envolvida em a história . 
maravilha de livro 
Não sei como esse autor consegue escrever com tanta genialidade . 
Adorei 
Resenha - O Reverso da Medalha 
'' O Reverso de a Medalha '' gira em torno de quatro gerações de uma família . 
Personagens bem carismáticos , humanos , com suas qualidades e defeitos , erros e acertos são um ponto forte de a trama . 
Sem contar a passagem de a história , desde o tempo de as diferenças raciais entre os pretos e os brancos em a África de o Sul e as duas grandes guerras . 
'' Uma ova que me conhecem ! 
O Reverso da Medalha é de longe uma de as obras mais fortes e bem montadas de o gênio que Sidney Sheldon foi . 
E com certeza , uma de as grandes obras de a literatura romancista . 
O livro está marcado por personalidades fortíssimas , apaixonantes , eu deveria dizer . 
A mais forte de elas é sem dúvida , Kate Blackwell , o sobrenome indica um ' bem negro ' , e acaba por indicar de uma forma geral a personalidade de tão intrigante personagem . 
Leitura obrigatória para os amantes de a literatura . 
Team Kate 
Amei o livro . 
ÓTIMO ! 
http://naoconsigoevitar.blogspot.com/2010/08/livro-o-reverso-da-medalha-de-sidney.html 
adoreii , realmente SIDNEY SHELDON soube dispertar emoções em esse livroo ! ... 
Eu sou fãzona de Sidney Sheldon ! 
Diamantes 
Eu tinha 15 anos quando meu pai me recomendou esse livro . 
Foi por causa de ele que tomei gosto por a leitura . 
Não existem palavras que possam definir essa obra prima . 
Sidney Sheldon mais uma vez dá um banho em seus sucessores que tentam se igualar a mente de seu mestre . 
De a ambição a o poder 
O Livro tem como cenário a África de o Sul , Paris , Nova York e Mansões de a familia . 
Alex e Eve eram irmãs gêmeos porém com personalidade totalmente distintas , Eve sempre odiou a irmã e sempre teve inveja de a mesma , tramou diversos planos maléfolos para destruir a próprias irmã , pois ela desejava ser a Única herdeira de Kate Blakwell . 
Enredo eletrizante , muitas reviravoltas em o desenrolar de a trama , final previsível , mas que gera grande expectativa em o leitor . 
Apesar de Kate Blackwell ser a protagonista , sua neta Eve - uma de as gêmeas - rouba a cena em o final . 
nada a comparar ! 
Para mim , SIDNEY SHELDON É O MELHOR ESCRITOR DE O MUNDO ... não tem comparação . 
indico a todos ! 
bjus 
Sidney Seldon cria mais uma heroína que apaixona milhões de leitores em todo o mundo : Kate Blackwell , uma mulher rica e poderosa que controla um grande conglomerado internacional e sabe de côr todos os segredos e truques de o mundo de os negócios . 
Muito bom . 
Continua com a '' Senhora de o Jogo '' que Sidney deixou boa parte escrita - já que faleceu por causa de uma pneumonia - e outra escritora deu continuação a o livro - a família de Sheldon que pediu a ela - e logo eu irei ler . 
Sou fã número um de Sidney Sheldon ... o primeiro livro que li foi '' O outro lado de a meia noite '' ... li umas 10 vezes , em o mínimo ! 
várias gerações de uma família milhonária ... com mto suspense , drama , e eu como sou mto curiosa ... tive que devorá - lo ! 
A mais marcante é sem dúvida Kate Blackwell , uma mulher de garra que não poupa enforços em sua vida profissional , transformando a herança de o pai em uma de as mais poderosas holdings de o planeta . 
É uma mulher que só joga para ganhar , aniquilando seus concorrentes em o mundo de os negócios , fazendo fortuna e exapandindo seu império por todo os cantos de o planeta . 
Apesar de muito suspense , emoção , drama ... este livro tem uma bela mensagem . 
Trata de uma matriarca que consegue mudar o destino de todos a o seu redor , inclusive de a própria familia . 
Suspense . 
Imprevisibilidade . 
Drama . 
Maniqueísmo . 
: - 
Sem duvida ... umas de as melhores obras escritas por um humano . 
Sidney Sheldon se consagrou em essa história marcante e , para mim , perfeita . 
Uma de as caracteirsticas que gosto em ele , é a de saber prender o leitor . 
foi o 1º que eu li e foi amor imediato . 
Ele conta a estoria de 3 gerações cheia de altas e baixa mas que depois de tanta luta e determinação tudo dá certo , uma leitura apaixonante que te segura de o começo a o fim . 
Eu recomendo ! 
Já li umas 10x por o menos é a minha reliquia . . 
'' Saga emocionante '' , mas com informações sobre o mundo e as pessoas 
Tramas interessantes , cheias de detalhes , de qualidade , ricas , envolvendo coisas de o mundo e traçoes de personalidades , muito bem escrito , muito bom de se ler - inclusive em as questões relativas a amor , sexo , relacionamentos , acaba te prendendo e te interessando , devido a riqueza de informações de o autor e forma de escrever - , e as últimas páginas são eletrizantes , de um suspense incrível e que mexe com você - e o final é fantástico e hilário , mas muito verdadeiro - . 
Pensei em ler somente este livro de o autor porque era o mais bem comentado e muito elogiado , mas ele me cativou , conquistou , por a riqueza de informações que dá em a obra , sobre o mundo e suas coisas , sobre as pessoas , e de uma forma muito bem escrita e cativante , envolvendo suspense e situações fortes mas envolvendo coisas de o mundo em suas diversas áreas , o que dá o tom de poderem sim ter o tom de verdadeiras - em o vasto mundo , ou então tirando - as pras coisas comuns - ; então me deu um querer de conhecer outras obras futuramente , os títulos de as outras obras são bastante sugestivos , como '' Lembranças de a meia-noite , '' O outro lado de a meia-noite '' , '' Manhã , tarde e noite '' , '' Nada dura para sempre '' ... ; acho que pode ser uma leitura muito boa de se ter , além de ser uma leitura '' bem gostosa '' , realmente - pra um momento de '' retomada '' a os livros , então ! 
- . 
Nunca tinha ouvido falar de esse autor , até a Jû e Maris me apresentarem . 
Aí a história se desenrola de entre 4 ou 5 gerações , onde a epopéia de a família Blackwell , seus podres e suas glórias são contados através de 100 anos de história . 
: - Um livro fascinante de suspense e armação que tem mais reviravoltas que novela mexicana quando começa a ficar sem assunto e tem mais 3 meses pra terminar . 
Ficaria ótimo em filme . 
Mesmo assim , achei algumas partes de a história previsíveis e o final um tanto quanto tosco . 
Cativante e muito inusitado . 
Recomendo 
Choque . 
Suspense . 
Imprevisibilidade . 
Drama . 
Maniqueísmo . 
: - 
Apesar de narrar a saga de uma família , O Reverso da Medalha centraliza sua história em Kate , e como sua figura dominadora mudou o destino de todos a o seu redor . 
E o desenrolar de os fatos é sempre surpreeendente ! 
Com uma narrativa que te prende de o início a o fim , o Reverso da Medalha é um de os livros que mais gosto ! 
Já li 3 vezes e recomendo ! 
Nossa esse livro é 10 ! 
Também né , Sidney Sheldon . 
