Apesar de muitas vezes ser uma leitura dificil , por causa de o tipo de linguagem usado , de os poucos paragrafos , de as falas sem travessões , apenas separadas por virgulas ou ponto final . 
Eu esperava bem mais de Ensaio Sobre a Cegueira . 
Esperava um livro fantástico e impressionante , mas em o fim acabei bem decepcionado por conta de alguns motivos . 
Por fim Saramago adora enrolar , o livro é repleto de frases de 8 , 9 linhas que não dizem nada , sendo simplesmente uma sequência de palavras que não adicionam nada - por exemplo : " O primeiro de a fila de o meio está parado , deve haver ali um problema mecânico qualquer , o acelerador solto , a alavanca de a caixa de velocidades que se encravou , ou uma avaria de o sistema hidráulico , blocagem de os travões , falha de o circuito eléctrico , se é que não se lhe acabou simplesmente a gasolina , não seria a primeira vez que se dava o caso " - , assim como inúmeros trechos que não adicionam nada A A história . 
O ritmo de a história é um tanto quanto inconsistente , em o geral é tudo meio lento e talvez por o volume de texto tem - se a impressão de que a coisa não está evoluindo . 
Os personagens principais são muito mal desenvolvidos , interpreto isso que o autor queria colocar o foco em o ' todo ' , mas é um tanto quanto estranho acompanhar um grupo de personagens tão simples . 
E a conclusão de a história me pareceu um tanto quanto preguiçosa , pra não dizer meio que vazia , afinal muito de o propósito de todos os acontecimentos acabaram se perdendo em o desfecho . 
Nunca sofri tanto para ler um livro . 
Mas , realmente , depois de ter lido , Memorial de o Convento , A jangada de pedra e outros mais de o Saramago , tenho a dizer que esse livro me atormentou e não voltarei a lê - lo tão em breve ! 
Em o entanto , acho que criei muita expectativa e quando o final realmente chegou , não achei tudo aquilo ... Para mim , foi só bom . 
Confesso que em o começo não gostei . 
A falta de pontuação ás vezes me confundiu por o fato de não saber se era a personagem quem estava falando ou o narrador . 
Em uma palavra : chato . 
O que chega a os dois únicos pontos negativos de o livro : 1º é a questão de que os capítulos são imensos , quase que intermináveis , o que deixa a leitura mais lenta , pesada , massante . 
Outro ponto negativo é exatamente isso , em certos pontos fica dificil de prosseguir com a leitura , o livro fica muito massante . 
Mas confesso que este não está em a lista de os meus livros preferidos de ele . 
Mas achei o final mto triste , não gostei não . 
E também não podemos esquecer o Larry Douglas , que eu particularmente detesto , mas isso só vocês lendo para entender . 
Não é uma obra prima literária com certeza , mas Sidney Sheldon tinha um dom que poucos tem : ele sabia como contar uma história e principalmente uma boa história ... Este é um de os melhores livros que li em minha vida . 
Chato , entediante , repetitivo 
Desisti de todos os demais títulos , não consegui terminar a leitura . 
O enredo é fraco e nada bem escrito , deixou muito a desejar . 
Li por ler , emprestado de um amigo . 
Com todo o respeito a quem gostou , mas sinceramente não consegui entender o que faz esse livro ser considerado bom . 
Achei a narrativa pobre , a história muito forçada e extremamente mal contada , os personagens " murchos " . 
Insisto : com todo o respeito a quem gostou , mas esse livro é desprezível . 
Não sei se recomendaria esse livro , penso que não com tantas opções em o mundo . 
Como disse em o título , o início de o livro não anima muito , pareceu bastante arrastado . 
Hoje a leitura fluiu mais rápida , mas depois de ler tantos livros de o Vonnegut e de tantos outros autores , eu acho o Sidney Sheldon muito prolixo e as vezes os mistérios são meio previsíveis . 
Ruim , uma estória muito previsível , como em todos os livros que ele escreve . 
Kate pode ser inteligente , mulher de negocios , linda ... mas parece que nao tem coração , passa por cima de td e de todos por a Empressa ! 
Odiei o que ela fez e as consequências que isso causou em a vida de pessoas que nasceram inocentes e que , tenho certeza , não teriam sido corrompidas se Kate não tivesse feito o que fez . 
Mas não daria um ' dez ' a um livro cujo personagem , pra atingir seu ' final feliz ' , controlou , manipulou e destruiu vidas em o processo . 
Em o inicio eu fiquei com a impressão de ser um plágio de O Conde de Monte Cristo , mas só a primeira parte é parecida . 
Mesmo assim , achei algumas partes de a história previsíveis e o final um tanto quanto tosco . 
Você tem uma protagonista burra que dói que faz uma besteira atrás de a outra , acaba presa por um crime que não cometeu e entra pra faculdade de a malandragem em a penitenciária . 
Parece legal , mas dá sono . 
Agora , os personagens , os relacionamentos e os diálogos são muito fracos . 
Os personagens passam a se amar e a se desamar de uma hora pra outra e eles são completamente volúveis e superficiais . 
Não é um romance como pretende ser e nem é original ; é uma coletânea de contos sendo vários de eles pirateados , inclusive um de escritor brasileiro . 
O início de a história em nada se relaciona com o meio ou o final . 
O livro é envolvente e tal , te prende em o começo , mas de o meio pro final parece meio forçado , o autor meio que começa a inventar histórias demais , não havendo uma ligação coerente entre elas , fica uma impressão de que se está lendo algum tipo de livro seriado , sei lá , com várias histórias diferentes em o mesmo livro . 
Se não houver coisa melhor 
Se Houver Amanhã não é tão ruim quanto eu imaginava , é claro que o livro não é um 5 estrelas , a prosa é pró lixo , superficial de mais , não permitindo que vc se apegue a os personagens e torça por eles . 
Apesar de as cinco estrelas , o livro possui alguns pontos fracos : A história em si é um pouco exagerada , principalmente por as barbaridades que acontecem em a vida de Tracy . 
O fato de a narrativa ser muito dinâmica e de não se abster em uma mesma situação por mais que algumas poucas páginas , faz com que alguns trechos se tornem ainda mais exacerbados . 
Enredo mirabolante , trama envolvente mas chega a haver exagero . 
nao chega a ser o melhor romance .... Gostei de o livro ... Não é uma historia maravilhosa mas ele te prende bastante ... so q alguns roubos de a protagonista foram sem muita explicação ... mas vale a pena le - lo . 
Parece uma versão mais recente de a coleção Bianca e outros de o gênero . 
Constrói uma imagem patética de a heroína , que tem que passar por várias injustiças para se tornar uma ladra . 
O único ponto que achei negativo , é o fato de ter muitos palavrões ! 
Apesar de isso esse livro não me agradou tanto por o final . 
Frustrante . 
Confesso que me decepsionei um pouco . 
Mas , o que me irritou em o livro é o fato de que a personagem principal tem um objetivo em o começo de o livro , que ela o resolve em alguns capítulos e depois o autor continua contando a história como se fosse uma espécie de diário de a vida de Tracy , os objetivos de ela mudam e a história se torna cansativa , parace que não acaba nunca . 
O romance entre Tracy e o outro personagem também é muito óbvio , é quase uma história paralela que serve para preencher a lacuna de tempo entre os roubos . 
Me decepsionei de novo . 
Um livro ótimo , totalmente fantástico assim como todos os outros , Meyer nos transporta para um novo mundo , ela criou vampiros diferentes , com a capacidade de amar , e nos fazer amar o sobrenatural em si . 
Recomendo ! 
Nunca tinha me interessado por vampiros antes , e achei o enredo de crepúsculo fantástico , diferente , uma coisa que realmente me prendeu . 
O primeiro livro foi muito bom , pecou apenas em algumas partes extremamente melosas e com a personalidade super protetora de o Edward , que muitas vezes é muito chato . 
Eu gostei de o livro . 
Bom ! 
Em a minha opinião o livro é muito bom pra quem é amante de livros românticos com um toque de suspense . 
Um livro maravilhoso que trouxe uma nova forma de se olhar para o mundo de os vampiros , através de Edward Cullen . 
Ameii o primeiro livro .... Bella Swan muda - se de a ensolarada Phoenixpara a chuvosa cidade de Forks , Washington , para viver com seu pai , Charlie , o chefe de a polícia local . 
É um bom livro , bem melhor de o que o filme ! 
Suspense , romance e ação permeiam este livro , para quem gosta de este tipo de enredo que mistura sobrenatural moderno com realidade é muito bom . 
O início de o livro é bem interessante , pois conta um pouco de a história pessoal de Bella , Edward e de o próprio Jacob para podermos entender melhor o desenrolar de as ações . 
Como a história é leve , te prende de o início a o fim e você fica querendo ler o próximo - já que é uma série - para querer saber o desenrolar de o triângulo amoroso que se inicia quase em o final de o livro . 
Eu li , e confesso que gostei , mas hoje vejo que a historia é fraca e sem sal , não tem conteúdo ... sei lá ! 
Adorei o livro mesmo que digam que não é bom . 
Ele é lindo , emocionante , envolvente , jamais nenhuma mulher ira ver os homens de a mesma forma ; por que dizer te amo , se você pode dizer : " você é minha vida agora " . 
Um livro mágico e que atingiu o coração de milhares de pessoas sendo elas adolescentes , jovens e adultos . 
Simplesmente magnifico . 
Mas é um livro muito bem escrito , gostei bastante . 
Mas foi uma ótima aquisição porque fiquei completamente apaixonada por ele e sempre indicava pra todas as pessoas que eu conhecia . 
Me apaixonei não só por a história de amor entre Bella e Edward , mas também por o carisma de os outros personagens como Alice e Carlisle , irmã e pai de Edward . 
Literatura Fantástica viciante . 
Então por mais que seja uma leitura simples , boba ... eu me apaixonei por essa história e já li mais de 5 vezes esse livro . 
Gosto de o diálogo de os personagens principais , gosto de sentir o que eles sentem , me colocar em o lugar de eles ... fabuloso ! 
É um livro rápido , de leitura fácil e agradável , faz você lembrar como é bom está apaixonado . 
Uma boa pedida quando se tem tempo livre .. 
Crepúsculo - diferente de o que alguns dizem - é muito bom - para os meninos preconceituosos , não me levem a mal , o livro é realmente bom - , mas também muito meloso . 
Perfeito ! 
O melhor de a série , adorei e me apaixonei ... fui quase que obrigada a le - lo já que todas as minhas amigas já tinham lido e faziam a maior pressão rs , ainda bem : - que fui tão pressionada , me empolguei tanto que em a época li suas continuações em tempo recorde .. . 
Gostei . 
Tem 355 páginas de puro romantismo , o mais doce de eles . 
Ler Crepúsculo é embarcar em uma narrativa longa e bem detalhada . 
Particularmente gosto de o fato de a personagem seja um pouco de cada garota , mas o insegura , comum , desastrada , interessante , madura , independente - apesar de se tornar dependente de Edward - . 
É um bom livro ... 
A historia de prende de uma forma incrivel , acho que ja li por o menos umas 20 vezes , e mesmo assim ainda tenho vontade de vez em quando e começo tudo de novo , normalmente leio em 2 dias porque nao com si largar ele nem para comer ... Amo demais essa historia ! 
Com o passar de o tempo acabam namorando , mas essa fantástica história quase é destruida quando James quer matar Bella ... 
Ela leu o restante e gostou muito ... 
Gostei de Crepúsculo pois é uma História bem Instigante Bella vai Morar com o pai e em a Nova escola Acaba Conhecendo Edward Um Rapaz Bonito mas meio Diferente . 
Gostei ... mas Bella é muito chata ! 
Crepúsculo é o tipo de livro que só se larga em o final . 
Por isso não deixe de lê - la . 
VOCÊ NÃO VAI QUERER PARAR ! 
Bom para começar não achei Crepúsculo uma porcária como muitos dizem , mas também não é o livro perfeito e não entendo porque toda essa euforia por causa de o livro , poís nha minha opinião Lua Nova é melhor que Crepúsculo - bom isso não vem a o caso - . 
Gostei muito de os Cullen que são meus personagens famoritos , a familia mais badalada em o momentos e claro os vampiros nômades , principalmente Victória , Bom como ja dizia o titulo de a minha resenha , Crepúsculo é um livro simples , normal , mas é um livro bom ! 
É um livro altamente recomendado para adolescentes e não apenas por o fato de introduzir muitos a literatura , mas porque de maneira simples e rápida a autora nos gruda a a história de maneira impresionante . 
Muitos falam que não gostam de o livro por se tratar de uma moda passageira de vampiros com o ator X fazendo o papel de edward e a atriz Y fazendo a bella , porem é um livro que te leva para outro mundo como qualquer outro tão bom quanto . 
Muito bom ! 
Mas não um vampiro de o tipo Lestat ou Conde Drácula e sim um vampiro bonzinho , fofo , romântico , estonteantemente lindo - não que o Lestat não fosse , né ? 
O que eu posso dizer é que eu gostei de o livro . 
Lá pro final , a família de Edward surge e as coisas ficam mais interessantes ! 
Os lobos , achei super bacana , a familia de o Edward , enfim , tem sim bons personagens , mas concerteza a Bella não é a melhor de elas ... 
O primeiro livro de a série é bem divertido , uma leitura despretensiosa . 
É um ótimo companheiro para aquele tempo livre que precisamos que passe rápido . 
Em o começo , demora para engrenar , mas depois de as primeiras páginas , a leitura flui gostosa . 
Há um bom tempo atrás , logo que fiz conta em o Skoob , fiz questão de vir aqui malhar o livro , mas hoje , sinceramente , eu devo morder a língua . 
Não virei fã , mas devo confessar que me interessei bastante por a história . 
Crepúsculo é um livro passável , em a verdade . 
