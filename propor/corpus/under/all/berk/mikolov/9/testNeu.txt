Jorge Amado a o escrever capitães de areia , usou de todos os seus talentos , é interessante como em um livro de meninos de rua , que vivem em a verdadeira miséria ; podemos encontrar assuntos tão diversoos , como : a religão , que mostra o catolicismo e o candomblé , em as figuras de o Padre José Pedro e Don'Aninha . 
O amor inocente e desconhecido de Dora e Pedro bala , os prazeres de o sexo ; encontrado por os meninos antes mesmo de se tornarem adultos , a carencia por um amor fraternal , a desigualdade social e cultural , o descaso de as autoridades , os sonhos e desejos . 
Os capitães de areia são crianças , pobres , sem estudos e maltrapilhas ; porém , com o coração de adultos ; preservam os amigos , respeitam as leis ; não as nossas ; mas as leis de eles , as de os capitães de areia . 
É triste ler e lembrar que existem varios " capitães de areia " por aí . 
Por as ruas , em o frio , com fome e sede , necessitando de carinho e atenção . 
É triste e revoltante como existem pessoas que não se importam verdadeiramente com essas crianças , não percebem que o futuro de elas é tambem o futuro de o nosso pais . 
Mas os capitães de areia , acreditam que quem escolhe o nosso destino somos em os mesmos ; e assim eles seguem , crendo que a sorte irá virar e que um dia eles estarão em o lugar de aqueles que tanto humilharam e não se importaram como eles . 
Uma obra perene , sem dúvidas . 
XSPOILERSX 
O livro , acima de tudo , tem um caráter altamente satírico sobre a sociedade de a época , altamente descritivo e abstrato . 
Opinam entidades sociais , padre , mãe de um de os meninos , polícia . 
Aqui já é realçada uma cena bem explorada em o livro todo : a discriminação por pessoas pobres . 
Também mostra a tentativa de auxílio A A essas pessoas por parte de algumas pessoas , como o Padre José Pedro . 
A o decorrer de o livro , há a apresentação de os personagens principais : Padre José Pedro , Dona Aninha , Querido-De-Deus , João , Pedro Bala , João Brandão , Sem Pernas , Pirulito , Boa-Vida , Gato , Professor , Barandão , VoltaSeca , assim como mostra a personalidade de cada um . 
Com o passar de a história é mostrado também a solidão de os meninos , muitas vezes usando de a pederastia para receber carinho ; a sexualidade é outro importante marco em a história . 
A literatura de cordel é plenamente vista em a obra . 
Depois de desenrolares , cenas que explicarão o final de cada personagem , chega outra importante personagem : Dora , que junto com seu irmão vem para o trapiche - ambiente principal de a história - , pois perdeu seu pai e mãe vítimas de bexiga - alastrim - varíola - . 
Logo quando chegou , trazida por o Professor e por o João Brandão , os meninos tentaram abusá - la , assim como faziam com algumas meninas em o areal . 
Porém os 2 que a trouxeram a defenderam , depois ajudados por o Pedro Bala , que proibiu mecherem com a moça . 
Ganhando a afeição de muitos , e se tornando uma espécie de mãe para Pirulito , Gato e Volta Seca , além de irmã par a os outros , como para o João Brandão e pro Boa-vida . 
Pro Pedro e pro Professor ela era uma noiva desejada , o que era correspondido por ela a o Pedro . 
Em outra cena mostra Pedro Bala sendo capturado e conseguindo fazer escapulir alguns amigos em o seu furto . 
Ele vai pro reformatório e , Dora , pro Orfanato . 
Depois de dias sofridos , dolorosos e cruéis lá ele consegue finalmente fugir , indo libertar Dora , que ficou altamente com febre . 
Mesmo sendo tratada por muitos , ela acaba falacendo , nao sem antes chamar seu amante Pedro de noivo e eles fazerem amor - são apenas crianças de 16-14 anos - . 
Em a 3° e ultima parte , mostra Professor , que possui inteligência nata e dom artístico se mudando para morar com um artista em o rio , pra depois virar famoso Pintor ; Boa-Vida virando mais um malandro e sendo procurado por a polícia ; Sem-Pernas , sem amor nenhum , se " vinga " de os policiais nao se entregando e se suicidando ; VoltaSeca vai para o nordeste encontrar seu " Padrim " , o Lampião ; Joao Brandao vai trabalhar em navios , o Gato vira gigolô e é preso ; Volta seca posteriormente vira um temido acompanhante de Lampião e é preso , tendo em conta que matou 60 pessoas ; Padre José vai para uma cidade de o interior de o Nordeste com um lugar odne rezar as missas ; Pirulito vira frade , e , por ultimo , Pedro Bala vira um importante grevista , deixando o comando de os Capitães de Areia para o Barandão . 
Parece - se muito com o personagem Santo Cristo de a música Faroeste Caboclo , com um misto de a história de Peter Pan , que me deu um dejávu quando é narrado o sentimento maternal de alguns garotos por Dora . 
Com a força de sua prosa , Jorge Amado aproxima o leitor de esses garotos e de o desejo de liberdade 
Paula Cordeiro 
Consegui ver a Salvador que Jorge Amado traça . 
Consegui ver os meninos correndo em a Cidade Baixa e brincando em o Trapiche . 
E ... ESTA EM A LISTA DE LEITURA DE OS MELHORES VESTIBULARES PARA 2011 E 2012 ! 
Boa notícia , não ? 
. 
Lindo 
Um de os livros mais triste que já li em toda minha vida . 
Meninos livres , soltos , independentes , unidos , esquecidos . 
Capitães do Meu Coração 
Sinopse : " Tendo como cenário as ruas e as areias de as praias de Salvador , a história trata de a vida de meninos de rua sem família que viviam em um velho armazém abandonado em o cais de o porto . 
Os motivos que os uniram eram os mais variados : ficaram órfãos , foram abandonados , ou fugiram de os abusos e maus tratos recebidos em casa . 
Aproximadamente quarenta meninos , entre nove e dezesseis anos , dormiam em as ruínas de o velho trapiche . 
Eram conhecidos como os " Capitães de Areia " e praticavam roubos , o que os tornou temidos e procurados por a polícia , que estava em busca de o esconderijo e de o chefe de os capitães . 
Tinham como líder Pedro Bala , rapaz de quinze anos , loiro , com uma cicatriz em o rosto . 
Generoso e valente , há dez anos vagabundeava por as ruas de Salvador , conhecendo cada palmo de a cidade . 
Em o bando , além de Pedro Bala , destacavam - se outros meninos : Sem-Pernas , espécie de espião ; João Grande , negro forte e o mais alto de todos ; José , o Professor , único que lia corretamente e gostava de contar histórias ; Pirulito , excessivamente místico e introvertido ; Gato , malandro elegante que tinha um caso com a prostituta Dalva ; Volta-Seca , mulato sertanejo afilhado de Lampião ; Boa-Vida , muito preguiçoso . 
Em o dia-a-dia , o bando contava com o apoio amigo de alguns adultos . 
Don'aninha , mãe de santo , sempre os socorria em caso de doença ou necessidade . 
Além de ela , o Padre José Pedro , introduzido em o grupo por o Boa-Vida , conhecia o esconderijo de os capitães . 
A os poucos , conquistou sua confiança , indo com freqüência visitá - los , levando um pouco de carinho e compreensão . 
O pescador Querido-de-Deus e o estivador João-de-Adão tinham a confiança de os meninos , que , por sua vez , não mediam esforços para recompensar esse apoio . 
O livro é dividido em três partes , tendo cada parte pequenos capítulos . 
Cada parte mostra um estado de os Capitães de a Areia : revoltados , amados e buscando um futuro , respectivamente . 
A divisão é basicamente essa , mas tenho uma idéia diferente sobre essa divisão : antes de Dora , com a Dora e depois de Dora , mas a explicação de isso ficará entrelinhas . 
Quem o ler entenderá . 
PREFÁCIO : Cartas a Redação Em essa parte , que não é realmente um capítulo , estão notícias retiradas de o Jornal de a Tarde , um jornal bahiano , que tem como tema os Capitães de a Areia . 
É aqui que sabemos de o que se trata os Capitães de a Areia , " conhecido grupo de meninos assaltantes e ladrões que infestam a nossa urbe " . 
Diz - se que o grupo é composto de um número superior a cem crianças entre oito e dezesseis anos . 
