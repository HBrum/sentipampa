Me senti completamente sufocada ! 
- não leria de novo ! - 
PONTO NEGATIVO : A linguagem utilizada é um pouco pesada para pessoas não habituadas a leitura . 
Foi tãããão díficil terminei este livro , e como o DVD estava em casa eu pensei " vou assistir o filme pra me incentivar a terminar " , e foi aí que a coisa piorou ! 
Os pensamentos de as personagens e as filosofias de o autor são falados , repetidos , re-repetidos , re-re-repetidos ... E em o final de o livro ainda há uma grande redação reforçando tudo mais ainda . 
Porém a história é cansativa , infelizmente . 
Quem não aprecia tanto , talvez não tenha gostado muito , principalmente de aquelas trinta páginas que falam de a política vigente de o partido . 
Reconheço que esta foi a única parte de o livro em que fiquei um pouco decepcionado , pois como um amigo meu me falou , ele poderia muito bem tê - la resumida , pois essa parte interessava sim , mas nem tanto . 
Tem uma grande história , mas infelizmente eu não tenho saco pro estilo de escrever de o autor 
muito violento ! 
Em o entanto , urge fazer certas ressalvas : como toda narrativa que se propõe fazer um diagnóstico de o seu tempo e de a sociedade , tentando até antecipar em certos pontos o futuro , deixa a desejar em muitos pontos . 
Contudo ... Desesperança até demais ! 
Não há a luz em o fim de o túnel . 
Não há nenhum tipo de vitória de a virtude em suas páginas . 
Essa é a narrativa de o vazio e de o niilismo totais . 
Li esse livro em o meu tempo de colégio e até hoje me lembro como em o começo foi muito difícil a leitura . 
Em a verdade em o começo estava achando insuportável o livro . 
Lá por o fim , a leitura deixa de ser interessante . 
Confesso que não compraria , até porque não gostei de ' Fala sério , amor ! ' . 
É um livrinho pra meninas adolescentes , é bonitinho , engraçadinho , conta coisas por as quais todas as meninas acabam passando , mas senti falta de uma linha , de um clímax antes de o desfecho . 
Bem enjoadinho ! 
Li em poucos dias e rápido ! 
Ainda bem que foi bem baratinho . 
Um fato que me incomodou muito foi que a Malu - protagonista - troca de namorado como se trocasse de roupa , a impressão que me passou foi que homens são seres descartáveis e que por qualquer briga termina e pronto . 
Mesmo sendo um livro de " contos " isso me incomodou . 
Foi o primeiro livro que eu li de a Thalita Rebouças e como todas as pessoas que eu conheço elogiavam muito a autora , fui com grande expectativa para esse livro e , admito , me decepcionei um pouco . 
Mais não é só isso , esse livro gera muitas decepções a os leitores , que como eu curto um bom livro . 
Fala serio , amor narra de uma forma simples e pacata o começo de as descobertas mais esperadas de toda garota , Um livro feito para o publico jovem deveria , de fato , ser escrito com mais paixão , com mais emoção , com a inocência que se é perdida , de a cumplicidade que se tem com o garoto que você escolha pra dar seu primeiro beijo . 
Esse livro não é nada mais de o que a historia de uma adolescente boba , que se achava a a esperta , ou seja , um livro sem historia , sem verdade e com muita bobagem . 
Péssimo . 
Meio chato , meio bobo , meio argh ... 
É interessante , mas , de fato , não está nem entre os 50 melhores que eu já li . 
São mini contos muito mal elaborados e meio bobos , o que contribui para o ar de falsa simpatia que o leitor cria por a história . 
Definitivamente , irritante . 
Enquanto lia esse livro , não é exagero dizer que tive que respirar fundo e contar até dez inúmeras vezes para não largá - lo por a metade . 
O problema em " Fala sério , mãe ! " , é o mesmo de " Fala Sério , professor ! " : pouca conexão com a realidade . 
Mas em este ocorre com mais frequencia , o que torna a leitura algo irritante , em alguns momentos . 
Pode - se notar claramente esse surrealismo observando as falas de Malu quando pequena . 
Muito vocabulário para pouca idade , como em essa frase , de quando ela tinha 6 anos : " - Uma beleza enorme , mamãe . 
Ou em a insistência em saber porque seu nome é Maria de Lourdes , não se conformando com as respostas de a mãe , sempre replicando com comentários assustadoramente complexos para uma menina de 5 anos . 
A mãe é demasiadamente sem-noção . 
O problema é que não funcionou , justamente porque as atitudes são tão fora de o comum , que a o invés de ser engraçado , soa forçado , artificial , e até mesmo ridículo , algumas vezes . 
Achei o livro um pouco fútil . 
Acho que adolescentes como a Malu são difícies de encontrar , não me identifiquei com ela . 
O que achei meio estranho foi a maneira de como a filha trata a mãe em muitos momentos , como ela é respondona , como já com tão pouca idade tem vocabulário extenso para respostas assustadoras ... Mas depois fiquei pensando ... isso é estranho para mim , pois nunca vivi isso em casa . 
Mas a partir de a segunda metade , Malu começa a ' 'pagar mico ' ' a usar o vocabulário aborrecente de ela e isso não me agradou . 
Mt bobo , indicado pra adolescentes e só . 
Confesso que , em o início , não fui muito com a cara de a história . 
Mais o mais insuportavel comportamento de o pai de Malu foi de quando ela começou a jogar futebol , seu pai fala que isso não e coisa de menina é que ela esta errada , mais Malu e munto boa em o futebol e ensina ate para seu pai que mesmo que ela seja uma mulher ela pode ter os mesmo direitos e as mesmas abilidades para jogar futebou como um homem qualquer ! 
Embora o livro seja legal , não gosto de a literatura de a Thalita Rebouças , acho que os livros de ela deveriam ser menos engraçados e mais interessantes , pois um livro tem que te prender , aí que é bom mesmo . 
É esse tipo de bobagem que as editoras estão impondo a os pré-adolescentes ? 
Então , animada resolvi começar por o de o professor , e infelizmente me decepcionei muito . 
A o contrário de o que imaginei , não me identifiquei nada com os professores de a Malu , e muito menos com ela . 
Achei o livro muito artificial , e nada próximo de a realidade de os estudantes . 
Li quando tinha 12 anos , e acredite , em essa idade já achei o livro infantil demais . 
Não vale a pena 
É um livro chato e sem uma narração muito boa . 
Diferente de os outrso de a série , ele não te prende a a estória e te insentiva a abandonar o livro . 
