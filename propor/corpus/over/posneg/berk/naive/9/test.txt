Apesar de muitas vezes ser uma leitura dificil , por causa de o tipo de linguagem usado , de os poucos paragrafos , de as falas sem travessões , apenas separadas por virgulas ou ponto final . 
Eu esperava bem mais de Ensaio Sobre a Cegueira . 
Esperava um livro fantástico e impressionante , mas em o fim acabei bem decepcionado por conta de alguns motivos . 
Por fim Saramago adora enrolar , o livro é repleto de frases de 8 , 9 linhas que não dizem nada , sendo simplesmente uma sequência de palavras que não adicionam nada - por exemplo : " O primeiro de a fila de o meio está parado , deve haver ali um problema mecânico qualquer , o acelerador solto , a alavanca de a caixa de velocidades que se encravou , ou uma avaria de o sistema hidráulico , blocagem de os travões , falha de o circuito eléctrico , se é que não se lhe acabou simplesmente a gasolina , não seria a primeira vez que se dava o caso " - , assim como inúmeros trechos que não adicionam nada A A história . 
O ritmo de a história é um tanto quanto inconsistente , em o geral é tudo meio lento e talvez por o volume de texto tem - se a impressão de que a coisa não está evoluindo . 
Os personagens principais são muito mal desenvolvidos , interpreto isso que o autor queria colocar o foco em o ' todo ' , mas é um tanto quanto estranho acompanhar um grupo de personagens tão simples . 
E a conclusão de a história me pareceu um tanto quanto preguiçosa , pra não dizer meio que vazia , afinal muito de o propósito de todos os acontecimentos acabaram se perdendo em o desfecho . 
Nunca sofri tanto para ler um livro . 
Mas , realmente , depois de ter lido , Memorial de o Convento , A jangada de pedra e outros mais de o Saramago , tenho a dizer que esse livro me atormentou e não voltarei a lê - lo tão em breve ! 
Em o entanto , acho que criei muita expectativa e quando o final realmente chegou , não achei tudo aquilo ... Para mim , foi só bom . 
Confesso que em o começo não gostei . 
A falta de pontuação ás vezes me confundiu por o fato de não saber se era a personagem quem estava falando ou o narrador . 
Em uma palavra : chato . 
O que chega a os dois únicos pontos negativos de o livro : 1º é a questão de que os capítulos são imensos , quase que intermináveis , o que deixa a leitura mais lenta , pesada , massante . 
Outro ponto negativo é exatamente isso , em certos pontos fica dificil de prosseguir com a leitura , o livro fica muito massante . 
Mas confesso que este não está em a lista de os meus livros preferidos de ele . 
Mas achei o final mto triste , não gostei não . 
E também não podemos esquecer o Larry Douglas , que eu particularmente detesto , mas isso só vocês lendo para entender . 
Não é uma obra prima literária com certeza , mas Sidney Sheldon tinha um dom que poucos tem : ele sabia como contar uma história e principalmente uma boa história ... Este é um de os melhores livros que li em minha vida . 
Chato , entediante , repetitivo 
Desisti de todos os demais títulos , não consegui terminar a leitura . 
O enredo é fraco e nada bem escrito , deixou muito a desejar . 
Li por ler , emprestado de um amigo . 
Com todo o respeito a quem gostou , mas sinceramente não consegui entender o que faz esse livro ser considerado bom . 
Achei a narrativa pobre , a história muito forçada e extremamente mal contada , os personagens " murchos " . 
Insisto : com todo o respeito a quem gostou , mas esse livro é desprezível . 
Não sei se recomendaria esse livro , penso que não com tantas opções em o mundo . 
Como disse em o título , o início de o livro não anima muito , pareceu bastante arrastado . 
Hoje a leitura fluiu mais rápida , mas depois de ler tantos livros de o Vonnegut e de tantos outros autores , eu acho o Sidney Sheldon muito prolixo e as vezes os mistérios são meio previsíveis . 
Ruim , uma estória muito previsível , como em todos os livros que ele escreve . 
Kate pode ser inteligente , mulher de negocios , linda ... mas parece que nao tem coração , passa por cima de td e de todos por a Empressa ! 
Odiei o que ela fez e as consequências que isso causou em a vida de pessoas que nasceram inocentes e que , tenho certeza , não teriam sido corrompidas se Kate não tivesse feito o que fez . 
Mas não daria um ' dez ' a um livro cujo personagem , pra atingir seu ' final feliz ' , controlou , manipulou e destruiu vidas em o processo . 
Em o inicio eu fiquei com a impressão de ser um plágio de O Conde de Monte Cristo , mas só a primeira parte é parecida . 
Mesmo assim , achei algumas partes de a história previsíveis e o final um tanto quanto tosco . 
Você tem uma protagonista burra que dói que faz uma besteira atrás de a outra , acaba presa por um crime que não cometeu e entra pra faculdade de a malandragem em a penitenciária . 
Parece legal , mas dá sono . 
Agora , os personagens , os relacionamentos e os diálogos são muito fracos . 
Os personagens passam a se amar e a se desamar de uma hora pra outra e eles são completamente volúveis e superficiais . 
Não é um romance como pretende ser e nem é original ; é uma coletânea de contos sendo vários de eles pirateados , inclusive um de escritor brasileiro . 
O início de a história em nada se relaciona com o meio ou o final . 
O livro é envolvente e tal , te prende em o começo , mas de o meio pro final parece meio forçado , o autor meio que começa a inventar histórias demais , não havendo uma ligação coerente entre elas , fica uma impressão de que se está lendo algum tipo de livro seriado , sei lá , com várias histórias diferentes em o mesmo livro . 
Se não houver coisa melhor 
Se Houver Amanhã não é tão ruim quanto eu imaginava , é claro que o livro não é um 5 estrelas , a prosa é pró lixo , superficial de mais , não permitindo que vc se apegue a os personagens e torça por eles . 
Apesar de as cinco estrelas , o livro possui alguns pontos fracos : A história em si é um pouco exagerada , principalmente por as barbaridades que acontecem em a vida de Tracy . 
O fato de a narrativa ser muito dinâmica e de não se abster em uma mesma situação por mais que algumas poucas páginas , faz com que alguns trechos se tornem ainda mais exacerbados . 
Enredo mirabolante , trama envolvente mas chega a haver exagero . 
nao chega a ser o melhor romance .... Gostei de o livro ... Não é uma historia maravilhosa mas ele te prende bastante ... so q alguns roubos de a protagonista foram sem muita explicação ... mas vale a pena le - lo . 
Parece uma versão mais recente de a coleção Bianca e outros de o gênero . 
Constrói uma imagem patética de a heroína , que tem que passar por várias injustiças para se tornar uma ladra . 
O único ponto que achei negativo , é o fato de ter muitos palavrões ! 
Apesar de isso esse livro não me agradou tanto por o final . 
Frustrante . 
Confesso que me decepsionei um pouco . 
Mas , o que me irritou em o livro é o fato de que a personagem principal tem um objetivo em o começo de o livro , que ela o resolve em alguns capítulos e depois o autor continua contando a história como se fosse uma espécie de diário de a vida de Tracy , os objetivos de ela mudam e a história se torna cansativa , parace que não acaba nunca . 
O romance entre Tracy e o outro personagem também é muito óbvio , é quase uma história paralela que serve para preencher a lacuna de tempo entre os roubos . 
Me decepsionei de novo . 
Livro muito bem escrito , onde conta a historia de varias geraçoes de a mesma fámilia ... O livro te prende muito .... recomendadíssimo ! 
Excelente ! 
Um de os melhores livros de Sydney Sheldon . 
Sou fã incondicional de os livros de o Sidney Sheldon , mas de todos , este sem dúvida é o melhor . 
Excelente ! 
Esse livro não é muito fino , a versão " normal " tem 402 páginas , mas em nenhum momento o ritmo de a estória diminuiu o que é incrível , pois o livro é separado em três momentos , de acordo com a vida de Tracy , e mesmo a terceira parte sendo a mais excitante , pois é quando ele começa a " trabalhar " em o mundo de o crime , as duas primeiras não ficam pra traz em qualidade . 
Eu simplesmente me encantei por o livro , e estou louca para repetir a dose e ler muitos outros livros de o Sidney Sheldon , se vocês já leram e tiverem alguma sugestão pra mim deixem em os comentários , e quem nunca leu eu aconselho a ler em a primeira oportunidade . 
Se houver amanhã foi um de os livros mais empolgantes que já li . 
Uma história rica em detalhes e artimanhas que até chegam as nos fazer prender o folego esperando o que vai acontecer , como a personagem irá se sair para resolver seus crimes . 
Sempre indico este livro a os meus amigos . 
A história é ótima e bastante surpreendente , não consegui largar o livro até terminá - lo . 
O melhor ponto de a história é , com certeza , a protagonista . 
É um ótimo livro e vale a pena cada página lida . 
Com certeza recomendo . 
O final é surpreendente . 
O livro é espetacular , merece ser lido , ele te prende de o começo a o fim e você fica torcendo por a heroina mesmo sabendo que ela está errada . 
Foi o primeiro livro de Sidney Sheldon que eu li , amei a história ! 
Toda trama envolvendo Tracy Whitney , como a vida de ela mudou e como essas mudanças determinaram seu futuro . 
É um livro sensasional ! 
O melhor entre todos os livros escritos por Sidney Sheldon . 
Sempre empolgante , não importa quantas vezes eu leia , sempre fico fascinada com o desenrolar de a trajetoria de Tracy Witney , virei fã de ela apesar de ser um personagem ficticio ! 
O livro é esplêndido , logo em o começo pensei que seria só mais uma história de injustiças , vinganças e superações , mas não . 
Envolve uma reviravolta em a vida de Tracy e as partes mais interessantes e angustiantes de o livro são os roubos , feitos em lugares inusitados . 
Se houver amanhã é antes de tudo surpreendente . 
Contém palavrões - o que eu não gosto muito em um livro - e algumas cenas ' picantes ' , mas nada que desmereça o livro e toda a história . 
Um de os melhores que já li . 
Nada dura para sempre , é um livro que possui uma leitura bem leve , de vocabulário bem direto . 
Sem aquelas palavras lindas . 
Esta obra consegue prender sua atenção de o início a o fim . 
Sem dúvida é uma história envolvente , que vale muito a pena ser lida . 
Recomendo a todos ! 
Incrível . 
Foi um de os livros que mais gostei de o autor e espero ter a oportunidade de ler o seus outro romances , principalmente O REVERSO DE A MEDALHA já que li a sua continuação , A SENHORA DE O JOGO . 
Se houve amanhã é extraordinariamente um livro arrebatador , com uma personagem arrasadora . 
Mas a revira volta de sua história acaba acontecendo de forma súbita e muito bem tramada . 
Ingenuidade a a parte , ela é um personagem inteligente e muito esperta , até demais . 
Perfeito ! 
Sem duvidas é um de os melhores livros que já li , a historia é incrível , super movimentada e bem desenvolvida , os personagens são marcantes e a narrativa é impecável . 
O autor te envolve de uma maneira tão forte que você não consegue largar o livro até o fim , absorto em os fatos apresentados e tentando descobrir o que virá a seguir , a historia é cheia de reviravoltas e te deixa surpreso a todo o momento . 
O livro conta a historia de Tracy , que esta irrevogavelmente em o topo de o meu top de protagonistas , ela é literalmente uma guerreira , passa por uma serie de acontecimentos difíceis e nós acompanhamos sua impressionante e de certa forma impactante transformação , sua característica mais marcante é a inteligência , ela é simplesmente incrível . 
Li em um dia ação , surpresas , roubos inimaginaveis tem de td em o em este livro imperdivel 
Em sua primeira missão ela se encontra com Jeff Stevens , nosso mocinho , cheio de charme essa é a melhor definição para esse personagem fascinante ... Restante de a resenhano blog : http://llucyinthesky.com/ ? 
Sem sombra de dúvida é um de os melhores livros que já li , Sidney Sheldon entrou pra minha lista de autores preferidos com ele e Tracy Whitney ganhou mais uma fã , como eu torci por essa personagem , acho que nunca encontrei uma protagonista que eu admirasse tanto , e creio que não estou sozinha em essa , uma amiga cogitou a hipótese de dar a sua filha , quando ela tiver uma , o nome de Tracy . 
Parece exagero eu sei , mas a história é realmente muito boa , Tracy Whitney tinha um bom emprego , ganhava bem , estava apaixonada e iria se casar com um homem de a alta sociedade que era carinhoso e bonito , se considerava a mulher mais feliz de o mundo , e a com mais sorte também . 
É em esse ponto que ocorre a passagem que mais gostei em o livro , o modo como Tracy sai de a cadeia , legalmente , muito antes de cumprir sua sentença , sem conseguir provar sua inocência . 
SE HOUVER AMANHÃ é um livro que quando se termina de ler se pergunta : por que é que não li este livro antes ? 
Para mim , " Se Houver Amanhã " é o melhor romance de Sidney Sheldon , talvez o melhor romance-aventura de todos os tempos . 
Sou fã número um de a Tracy Whitney e de o charmoso Jeff Stevens . 
Sem dúvida , Tracy é a heroína mais espetacular de a literatura , pq suas armas são : inteligência e beleza . 
Com todo esse movimento , não se consegue largar o livro até chegar a a última página . 
E quando o final chega e você fica pensando , bem que poderia ter uma seqüência ... RECOMENDADÍSSIMO ! 
ADORO ! 
Nossa Peeeeeeeeeeeeeeerfeito rsrs . 
Eu adorei a personagem , ela realmente fez o que qualquer pessoa de carater fariA correu atras de quem a pois em a prisão sem ela não ter feito nada ! 
Totalmente emocionante , eu leria novamente . 
Um de os meus livros prediletos ! 
Me deliciei com este livro , devorei - o rapidamente ! 
Uma estória tão bem escrita e interessante , passei noites de pouco sono , por não conseguir parar de lê - lo . 
Muito bom , ótimo , super recomendado ! 
Viciante e eletrizante . 
A gente torce por a personagem , apesar de ela ser uma ladra . 
Muito bom . 
O primeiro que li de Sidney Sheldon e , com certeza , um de os melhores entre todos . 
Esse livro ficará em a memória de muitos que lerem - no . 
Toda a parte legal de a estória , que são mais ou menos umas vinte páginas , minha esposa já tinha me contado . 
O enredo de a estória não é ruim , parece um filme de ação - aliás , Sidney Sheldon era roteirista antes de fingir que era escritor - . 
Galera , esse livro é maravilhoso ! 
Gente , esse livro é muito bom ! 
Em os prende de o começo a o fim . 
Recomento muito . 
Primeiro livro de o Sidney Sheldon que eu leio e me apaixono , repleto de suspense e uma pitada de românce eu amei 
Um livro instigante , uma história com sabor de quero mais , aventuras , vingança , romance é um pouco de o que se pode esperar de a obra de o magnífico Sheldon ... 
Este é o que eu mais gosto ! 
De todos os livros de o Sidney este é o que eu mais gosto . 
Acho o que mais me fez gostar foi a maneira que o li . 
Todos os personagens são bons , o enredo te engana , quando você pensa que a historia para ali , fica ainda melhor , toma uma proporção que não imaginava e é claro em o final deixa aquele gostinho de quero mais . 
Livro perfeito ... Essa mulher foi presa injustamente , perdeu o bebê e se vingou de cada pessoa que fez mau para ela e sua mãe ! 
Gostei , mt bom . 
O livro é otimo mais é como posso faalr 2 em 1 . 
Claro , isso não faz com que o livro seja cansativo , muito por o contrário . 
Além de diversas histórias surpreendentes , conhecemos personagens incríveis , como é o caso de a própria Tracy . 
É impossível não se apaixonar por ela : uma mulher linda , guerreira , determinada , enfim . 
É um livro sensacional , apesar , de como eu disse , o começo e meio , não coencidem com o final . 
Aventuras essas , deliciosas de se ler . 
São emocionantes e além de tudo , muito astutas as confusões que essa protagonista cheia de charme se mete ! 
Sidney Sheldon não decepciona e sim surpreende com esse livro fantástico , de verdade , NÃO DÁ PRA PARAR DE LER ! 
Já livros bons , livros ótimos , mas Se Houver Amanhã é imcomparável . 
ps : ainda sou completamente apaixonada por Jeff Stevens , o cara é perfeito ! 
O livro é incrível , quanto mais você acha que entendeu a historia e sabe o que vai acontecer , mais ele te surpreende com novas situações . 
Mas advirto : Só leia - o quando tiver tempo , pois não vai mais conseguir parar ! 
- : 
É um livro muito gostoso de se ler , mas é literatura barata . 
Pra inicio de conversa : Não quero dizer que o livro só se torna bom em a página 170 , ele é ótimo de o inicio a o fim , mas o fato é ele só realmente toma seu corpo em a página 170 . 
O ritmo é eletrizante e rápido , mal dá tempo de ter a curiosidade , e as soluções já aparecem , é um digno livro de o mestre Sheldon , com mais uma perfeita personagem feminina , a Tracy , ela passa por fases , diversas de elas de uma mocinha lerdinha , á uma presidiria revoltada , para uma vingativa put's revolt's , e por fim á mestra de roubos . 
O livro embora antigo , é atual . 
Indico . 
Não é a melhor obra de o grande Sidney Sheldon , mas recomendo , uma boa leitura pra passar o tempo . 
Disparado o melhor livro de o Shaeldon que já li . 
A narrativa é tão incrivelmente bem construída , que algumas vezes eu tinha a impressão de que estava lendo um outro livro , diferente de o que tinha começado a ler . 
Problemas como preconceito o funcionamento de as penitenciárias são temas relevantes em esse espetacular romance de Sidney Sheldon . 
Se houver Amanhã é fascinante , tem uma história que envolve o leitor com seu enredo de trapassas mirabolantes , mesmo sendo uma prática errada de os personagens você acaba se apaixonando por eles . 
Para os leitores que gostam de um bom livro este é de aqueles para ler e reler . 
A história em si é legal , uma mulher colocada em a cadeia injustamente que decide se vingar de os seus algozes e de a sociedade , o livro se divide em 3 livros e é narada em terceira pessoa , se vc acabou de ler um Dostoievisk ou um Saramago e quer algo mais leve , sem grandes questões a se refletir de leitura rápida leia Se Houver Amanhã . 
Ótimo livro , me prendeu de o começo a o fim , leva a história muito bem o Sidney , super recomendado , comprei mais outros títulos de ele e irei ler certamente . 
Se você lê um livro de 434 páginas em dois dias , é por que ele é realmente muito bom ! 
Entretanto , como obra literária de ficção e com o objetivo de entreter , esse é um livro maravilhoso ! 
Os capitulos curtos e a história dinâmica fazem com que o livro flua de forma genial , impossibilitando o leitor de abandoná - lo antes de a última página ser lida . 
Livro muito bom , como muitos de os livros de o Sidney é um de aqueles que prende a atenção de o começo a o fim , a única parte de a qual deixou a desejar foi um pouco de o começo a ingenuidade de a Tracy meio que me irrito , mas achei bem legal a parte de as tramas e a inteligência de como foram executados os planos , recomendo ; - . 
É sem dúvida um livro bem interessante , principalmente por se tratar primeiramente de vingança , e por a perfeição com que os crimes acontecem . 
Bem legal . 
Excelente ! 
algumas passagens angustiantes em essa hora ... mas fortalece a vontade de ler mais e mais ! 
e em o fim ... vc fica com um sorriso bobo estampado em o rosto e um gosto de quero mais ! 
Devorei . 
Adorei a personagem Tracy , sua transformação , crescimento . 
Algumas passagens de o livro me fizeram contorcer , mas Tracy foi forte e me impressionei com o andamento de a trama . 
É puro entretenimento e deve ser lido sem preconceito . 
Mas tenho que admitir , a leitura flui , a trama prende até a última página . 
Acho legal isso , pois já prende o leitor já em o início . 
A protagonista , Tracy Whitney , vai ficar guardada em a minha mente como uma de as mais belas e espertas personagens que eu já lera . 
Esse livro trouxe tudo o que eu queria : gosto mesmo de ação , de mistério , aventura , e esse livro saciou minha sede . 
Agora , Sidney Sheldon vai ganhar mais um fã , pois esse foi o primeiro de muitos que lerei . 
Irresistível , desafiador , envolvente , intrigante , excelente ... Nota 10 . 
A narrativa é empolgante e muito bem construída ! 
Para quem gosta de desvendar mistérios , é uma excelente leitura . 
Gostei muito . 
Como sempre muito bom 
Mais um bom romance de Sidney Sheldon . 
grande passa tempo .. li rapidinho ... e envolvente tambem ... vc nunca vai acha - lo chato ou cansativo ... ele eh bem corrido ... leiammmmmmm 
Putz o melhor livro de romance polícial que eu já li ! 
Sem palavras para descrever o quanto ele é bom . 
As personagens são muito bem trabalhadas e a digreção psicológica é fantástica . 
Envolvente , inteligente e divertido . 
Apesar de o começo um tanto melodramático , a história é cheia de surpresas , estratégias e trapaças , uma garantia de emoções e diversão . 
Não deixou nada a dever , a ação e o romance foram muito bem preenchidos e temos um final divertido e com gostinho de quero mais . 
Uma trama com grandes sacadas , tornou - se um de os meus livros favoritos . 
ele te prende mais por causa de a personagem de o que por o enredo . 
mas o suspense e as peripécias de tracy são bem descritas e aguçam a curiosidade 
Mara . 
Livro incrivel dmais . 
com certeza eh o melhor de o Sidney - ate agora - . 
:P Super Recomendo *---* 
Li esse livro quando tinha 12 anos de idade e adorei . 
Faz muito tempo desde a minha última leitura - aproximadamente 10 anos - mas o interessante é que não aconteceu o que geralmente acabou por acontecer com os livros que eu li em a época : de esse eu me lembro claramente de a história e de como era bem escrita , interessante e cheia de reviravoltas . 
Definitivamente um de os melhores livros de Sidney Sheldon - se não , o melhor - só não escrevo com certeza pois não li todos . 
Mas , claro , lembro de ter apreciado a leitura de todos , principalmente de o Conte - me Seus Sonhos , que foi o primeiro que li de o autor e é simplesmente extraordinário , só não ganha de esse . 
Nossa , que livro é esse ? 
Livro que em muiitas partes tiive que parar para respirar , para tomar um pouco de fôlego .. kkk Gostei Bastante , a Tracy e o Jeff formam um casal perfeito e idênticos =p Suas trapaças se tornam um vício e em o final , fica uma pergunta em o final : Eles realmente conseguiram parar ? 
Irresistível , fantástico , maravilhoso , surpreendente 
Faltam adjetivos para descrever este livro , ele é simplesmente o melhor livro de o Sidney Sheldon que eu já li ! 
É simplesmente impossível parar de ler depois de começar ! 
Recomendo a todos que gostam de um livro que prende a leitura de o início a o fim ! 
; - 
Como não se apaixonar por tracy whitney , essa mulher que foi injustamente presa por um crime que não cometeu , é ótimo vc acompanhar a reviravolta de ela .. Todos devem ler . 
Maravilhoso ! 
O melhor livro de o Sidney Sheldon que já li ! 
Trracy é uma de as melhores ' protagonistas ' de as histórias de ele ... 
ótimo ! 
Resolvi dar uma chance a ele e me apaixonei . 
O livro é ótimo . 
A história dá tantas reviravoltas que você se surpreende . 
Chega um momento que você não quer parar de ler . 
Amei e recomendo . 
Esse sem duvidas foi um de os melhores livros que eu ja li . 
O começo de ele é meio parado , e ouso a dizer um pouco entediante , mais se voce vencer as primeiras paginas ele se transforma em um suspence imprevisivel e envolvente . 
ja li outros livros de o sidney sheldon , mas esse é perfeito ... muito bom mesmo , te envolve de um jeito inexplicavel , recomendo a todos ! 
Um clássico exemplar , cheio de mistérios e reviravoltas . 
Fantástico Surpreendente Aplausos ... 
O melhor livro de Sidney Sheldon 
Pra mim , esse foi o melhor livro de o S.S. A personagem de o livro Tracy , é so sofrimento em o começo , sofremos junto com ela ... ela perde tudo assim que sabe que sua mãe cometeu suicidio , ela quer se vingar de o sujeito e acaba presa e ninguem ajuda , nem o noivo de ela ... mas ela sai de a cadeia resolve se vingar de todos ... ela vira uma ' vigarista ' , que consegue fazer roubos , que nem a policia consegue descobrir , e msm assim a gente torce por ela , pra não ser pega ... é muito bom ... ah é tem o Jeff , outro vigarista , os dois se apaixonam ... ah é muito bom ! 
Quatro estrelinhas , pois achei que Sidney Sheldon viajou bastante em algumas cenas de o livro , fora isso , é excelente ! 
Foi um bom começo . 
Reviravolta surpreendente 
Talvez tenha sido por que eu não li a sinopse de o livro , mas a estoria foi um choque pra mim . 
Uma envolvente história , em a qual a personagem Tracy Witney após ser injustamente acusada e condenada por 15 anos de cadeia , se torna uma vigarista de primeira linhagem . 
Um livro que vc começa a ler e não quer parar . 
Envolvente , simplesmente maravilhoso , dá pra ver bem de perto a negligencia de a policia , que em a verdade é assim em o mundo inteiro ... 
O livro é surpreendente , começa de um jeito e depois de algumas páginas você não tem idéia de como vai terminar . 
Mas , o desenrolar de essa história é demais . 
Leiam ! 
Vale a pena xD 
A história de o livro em si , é muito boa ... O autor descreve os acontecimentos como se fosse um filme . 
Realmente é emocionate ! 
A cada parte de o livro o leitor torce para a protagonista se dar bem . 
Odeia quem a faz mal - por o menos a maioria , pois Jeff Stevens conseguiu me cativar - e comemora quando um aliado aparece . 
E o leitor acompanha , se emocionando com a jornada árdua , difícil e divertida de a Srta . Withney . 
Uma história cheia de ação , suspense , revelações e reviravoltas , que convida o leitor para uma viagem através de a riqueza por o mundo , mostrando como o dinheiro pode transformar uma pessoa , tanto para melhor - em o caso de Tracy - , tanto pior . 
Em suma , é um livro memorável , que nos ensina que seguir em frente sem medo de errar é o melhor caminho para a felicidade . 
Em a minha opinião este é o melhor livro de ele , uma mulher que passa por uma série de dificuldades , medo , perigo , e isso torna ela forte para conseguir sobreviver , e com um enredo surpreendente ... Amei ... Meu Preferido , o inesquecível Sidnei Sheldon . 
vale a pena ! 
Com ctz o melhor ! 
Em a minha opinião é o melhor livro de Sidney Sheldon . 
Perfeito ! 
Vc não consegue parar de ler um minuto . 
Tracy é ótima ! 
Faz a gente rir , chorar , sentir raiva ... Impossível parar de ler 
Nossa em a minha opinião o melhor livro de Sheldon uma história apaixonante . 
Uns de os meus preferidos livros que releria com o maior prazer , já que é uma ótima obra que trata de roubos , o mundo atrás de a prisão , honestidade , romance de uma forma bem trabalhada e uma virada de sorte em a vida . 
Um livro chocante e que chamou mais ainda a minha atenção é que capítulo após capítulo a história fica ainda mais intressante e boa . 
Umas de as maiores obras primas de Sidney Sheldon e a minha também . 
um ótimo livro ! 
engenhoso , vale a pena ler ainda mais se vc for mulher rs = ] 
Como sempre : Incrivel ! 
Um de os meus livros favoritos de ele - se é q tem como escolher - . 
RECOMENDADISSIMO ! 
Uma ladra que poderia ser a professora de a personagem de Catherine Zeta-Jones em o filme " Armadilha " , de tão engenhosa , sendo o que Zeta-Jones não conseguiu em o filme : ser fascinante . 
Não foi a a toa que Tracy Whitney foi considera a mais atraente de as heroínas de Sheldon : acabamos torcendo para que ela se dê bem . 
Um livro expetacular dese escritor fantástico , só ele mesmo pra escrever livros com uma dinâmica que é dificil parar de ler , eu mesmo só suceguei quando terminei de ler e achei esse livro um de os melhores de ele . 
Com certeza um de os melhores que já li ! 
O livro prende bastante o leitor ! 
Livro fantástico indico a leitura para todos ! 
Um de os melhores livros de o sidney ! 
Primeiro livro de o Sidney Sheldon que eu li , e , como podem ver em a minha estante , valeu muito a pena . 
Apesar de ser um livro longo , a leitura não é nem um pouco cansativa , - mas para isso tem que gostar de ler - . 
O livro mais inteligente que li até agora , sendo tambem um estouro de os muitos best - sellers que fazem parte de essa coleção . 
Porém , este eu realmente gosto . 
Já li várias vezes e sempre é um passatempo muito bom . 
A heroína de o livro é uma ladra e suas aventuras e criações são ótimas . 
Acho que o leitor - uso meu exemplo - se realiza através de Tracy , imaginando como faria para se vingar de um mundo injusto e ganhar com isso . 
A heroína acaba conquistando a simpatia de o leitor , que torce por ela de o começo a o fim . 
Esse livro é bem antigo mas nem por isso economiza em a trama . 
Ação , vingança e muita trama inteligente garantem uma leitura voraz . 
O livro em uma palavra : INTENSO . 
Vale a pena vasculhar os sebos e livrarias atrás de esse livro porque é certeza de horas de puro deleite . 
A cena de o campeonato de xadrez é a minha preferida . 
Só lendo pra saber , nem vou tentar falar sobre este livro , só tenho uma coisa a dizer , eu gostaria de ser tão inteligente quanto ela . 
História que prende 
A história prende de o início a o fim . 
E é um de os poucos livros que eu com si lembrar o nome de os protagonistas : Tracy Whitney e Jeff Stevens . 
É claro que a Tracy não é nenhum exemplo a ser seguido mas o livro prende com certeza . 
Mas o livro em o geral é muito interessante . 
Livro de descreve a vida de uma mulher , que todos iram pensar que estava perdida , pois vai presa por engano , mas ai a vida de ela dar uma virada , e dai por diante muitas coisas interessante e inesplicaveis acontece .... 
Deliciosamente sacrificante e justiceiro ! 
a história é linda , quando se quer algo de a vida , se ela não te der ; então roube . 
Um de os melhores livros que eu já li . 
É impossível largá - lo antes de o fim . 
O livro é cheio de mistérios e suspense ! 
Maravilhoso ! 
Um livro muito inteligente e enigmático que prende a sua atenção de a primeira a última página . 
O meu favorito de o Sidney Sheldon , é surpreendente e tem um ritmo de tirar o fôlego . 
Mas de essa vez ela supera a situação de uma forma incrível . 
A história fica cada vez melhor , com ação , esquemas inteligentes que você só entende o truque depois , e uma maravilhosa mistura de rivalidade com romance . 
Mas foi essa obra que me fez amar todas as outras de o autor . 
História intrigante que prende a atenção e surpreende a cada página . 
Perfeitoooo ... tem tudo para virar filme ... e podem me convidar pra interpretar a Tracy rsss ... Se houver amanhã- Sidney Sheldon - e Três destinos - Nora Roberts - são simplesmente los mejores ! 
Perfeito os planos de a protagonista de o livro ... Bem interessante e eu quase tenho um infarto com as coisas que ela se livrava ... rsrs Indico ! 
Astucia , muita astucia . 
Esse livro , convida os leitores a exercitar mais o cerebro . 
Leva - nos de zero a cem em poucos segundos . 
Uma histotia eletrizante , sem dúvida . 
O MELHOR .... 
Exelente ! 
Tracy Whitney uma de as minhas heroínas preferidas , ela mostra como é importante não desistir , e que uma idéia criativa sempre é um bom caminho ! 
A história tem um ritmo delicioso , além de os roubos espetaculares que são narrados ! 
O melhor de tudo é que em muitos só descobrimos o truque depois de o golpe , o que dá outro sabor a leitura ! 
Uma de as minhas heroínas preferidas , ela mostra como é importante não desistir , e que uma idéia criativa sempre é um bom caminho ! 
Este foi o primeiro livro que li de Sidney Sheldon , e amei a reviravolta que aconteceu em a vida de a protagonista de a historia , você acaba virando torcida de Tracy . 
E como o faz maravilhosamente bem ! 
Nunca torci tanto por uma criminosa . 
Confesso que ele peca em algumas de suas obras , mas esta é uma de as melhores . 
Um de os melhores que já li em a minha vida ! 
Meu livro favorito ! 
Sou fã de a Tracy , que em a minhã opinião é a melhor heroína que Sidney seldon já criou .. O livro é perfeito , e viciante ! 
recomendadíssimo ! 
Ótima leitura ! 
Um livro de ótima leitura , com certeza ! 
Uma história cativante que passa por as mais diversas situações . 
Bom desde o primeiro capítulo ! 
Uma leitura fácil e agradável , recomendado a todos ! 
O que mais gostei foram os roubos , realmente , muito bem contados , geniais , ousaria dizer . 
Sem dúvida , é um de os melhores livros que eu já li ! 
Os crimes e as armações de Tracy são incríveis e , sinceramente , o final não podia ser melhor . 
Quando se acaba de ler fica aquela sensação estranha e a vontade de ler novamente . 
Um de os melhores livros de o autor , vemos claramente em esse livro uma mulher que foi injustiçada e após se vingar , acaba gostando de aplicar golpes em as pessoas que não são tão dignas . 
Uma história que te prende de o inicio a o fim . 
Li quando tinha 13 anos de idade e este livro me abriu as portas para o que veio a ser uma grande paixão em a minha vida : a literatura ! 
Uma história cheia de ação , romance e alguns clichês temáticos próprios de o Sidney Sheldon , mas imperceptíveis a os olhos de um leitor que preza mais por o conteúo e por a afetividade de o que por a técnica . 
Nunca vou esquecer de a tristeza que senti quando terminei de ler " Se houver amanhã " . 
